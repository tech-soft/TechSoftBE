package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QEstadoEntity is a Querydsl query type for EstadoEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QEstadoEntity extends EntityPathBase<EstadoEntity> {

    private static final long serialVersionUID = -531716296L;

    public static final QEstadoEntity estadoEntity = new QEstadoEntity("estadoEntity");

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QEstadoEntity(String variable) {
        super(EstadoEntity.class, forVariable(variable));
    }

    public QEstadoEntity(Path<? extends EstadoEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEstadoEntity(PathMetadata<?> metadata) {
        super(EstadoEntity.class, metadata);
    }

}

