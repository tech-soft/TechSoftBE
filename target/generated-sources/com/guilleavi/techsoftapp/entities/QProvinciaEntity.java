package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QProvinciaEntity is a Querydsl query type for ProvinciaEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QProvinciaEntity extends EntityPathBase<ProvinciaEntity> {

    private static final long serialVersionUID = -159662783L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProvinciaEntity provinciaEntity = new QProvinciaEntity("provinciaEntity");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath nombre = createString("nombre");

    public final QPaisEntity pais;

    public QProvinciaEntity(String variable) {
        this(ProvinciaEntity.class, forVariable(variable), INITS);
    }

    public QProvinciaEntity(Path<? extends ProvinciaEntity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QProvinciaEntity(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QProvinciaEntity(PathMetadata<?> metadata, PathInits inits) {
        this(ProvinciaEntity.class, metadata, inits);
    }

    public QProvinciaEntity(Class<? extends ProvinciaEntity> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.pais = inits.isInitialized("pais") ? new QPaisEntity(forProperty("pais")) : null;
    }

}

