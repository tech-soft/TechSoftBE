package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPersonaRolEntity is a Querydsl query type for PersonaRolEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPersonaRolEntity extends EntityPathBase<PersonaRolEntity> {

    private static final long serialVersionUID = -1921652971L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPersonaRolEntity personaRolEntity = new QPersonaRolEntity("personaRolEntity");

    public final QEstadoEntity estado;

    public final DatePath<java.time.LocalDate> fechaAlta = createDate("fechaAlta", java.time.LocalDate.class);

    public final DatePath<java.time.LocalDate> fechaBaja = createDate("fechaBaja", java.time.LocalDate.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QPersonaEntity persona;

    public final QRolTipoEntity rolTipo;

    public QPersonaRolEntity(String variable) {
        this(PersonaRolEntity.class, forVariable(variable), INITS);
    }

    public QPersonaRolEntity(Path<? extends PersonaRolEntity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPersonaRolEntity(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPersonaRolEntity(PathMetadata<?> metadata, PathInits inits) {
        this(PersonaRolEntity.class, metadata, inits);
    }

    public QPersonaRolEntity(Class<? extends PersonaRolEntity> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.estado = inits.isInitialized("estado") ? new QEstadoEntity(forProperty("estado")) : null;
        this.persona = inits.isInitialized("persona") ? new QPersonaEntity(forProperty("persona"), inits.get("persona")) : null;
        this.rolTipo = inits.isInitialized("rolTipo") ? new QRolTipoEntity(forProperty("rolTipo")) : null;
    }

}

