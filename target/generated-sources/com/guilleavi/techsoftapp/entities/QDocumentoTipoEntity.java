package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QDocumentoTipoEntity is a Querydsl query type for DocumentoTipoEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDocumentoTipoEntity extends EntityPathBase<DocumentoTipoEntity> {

    private static final long serialVersionUID = -128638756L;

    public static final QDocumentoTipoEntity documentoTipoEntity = new QDocumentoTipoEntity("documentoTipoEntity");

    public final StringPath descripcion = createString("descripcion");

    public final StringPath descripcionReducida = createString("descripcionReducida");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QDocumentoTipoEntity(String variable) {
        super(DocumentoTipoEntity.class, forVariable(variable));
    }

    public QDocumentoTipoEntity(Path<? extends DocumentoTipoEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDocumentoTipoEntity(PathMetadata<?> metadata) {
        super(DocumentoTipoEntity.class, metadata);
    }

}

