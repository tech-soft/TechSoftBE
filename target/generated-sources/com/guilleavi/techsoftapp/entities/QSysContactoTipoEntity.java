package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QSysContactoTipoEntity is a Querydsl query type for SysContactoTipoEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSysContactoTipoEntity extends EntityPathBase<SysContactoTipoEntity> {

    private static final long serialVersionUID = -910624220L;

    public static final QSysContactoTipoEntity sysContactoTipoEntity = new QSysContactoTipoEntity("sysContactoTipoEntity");

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QSysContactoTipoEntity(String variable) {
        super(SysContactoTipoEntity.class, forVariable(variable));
    }

    public QSysContactoTipoEntity(Path<? extends SysContactoTipoEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSysContactoTipoEntity(PathMetadata<?> metadata) {
        super(SysContactoTipoEntity.class, metadata);
    }

}

