package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPersonaComercialEntity is a Querydsl query type for PersonaComercialEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPersonaComercialEntity extends EntityPathBase<PersonaComercialEntity> {

    private static final long serialVersionUID = -902185723L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPersonaComercialEntity personaComercialEntity = new QPersonaComercialEntity("personaComercialEntity");

    public final NumberPath<Double> descuento = createNumber("descuento", Double.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QIvaTipoEntity ivaTipo;

    public final QPagoFormaEntity pagoForma;

    public final QPersonaEntity persona;

    public final QPrecioListaEntity precioLista;

    public QPersonaComercialEntity(String variable) {
        this(PersonaComercialEntity.class, forVariable(variable), INITS);
    }

    public QPersonaComercialEntity(Path<? extends PersonaComercialEntity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPersonaComercialEntity(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPersonaComercialEntity(PathMetadata<?> metadata, PathInits inits) {
        this(PersonaComercialEntity.class, metadata, inits);
    }

    public QPersonaComercialEntity(Class<? extends PersonaComercialEntity> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.ivaTipo = inits.isInitialized("ivaTipo") ? new QIvaTipoEntity(forProperty("ivaTipo")) : null;
        this.pagoForma = inits.isInitialized("pagoForma") ? new QPagoFormaEntity(forProperty("pagoForma")) : null;
        this.persona = inits.isInitialized("persona") ? new QPersonaEntity(forProperty("persona"), inits.get("persona")) : null;
        this.precioLista = inits.isInitialized("precioLista") ? new QPrecioListaEntity(forProperty("precioLista")) : null;
    }

}

