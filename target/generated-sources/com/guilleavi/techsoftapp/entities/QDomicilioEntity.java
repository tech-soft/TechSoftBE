package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QDomicilioEntity is a Querydsl query type for DomicilioEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDomicilioEntity extends EntityPathBase<DomicilioEntity> {

    private static final long serialVersionUID = -1312706311L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QDomicilioEntity domicilioEntity = new QDomicilioEntity("domicilioEntity");

    public final StringPath calle = createString("calle");

    public final QCiudadEntity ciudad;

    public final StringPath descripcion = createString("descripcion");

    public final StringPath dpto = createString("dpto");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath nro = createString("nro");

    public final QPersonaEntity persona;

    public final StringPath piso = createString("piso");

    public final BooleanPath principal = createBoolean("principal");

    public QDomicilioEntity(String variable) {
        this(DomicilioEntity.class, forVariable(variable), INITS);
    }

    public QDomicilioEntity(Path<? extends DomicilioEntity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QDomicilioEntity(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QDomicilioEntity(PathMetadata<?> metadata, PathInits inits) {
        this(DomicilioEntity.class, metadata, inits);
    }

    public QDomicilioEntity(Class<? extends DomicilioEntity> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.ciudad = inits.isInitialized("ciudad") ? new QCiudadEntity(forProperty("ciudad"), inits.get("ciudad")) : null;
        this.persona = inits.isInitialized("persona") ? new QPersonaEntity(forProperty("persona"), inits.get("persona")) : null;
    }

}

