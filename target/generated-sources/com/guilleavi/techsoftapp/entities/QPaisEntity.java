package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPaisEntity is a Querydsl query type for PaisEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPaisEntity extends EntityPathBase<PaisEntity> {

    private static final long serialVersionUID = 1332702381L;

    public static final QPaisEntity paisEntity = new QPaisEntity("paisEntity");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath nombre = createString("nombre");

    public QPaisEntity(String variable) {
        super(PaisEntity.class, forVariable(variable));
    }

    public QPaisEntity(Path<? extends PaisEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPaisEntity(PathMetadata<?> metadata) {
        super(PaisEntity.class, metadata);
    }

}

