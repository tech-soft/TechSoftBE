package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QIvaTipoEntity is a Querydsl query type for IvaTipoEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QIvaTipoEntity extends EntityPathBase<IvaTipoEntity> {

    private static final long serialVersionUID = 984215484L;

    public static final QIvaTipoEntity ivaTipoEntity = new QIvaTipoEntity("ivaTipoEntity");

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QIvaTipoEntity(String variable) {
        super(IvaTipoEntity.class, forVariable(variable));
    }

    public QIvaTipoEntity(Path<? extends IvaTipoEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QIvaTipoEntity(PathMetadata<?> metadata) {
        super(IvaTipoEntity.class, metadata);
    }

}

