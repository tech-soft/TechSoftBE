package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPersonaEntity is a Querydsl query type for PersonaEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPersonaEntity extends EntityPathBase<PersonaEntity> {

    private static final long serialVersionUID = 2059974208L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPersonaEntity personaEntity = new QPersonaEntity("personaEntity");

    public final BooleanPath cliente = createBoolean("cliente");

    public final NumberPath<Long> documentoNumero = createNumber("documentoNumero", Long.class);

    public final QDocumentoTipoEntity documentoTipo;

    public final QEstadoEntity estado;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath nombre = createString("nombre");

    public final BooleanPath proveedor = createBoolean("proveedor");

    public QPersonaEntity(String variable) {
        this(PersonaEntity.class, forVariable(variable), INITS);
    }

    public QPersonaEntity(Path<? extends PersonaEntity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPersonaEntity(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPersonaEntity(PathMetadata<?> metadata, PathInits inits) {
        this(PersonaEntity.class, metadata, inits);
    }

    public QPersonaEntity(Class<? extends PersonaEntity> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.documentoTipo = inits.isInitialized("documentoTipo") ? new QDocumentoTipoEntity(forProperty("documentoTipo")) : null;
        this.estado = inits.isInitialized("estado") ? new QEstadoEntity(forProperty("estado")) : null;
    }

}

