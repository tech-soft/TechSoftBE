package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPersonaRelacionTipoEntity is a Querydsl query type for PersonaRelacionTipoEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPersonaRelacionTipoEntity extends EntityPathBase<PersonaRelacionTipoEntity> {

    private static final long serialVersionUID = -552320351L;

    public static final QPersonaRelacionTipoEntity personaRelacionTipoEntity = new QPersonaRelacionTipoEntity("personaRelacionTipoEntity");

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QPersonaRelacionTipoEntity(String variable) {
        super(PersonaRelacionTipoEntity.class, forVariable(variable));
    }

    public QPersonaRelacionTipoEntity(Path<? extends PersonaRelacionTipoEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPersonaRelacionTipoEntity(PathMetadata<?> metadata) {
        super(PersonaRelacionTipoEntity.class, metadata);
    }

}

