package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QRolTipoEntity is a Querydsl query type for RolTipoEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QRolTipoEntity extends EntityPathBase<RolTipoEntity> {

    private static final long serialVersionUID = -851619081L;

    public static final QRolTipoEntity rolTipoEntity = new QRolTipoEntity("rolTipoEntity");

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public QRolTipoEntity(String variable) {
        super(RolTipoEntity.class, forVariable(variable));
    }

    public QRolTipoEntity(Path<? extends RolTipoEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRolTipoEntity(PathMetadata<?> metadata) {
        super(RolTipoEntity.class, metadata);
    }

}

