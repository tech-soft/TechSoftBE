package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCiudadEntity is a Querydsl query type for CiudadEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCiudadEntity extends EntityPathBase<CiudadEntity> {

    private static final long serialVersionUID = 864750762L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCiudadEntity ciudadEntity = new QCiudadEntity("ciudadEntity");

    public final StringPath codigoPostal = createString("codigoPostal");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath nombre = createString("nombre");

    public final QProvinciaEntity provincia;

    public QCiudadEntity(String variable) {
        this(CiudadEntity.class, forVariable(variable), INITS);
    }

    public QCiudadEntity(Path<? extends CiudadEntity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCiudadEntity(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCiudadEntity(PathMetadata<?> metadata, PathInits inits) {
        this(CiudadEntity.class, metadata, inits);
    }

    public QCiudadEntity(Class<? extends CiudadEntity> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.provincia = inits.isInitialized("provincia") ? new QProvinciaEntity(forProperty("provincia"), inits.get("provincia")) : null;
    }

}

