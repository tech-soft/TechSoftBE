package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPersonaRelacionEntity is a Querydsl query type for PersonaRelacionEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPersonaRelacionEntity extends EntityPathBase<PersonaRelacionEntity> {

    private static final long serialVersionUID = -533790899L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPersonaRelacionEntity personaRelacionEntity = new QPersonaRelacionEntity("personaRelacionEntity");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QPersonaEntity personaPrincipal;

    public final QPersonaEntity personaRelacionada;

    public final QPersonaRelacionTipoEntity tipo;

    public QPersonaRelacionEntity(String variable) {
        this(PersonaRelacionEntity.class, forVariable(variable), INITS);
    }

    public QPersonaRelacionEntity(Path<? extends PersonaRelacionEntity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPersonaRelacionEntity(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPersonaRelacionEntity(PathMetadata<?> metadata, PathInits inits) {
        this(PersonaRelacionEntity.class, metadata, inits);
    }

    public QPersonaRelacionEntity(Class<? extends PersonaRelacionEntity> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.personaPrincipal = inits.isInitialized("personaPrincipal") ? new QPersonaEntity(forProperty("personaPrincipal"), inits.get("personaPrincipal")) : null;
        this.personaRelacionada = inits.isInitialized("personaRelacionada") ? new QPersonaEntity(forProperty("personaRelacionada"), inits.get("personaRelacionada")) : null;
        this.tipo = inits.isInitialized("tipo") ? new QPersonaRelacionTipoEntity(forProperty("tipo")) : null;
    }

}

