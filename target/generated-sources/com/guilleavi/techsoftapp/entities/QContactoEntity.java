package com.guilleavi.techsoftapp.entities;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QContactoEntity is a Querydsl query type for ContactoEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QContactoEntity extends EntityPathBase<ContactoEntity> {

    private static final long serialVersionUID = -1886919775L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QContactoEntity contactoEntity = new QContactoEntity("contactoEntity");

    public final StringPath contacto = createString("contacto");

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final QPersonaEntity persona;

    public final NumberPath<Integer> tipo = createNumber("tipo", Integer.class);

    public QContactoEntity(String variable) {
        this(ContactoEntity.class, forVariable(variable), INITS);
    }

    public QContactoEntity(Path<? extends ContactoEntity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QContactoEntity(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QContactoEntity(PathMetadata<?> metadata, PathInits inits) {
        this(ContactoEntity.class, metadata, inits);
    }

    public QContactoEntity(Class<? extends ContactoEntity> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.persona = inits.isInitialized("persona") ? new QPersonaEntity(forProperty("persona"), inits.get("persona")) : null;
    }

}

