INSERT INTO `documento_tipo`
(`id`, `descripcion`, `descripcion_reducida`)
VALUES
(1, 'DOCUMENTO NACIONAL DE IDENTIDAD', 'DNI'),
(2, 'CUIT', 'CUIT'),
(3, 'CUIL', 'CUIL'),
(4, 'LIBRETA DE ENROLAMIENTO', 'LE'),
(5, 'LIBRETA CIVICA', 'LC');
