CREATE TABLE `domicilio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(100) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `dpto` varchar(5) DEFAULT NULL,
  `nro` varchar(10) NOT NULL,
  `piso` varchar(5) DEFAULT NULL,
  `principal` binary(1) NOT NULL,
  `ciudad_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_awmukvaqv2hi7477trn3009l0` (`ciudad_id`),
  KEY `FK_1593medxgoaaqn9ftrghtim3k` (`persona_id`),
  CONSTRAINT `FK_1593medxgoaaqn9ftrghtim3k` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `FK_awmukvaqv2hi7477trn3009l0` FOREIGN KEY (`ciudad_id`) REFERENCES `ciudad` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
