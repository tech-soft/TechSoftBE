CREATE TABLE `pago_transferencia` (
  `id` int(11) NOT NULL,
  `cuenta_bancaria` varchar(100) NOT NULL,
  `banco_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pago_transferencia_banco_fk_idx` (`banco_id`),
  CONSTRAINT `pago_transferencia_pago_fk` FOREIGN KEY (`id`) REFERENCES `pago` (`id`),
  CONSTRAINT `pago_transferencia_banco_fk` FOREIGN KEY (`banco_id`) REFERENCES `banco` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
