CREATE TABLE `remito_proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comprobante_tipo_id` int(11) NOT NULL,
  `puesto_id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `remito_proveedor_puesto_fk_idx` (`puesto_id`),
  KEY `remito_proveedor_proveedor_fk_idx` (`proveedor_id`),
  KEY `remito_proveedor_comprobante_tipo_fk_idx` (`comprobante_tipo_id`),
  CONSTRAINT `remito_proveedor_comprobante_tipo_fk` FOREIGN KEY (`comprobante_tipo_id`) REFERENCES `comprobante_tipo` (`id`),
  CONSTRAINT `remito_proveedor_proveedor_fk` FOREIGN KEY (`proveedor_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `remito_proveedor_puesto_fk` FOREIGN KEY (`puesto_id`) REFERENCES `puesto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
