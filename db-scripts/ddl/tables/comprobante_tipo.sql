CREATE TABLE `comprobante_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `letra` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `letra_UNIQUE` (`letra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
