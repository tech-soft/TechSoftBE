CREATE TABLE `persona_comercial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) NOT NULL,
  `iva_tipo_id` int(11) DEFAULT NULL,
  `pago_forma_id` int(11) DEFAULT NULL,
  `precio_lista_id` int(11) DEFAULT NULL,
  `descuento` decimal(5,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `comercial_persona_fk_idx` (`persona_id`),
  KEY `comercial_iva_tipo_fk_idx` (`iva_tipo_id`),
  KEY `comercial_precio_lista_fk_idx` (`precio_lista_id`),
  KEY `comercial_pago_forma_fk_idx` (`pago_forma_id`),
  CONSTRAINT `comercial_iva_tipo_fk` FOREIGN KEY (`iva_tipo_id`) REFERENCES `iva_tipo` (`id`),
  CONSTRAINT `comercial_pago_forma_fk` FOREIGN KEY (`pago_forma_id`) REFERENCES `pago_forma` (`id`),
  CONSTRAINT `comercial_persona_fk` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `comercial_precio_lista_fk` FOREIGN KEY (`precio_lista_id`) REFERENCES `precio_lista` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
