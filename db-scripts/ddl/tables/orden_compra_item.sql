CREATE TABLE `orden_compra_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(9,2) DEFAULT NULL,
  `estado_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orden_item_producto_fk_idx` (`producto_id`),
  KEY `orden_item_estado_fk_idx` (`estado_id`),
  KEY `orden_item_orden_fk_idx` (`orden_id`),
  CONSTRAINT `orden_item_estado_fk` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id`),
  CONSTRAINT `orden_item_orden_fk` FOREIGN KEY (`orden_id`) REFERENCES `orden_compra` (`id`),
  CONSTRAINT `orden_item_producto_fk` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

