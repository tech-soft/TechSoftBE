CREATE TABLE `factura_proveedor_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factura_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `iva_tipo_id` int(11) NOT NULL,
  `precio_unitario_sin_iva` decimal(13,2) NOT NULL,
  `valor_unitario_iva` decimal(13,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `factura_proveedor_item_factura_fk_idx` (`factura_id`),
  KEY `factura_proveedor_item_producto_fk_idx` (`producto_id`),
  KEY `factura_proveedor_iva_tipo_fk_idx` (`iva_tipo_id`),
  CONSTRAINT `factura_proveedor_item_factura_fk` FOREIGN KEY (`factura_id`) REFERENCES `factura_proveedor` (`id`),
  CONSTRAINT `factura_proveedor_item_producto_fk` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`),
  CONSTRAINT `factura_proveedor_iva_tipo_fk` FOREIGN KEY (`iva_tipo_id`) REFERENCES `iva_tipo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
