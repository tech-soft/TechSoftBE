CREATE TABLE `pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(11) DEFAULT NULL,
  `pago_forma_id` int(11) NOT NULL,
  `moneda_id` int(11) NOT NULL,
  `fecha_emision` date NOT NULL,
  `importe` decimal(13,2) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pago_proveedor_fk_idx` (`proveedor_id`),
  KEY `pago_forma_pago_fk_idx` (`pago_forma_id`),
  KEY `pago_moneda_fk_idx` (`moneda_id`),
  CONSTRAINT `pago_moneda_fk` FOREIGN KEY (`moneda_id`) REFERENCES `moneda` (`id`),
  CONSTRAINT `pago_pago_forma_fk` FOREIGN KEY (`pago_forma_id`) REFERENCES `pago_forma` (`id`),
  CONSTRAINT `pago_proveedor_fk` FOREIGN KEY (`proveedor_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
