CREATE TABLE `remito_proveedor_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remito_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `remito_proveedor_item_producto_fk_idx` (`producto_id`),
  KEY `remito_proveedor_item_remito_fk_idx` (`remito_id`),
  CONSTRAINT `remito_proveedor_item_remito_fk` FOREIGN KEY (`remito_id`) REFERENCES `remito_proveedor` (`id`),
  CONSTRAINT `remito_proveedor_item_producto_fk` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
