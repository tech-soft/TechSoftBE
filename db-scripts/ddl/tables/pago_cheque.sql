CREATE TABLE `pago_cheque` (
  `id` int(11) NOT NULL,
  `emisor_id` int(11) NOT NULL,
  `numero` varchar(100) NOT NULL,
  `banco_id` int(11) NOT NULL,
  `fecha_pago` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `emisor_persona_fk_idx` (`emisor_id`),
  KEY `pago_cheque_banco_fk_idx` (`banco_id`),
  CONSTRAINT `pago_cheque_pago_fk` FOREIGN KEY (`id`) REFERENCES `pago` (`id`),
  CONSTRAINT `pago_cheque_banco_fk` FOREIGN KEY (`banco_id`) REFERENCES `banco` (`id`),
  CONSTRAINT `pago_cheque_persona_fk` FOREIGN KEY (`emisor_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
