CREATE TABLE `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `documento_tipo_id` int(11) DEFAULT NULL,
  `documento_numero` int(18) DEFAULT NULL,
  `estado_id` int(11) NOT NULL,
  `cliente` binary(1) NOT NULL DEFAULT '0',
  `proveedor` binary(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `persona_docuento_tipo_fk_idx` (`documento_tipo_id`),
  KEY `persona_estado_fk_idx` (`estado_id`),
  CONSTRAINT `persona_documento_tipo_fk` FOREIGN KEY (`documento_tipo_id`) REFERENCES `documento_tipo` (`id`),
  CONSTRAINT `persona_estado_fk` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
