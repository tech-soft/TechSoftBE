CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `logo` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_UNIQUE` (`username`),
  KEY `fk_usuario_persona_idx` (`persona_id`),
  CONSTRAINT `fk_usuario_persona_fk` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
