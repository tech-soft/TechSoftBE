CREATE TABLE `producto_serie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remito_id` int(11) NOT NULL,
  `numero_serie` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `producto_serie_remito_fk_idx` (`remito_id`),
  CONSTRAINT `producto_serie_remito_fk` FOREIGN KEY (`remito_id`) REFERENCES `remito_proveedor_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
