CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `puesto_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sucursal_puesto_fk_idx` (`puesto_id`),
  CONSTRAINT `sucursal_puesto_fk` FOREIGN KEY (`puesto_id`) REFERENCES `puesto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
