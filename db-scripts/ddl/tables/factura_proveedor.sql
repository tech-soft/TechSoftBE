CREATE TABLE `factura_proveedor` (
  `id` int(11) NOT NULL,
  `comprobante_tipo_id` int(11) NOT NULL,
  `puesto_id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `remito_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `factura_proveedor_comprabante_tipo_fk_idx` (`comprobante_tipo_id`),
  KEY `factura_proveedor_puesto_fk_idx` (`puesto_id`),
  KEY `factura_proveedor_proveedor_fk_idx` (`proveedor_id`),
  KEY `factura_proveedor_remito_fk_idx` (`remito_id`),
  CONSTRAINT `factura_proveedor_comprabante_tipo_fk` FOREIGN KEY (`comprobante_tipo_id`) REFERENCES `comprobante_tipo` (`id`),
  CONSTRAINT `factura_proveedor_puesto_fk` FOREIGN KEY (`puesto_id`) REFERENCES `puesto` (`id`),
  CONSTRAINT `factura_proveedor_proveedor_fk` FOREIGN KEY (`proveedor_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `factura_proveedor_remito_fk` FOREIGN KEY (`remito_id`) REFERENCES `remito_proveedor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
