CREATE TABLE `usuario_grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_roles_usuarios_idx` (`usuario_id`),
  KEY `usuario_roles_usuario_rol_tipos_fk_idx` (`grupo_id`),
  CONSTRAINT `usuario_grupos_sys_usuario_grupos_fk` FOREIGN KEY (`grupo_id`) REFERENCES `sys_usuario_grupos` (`id`),
  CONSTRAINT `usuario_grupos_usuarios_fk` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
