CREATE TABLE `orden_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `puesto_id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_entrega` datetime DEFAULT NULL,
  `estado_id` int(11) NOT NULL,
  `observaciones` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orden_proveedor_fk_idx` (`proveedor_id`),
  KEY `orden_estado_fk_idx` (`estado_id`),
  KEY `orden_puesto_fk_idx` (`puesto_id`),
  CONSTRAINT `orden_estado_fk` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id`),
  CONSTRAINT `orden_proveedor_fk` FOREIGN KEY (`proveedor_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `orden_puesto_fk` FOREIGN KEY (`puesto_id`) REFERENCES `puesto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;


