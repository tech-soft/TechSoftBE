CREATE TABLE `ciudad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_postal` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `provincia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_nucuwpeey5l4e28qxcsonh5x4` (`provincia_id`),
  CONSTRAINT `FK_nucuwpeey5l4e28qxcsonh5x4` FOREIGN KEY (`provincia_id`) REFERENCES `provincia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
