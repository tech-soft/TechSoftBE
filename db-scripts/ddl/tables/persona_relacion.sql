CREATE TABLE `persona_relacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_ppal_id` int(11) NOT NULL,
  `persona_relacionada_id` int(11) NOT NULL,
  `relacion_tipo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_3lbvo2u84inshhn46kg9qwtpo` (`persona_ppal_id`),
  KEY `FK_qq48wawe6rv0u8c9d6skax4e` (`persona_relacionada_id`),
  KEY `FK_tln16rd2i80klcmsh9165x2iu` (`relacion_tipo_id`),
  CONSTRAINT `FK_3lbvo2u84inshhn46kg9qwtpo` FOREIGN KEY (`persona_ppal_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `FK_qq48wawe6rv0u8c9d6skax4e` FOREIGN KEY (`persona_relacionada_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `FK_tln16rd2i80klcmsh9165x2iu` FOREIGN KEY (`relacion_tipo_id`) REFERENCES `persona_relacion_tipo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
