CREATE TABLE `contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contacto` varchar(100) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `persona_id` int(11) NOT NULL,
  `contacto_tipo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_eonx666n8pf166wj4v7gqofcx` (`persona_id`),
  KEY `contacto_contacto_tipo_fk_idx` (`contacto_tipo_id`),
  CONSTRAINT `contacto_contacto_tipo_fk` FOREIGN KEY (`contacto_tipo_id`) REFERENCES `sys_contacto_tipo` (`id`),
  CONSTRAINT `FK_eonx666n8pf166wj4v7gqofcx` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
