package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.PaisDto;
import com.guilleavi.techsoftapp.entities.PaisEntity;
import com.guilleavi.techsoftapp.repositories.PaisRepository;

@Transactional(timeout = 300)
public class PaisServiceTest extends AbstractTest {

	@Autowired
	PaisService paisService = null;
	@Autowired
	PaisRepository paisRepository = null;

	@Test
	@Rollback
	public void getPaisConIdNullTest() {

		Integer idNull = null;

		try {
			paisService.getPais(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de pais no puede ser nulo.",
					e.getMessage().equals("El 'id' de pais no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getPaisConIdTest() {

		// datos de prueba
		PaisEntity paisTest = new PaisEntity();
		paisTest.setNombre("PAIS TEST");
		Integer idTest = paisRepository.save(paisTest).getId();

		// test get pais by id
		try {
			PaisDto paisDto = paisService.getPais(idTest);
			assertTrue("El id del pais obtenido debe coincidir con el filtro", paisDto.getId().equals(idTest));
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void insertPaisConPaisNullTest() {

		PaisDto paisNull = null;

		try {
			paisService.insert(paisNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El pais a insertar no se encuentra definido.",
					e.getMessage().equals("El pais a insertar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void insertPaisYaExistenteTest() {

		// datos de prueba
		PaisEntity paisTest = new PaisEntity();
		paisTest.setNombre("PAIS TEST");
		Integer idTest = paisRepository.save(paisTest).getId();

		PaisDto paisExistente = new PaisDto();
		paisExistente.setId(idTest);

		try {
			paisService.insert(paisExistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del pais a insertar ya existe en la base de datos.",
					e.getMessage().equals("El 'id' del pais a insertar ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void updatePaisConPaisNullTest() {

		PaisDto paisNull = null;

		try {
			paisService.update(paisNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El pais a modificar no se encuentra definido.",
					e.getMessage().equals("El pais a modificar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void updatePaisNoPreexistenteTest() {

		// datos de prueba
		Integer idTest = 999;
		if (paisRepository.findOne(idTest) != null) {
			paisRepository.delete(idTest);
		}

		PaisDto paisNoPreexistente = new PaisDto();
		paisNoPreexistente.setId(idTest);

		try {
			paisService.update(paisNoPreexistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del pais que desea modificar no existe en la base de datos.",
					e.getMessage().equals("El 'id' del pais que desea modificar no existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void deletePaisConIdNullTest() {

		Integer idNull = null;

		try {
			paisService.delete(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de pais no puede ser nulo.",
					e.getMessage().equals("El 'id' de pais no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void deletePaisNoExistenteTest() {

		Integer idInexistente = 30000;

		try {
			paisService.delete(idInexistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El pais que se desea eliminar no existe.",
					e.getMessage().equals("El pais que se desea eliminar no existe."));
		}
	}

}
