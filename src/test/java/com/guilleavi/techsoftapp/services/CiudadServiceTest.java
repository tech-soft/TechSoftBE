package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.CiudadDto;
import com.guilleavi.techsoftapp.entities.CiudadEntity;
import com.guilleavi.techsoftapp.entities.PaisEntity;
import com.guilleavi.techsoftapp.entities.ProvinciaEntity;
import com.guilleavi.techsoftapp.repositories.CiudadRepository;
import com.guilleavi.techsoftapp.repositories.ProvinciaRepository;

@Transactional(timeout = 300)
public class CiudadServiceTest extends AbstractTest {

	@Autowired
	CiudadService ciudadService = null;
	@Autowired
	CiudadRepository ciudadRepository = null;
	@Autowired
	ProvinciaRepository provinciaRepository = null;

	@Test
	@Rollback
	public void getCiudadConIdNullTest() {

		Integer idNull = null;

		try {
			ciudadService.getCiudad(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de ciudad no puede ser nulo.",
					e.getMessage().equals("El 'id' de ciudad no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getCiudadConIdTest() {

		// datos de prueba
		CiudadEntity ciudadTest = new CiudadEntity();
		ciudadTest.setNombre("CIUDAD TEST");
		ciudadTest.setCodigoPostal("2200");
		ciudadTest.setProvincia(new ProvinciaEntity(1, "SANTA FE", new PaisEntity(1, "ARGENTINA")));
		Integer idTest = ciudadRepository.save(ciudadTest).getId();

		// test get ciudad by id
		try {
			CiudadDto ciudadDto = ciudadService.getCiudad(idTest);
			assertTrue("El id de la ciudad obtenida debe coincidir con el filtro", ciudadDto.getId().equals(idTest));
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getCiudadesConProvinciaIdNullTest() {

		Integer provinciaIdNull = null;

		try {
			ciudadService.getCiudadesByProvincia(provinciaIdNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de provincia no puede ser nulo.",
					e.getMessage().equals("El 'id' de provincia no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getCiudadesConProvinciaIdTest() {

		// datos de prueba
		ProvinciaEntity provinciaTest = new ProvinciaEntity();
		provinciaTest.setNombre("PROVINCIA TEST");
		provinciaTest.setPais(new PaisEntity(1, "ARGENTINA"));
		Integer idTest = provinciaRepository.save(provinciaTest).getId();

		CiudadEntity ciudadTest = new CiudadEntity();
		ciudadTest.setNombre("CIUDAD TEST");
		ciudadTest.setCodigoPostal("2200");
		ciudadTest.setProvincia(provinciaTest);
		ciudadRepository.save(ciudadTest);

		// test get ciudad by provincia id
		try {
			List<CiudadDto> ciudadDtos = ciudadService.getCiudadesByProvincia(idTest);

			Long matchedCount = ciudadDtos.stream().filter(c -> c.getProvincia().getId().equals(idTest)).count();

			assertTrue("Los id de provincia deben coincidir con el filtro", ciudadDtos.size() == matchedCount);

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void insertCiudadConCiudadNullTest() {

		CiudadDto ciudadNull = null;

		try {
			ciudadService.insert(ciudadNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La ciudad a insertar no se encuentra definida.",
					e.getMessage().equals("La ciudad a insertar no se encuentra definida."));
		}
	}

	@Test
	@Rollback
	public void insertCiudadYaExistenteTest() {

		// datos de prueba
		CiudadEntity ciudadTest = new CiudadEntity();
		ciudadTest.setNombre("CIUDAD TEST");
		ciudadTest.setCodigoPostal("2200");
		ciudadTest.setProvincia(new ProvinciaEntity(1, "SANTA FE", new PaisEntity(1, "ARGENTINA")));
		Integer idTest = ciudadRepository.save(ciudadTest).getId();

		CiudadDto ciudadExistente = new CiudadDto();
		ciudadExistente.setId(idTest);

		try {
			ciudadService.insert(ciudadExistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de la ciudad a insertar ya existe en la base de datos.",
					e.getMessage().equals("El 'id' de la ciudad a insertar ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void updateCiudadConCiudadNullTest() {

		CiudadDto ciudadNull = null;

		try {
			ciudadService.update(ciudadNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La ciudad a modificar no se encuentra definida.",
					e.getMessage().equals("La ciudad a modificar no se encuentra definida."));
		}
	}

	@Test
	@Rollback
	public void updateCiudadNoPreexistenteTest() {

		// datos de prueba
		Integer idTest = 999;
		if (ciudadRepository.findOne(idTest) != null) {
			ciudadRepository.delete(idTest);
		}

		CiudadDto ciudadNoPreexistente = new CiudadDto();
		ciudadNoPreexistente.setId(idTest);

		try {
			ciudadService.update(ciudadNoPreexistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de la ciudad que desea modificar no existe en la base de datos.",
					e.getMessage().equals("El 'id' de la ciudad que desea modificar no existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void deleteCiudadConIdNullTest() {

		Integer idNull = null;

		try {
			ciudadService.delete(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de ciudad no puede ser nulo.",
					e.getMessage().equals("El 'id' de ciudad no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void deleteCiudadNoExistenteTest() {

		Integer idInexistente = 30000;

		try {
			ciudadService.delete(idInexistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La ciudad que se desea eliminar no existe.",
					e.getMessage().equals("La ciudad que se desea eliminar no existe."));
		}
	}

}
