package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.DocumentoTipoDto;
import com.guilleavi.techsoftapp.dto.EstadoDto;
import com.guilleavi.techsoftapp.dto.PersonaDto;
import com.guilleavi.techsoftapp.dto.PersonaResumenDto;
import com.guilleavi.techsoftapp.entities.ContactoEntity;
import com.guilleavi.techsoftapp.entities.DomicilioEntity;
import com.guilleavi.techsoftapp.entities.EstadoEntity;
import com.guilleavi.techsoftapp.entities.PersonaComercialEntity;
import com.guilleavi.techsoftapp.entities.PersonaEntity;
import com.guilleavi.techsoftapp.entities.PersonaRelacionTipoEntity;
import com.guilleavi.techsoftapp.entities.PersonaRelacionadaEntity;
import com.guilleavi.techsoftapp.entities.SysContactoTipoEntity;
import com.guilleavi.techsoftapp.repositories.CiudadRepository;
import com.guilleavi.techsoftapp.repositories.ContactoRepository;
import com.guilleavi.techsoftapp.repositories.DocumentoTipoRepository;
import com.guilleavi.techsoftapp.repositories.DomicilioRepository;
import com.guilleavi.techsoftapp.repositories.PersonaComercialRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRelacionadaRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;

@Transactional(timeout = 300)
public class PersonaServiceTest extends AbstractTest {

	@Autowired
	PersonaService personaService = null;
	
	@Autowired
	DocumentoTipoRepository documentoTipoRepository = null;
	@Autowired
	CiudadRepository ciudadRepository = null;
	@Autowired
	ContactoRepository contactoRepository = null;
	@Autowired
	DomicilioRepository domicilioRepository = null;
	@Autowired
	PersonaRepository personaRepository = null;
	@Autowired
	PersonaComercialRepository personaComercialRepository = null;
	@Autowired
	PersonaRelacionadaRepository personaRelacionadaRepository = null;

	@Test
	@Rollback
	public void getPersonaByKeyword_Existente_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaRepository.save(personaTemp);

		List<PersonaResumenDto> results = personaService.getPersonasByKeyword("x1b2c3");
		PersonaResumenDto result = results.get(0);

		assertEquals("Debe coincider el nombre", "Test x1b2c3", result.getNombre());
		assertEquals("No debe existir domicilio principal", result.getDomicilioPrincipal(), "");
		assertEquals("No debe existir teléfono principal", result.getTelefonoPrincipal(), "");
	}

	@Test
	@Rollback
	public void getPersonaByKeyword_NoExistente_Test() {

		List<PersonaResumenDto> results = personaService.getPersonasByKeyword("x1b2cc3");

		assertEquals("No deben obtenerse resultados", results.size(), 0);
	}

	@Test
	@Rollback
	public void getPersona_NoExistente_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		PersonaDto result = personaService.getPersona(personaTemp.getId() + 1);

		assertTrue("No debe obtenerse ningún resultado", result == null);
	}

	@Test
	@Rollback
	public void getPersona_Existente_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		PersonaDto result = personaService.getPersona(personaTemp.getId());

		assertTrue("Debe obtenerse un resultado", result != null);
		assertTrue("Debe coincidir el id", result.getId() == personaTemp.getId());
	}

	@Test
	@Rollback
	public void insertPersona_SinDefinir_Test() {

		try {
			personaService.insert(null);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La persona a insertar no se encuentra definida.",
					e.getMessage().equals("La persona a insertar no se encuentra definida."));
		}
	}

	@Test
	@Rollback
	public void insertPersona_IdExistente_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		try {
			PersonaDto personaDto = new PersonaDto();
			personaDto.setId(personaTemp.getId());
			personaService.insert(personaDto);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de la persona a insertar ya existe en la base de datos.",
					e.getMessage().equals("El 'id' de la persona a insertar ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void insertPersona_SinNombre_Test() {

		try {
			PersonaDto personaTemp = new PersonaDto();
			personaService.insert(personaTemp);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo nombre es obligatorio.", e.getMessage().equals("El campo nombre es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertPersona_DocumentoConTipoYSinNumero_Test() {

		try {
			PersonaDto personaTemp = new PersonaDto();
			personaTemp.setNombre("TEST");
			personaTemp.setDocumentoTipo(new DocumentoTipoDto(1, "DNI", "DNI"));
			personaService.insert(personaTemp);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("Los campos tipo y número de documento deben ser ambos nulos o ambos estar definidos.",
					e.getMessage().equals(
							"Los campos tipo y número de documento deben ser ambos nulos o ambos estar definidos."));
		}
	}

	@Test
	@Rollback
	public void insertPersona_DocumentoSinTipoYConNumero_Test() {

		try {
			PersonaDto personaTemp = new PersonaDto();
			personaTemp.setNombre("TEST");
			personaTemp.setDocumentoNumero(12345678L);
			personaService.insert(personaTemp);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("Los campos tipo y número de documento deben ser ambos nulos o ambos estar definidos.",
					e.getMessage().equals(
							"Los campos tipo y número de documento deben ser ambos nulos o ambos estar definidos."));
		}
	}

	@Test
	@Rollback
	public void insertPersona_TodosLosDatosCompletos_Test() {

		try {
			PersonaDto personaTemp = new PersonaDto();
			personaTemp.setNombre("TEST");
			personaTemp.setDocumentoTipo(new DocumentoTipoDto(1, "DOCUMENTO NACIONAL DE IDENTIDAD", "DNI"));
			personaTemp.setDocumentoNumero(12345678L);
			personaTemp.setProveedor(true);
			personaTemp.setCliente(true);
			personaTemp.setEstado(new EstadoDto(1, "Activo"));
			PersonaDto result = personaService.insert(personaTemp);

			assertTrue("Debe obtenerse la persona guardada.", result != null);
			assertTrue("El nombre debe ser TEST.", result.getNombre().equals("TEST"));
			assertTrue("El tipo de documento debe ser DNI.",
					result.getDocumentoTipo().equals(new DocumentoTipoDto(1, "DOCUMENTO NACIONAL DE IDENTIDAD", "DNI")));
			assertTrue("El número de documento debe ser 12345678.", result.getDocumentoNumero().equals(12345678L));
			assertTrue("El proveedor debe ser true.", result.getProveedor() == true);
			assertTrue("El cliente debe ser true.", result.getCliente() == true);
			assertTrue("El estado debe ser Activo.", result.getEstado().equals(new EstadoDto(1, "Activo")));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void updatePersona_SinDefinir_Test() {

		try {
			personaService.update(null);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La persona a modificar no se encuentra definida.",
					e.getMessage().equals("La persona a modificar no se encuentra definida."));
		}
	}

	@Test
	@Rollback
	public void updatePersona_NoExistente_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		try {
			PersonaDto personaDto = new PersonaDto();
			personaDto.setId(personaTemp.getId() + 1);

			personaService.update(personaDto);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de la persona que desea modificar no existe en la base de datos.",
					e.getMessage().equals("El 'id' de la persona que desea modificar no existe en la base de datos."));
		}

	}

	@Test
	@Rollback
	public void updatePersona_TodosLosDatos_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test 111111");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		try {
			PersonaDto personaDto = new PersonaDto();
			personaDto.setId(personaTemp.getId());
			personaDto.setNombre("Test 222222");
			personaDto.setDocumentoTipo(new DocumentoTipoDto(documentoTipoRepository.findOne(1)));
			personaDto.setDocumentoNumero(12345678L);
			personaDto.setProveedor(true);
			personaDto.setCliente(true);
			personaDto.setEstado(new EstadoDto(2, "Inactivo"));

			PersonaDto result = personaService.update(personaDto);
			assertTrue("Se debe devolver la persona modificada.", result != null);
			assertTrue("El nombre debe ser Test 222222.", result.getNombre().equals("Test 222222"));
			assertTrue("El tipo de documento debe ser DNI.",
					result.getDocumentoTipo().equals(new DocumentoTipoDto(documentoTipoRepository.findOne(1))));
			assertTrue("El número de documento debe ser 12345678.", result.getDocumentoNumero().equals(12345678L));
			assertTrue("El proveedor debe ser true.", result.getProveedor() == true);
			assertTrue("El cliente debe ser true.", result.getCliente() == true);
			assertTrue("El estado debe ser inactivo.", result.getEstado().equals(new EstadoDto(2, "Inactivo")));

		} catch (Exception e) {
			fail("Should not throw exception");
		}

	}

	@Test
	@Rollback
	public void deletePersona_SinId_Test() {

		try {

			personaService.delete(null);

		} catch (Exception e) {
			assertTrue("El 'id' de persona no puede ser nulo.",
					e.getMessage().equals("El 'id' de persona no puede ser nulo."));
		}

	}

	@Test
	@Rollback
	public void deletePersona_SinDatosAccesorios_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test 111111");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		try {

			personaService.delete(personaTemp.getId());

			PersonaEntity result = personaRepository.findOne(personaTemp.getId());
			assertTrue("La persona debe haberse eliminado", result == null);

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void deletePersona_ConDatosAccesorios_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test 111111");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		PersonaComercialEntity personaComercialTemp = new PersonaComercialEntity();
		personaComercialTemp.setPersona(personaTemp);
		personaComercialTemp.setDescuento(10.00);
		personaComercialTemp = personaComercialRepository.save(personaComercialTemp);

		DomicilioEntity domicilioTemp = new DomicilioEntity();
		domicilioTemp.setPersona(personaTemp);
		domicilioTemp.setCalle("CALLE TEST");
		domicilioTemp.setNro("1234");
		domicilioTemp.setCiudad(ciudadRepository.findOne(1));
		domicilioTemp.setPrincipal(false);
		domicilioTemp = domicilioRepository.save(domicilioTemp);

		ContactoEntity contactoTemp = new ContactoEntity();
		contactoTemp.setPersona(personaTemp);
		contactoTemp.setContacto("1111-2222");
		contactoTemp.setTipo(new SysContactoTipoEntity(1, "TELEFONO"));
		contactoTemp = contactoRepository.save(contactoTemp);

		PersonaRelacionadaEntity personaRelacionadaTemp = new PersonaRelacionadaEntity();
		personaRelacionadaTemp.setPersonaPrincipal(personaTemp);
		personaRelacionadaTemp.setPersonaRelacionada(personaTemp);
		personaRelacionadaTemp.setTipo(new PersonaRelacionTipoEntity(1, "FAMILIAR"));
		personaRelacionadaTemp = personaRelacionadaRepository.save(personaRelacionadaTemp);

		try {

			personaService.delete(personaTemp.getId());

			PersonaEntity personaResult = personaRepository.findOne(personaTemp.getId());
			PersonaComercialEntity personaComercialResult = personaComercialRepository
					.findOne(personaComercialTemp.getId());
			DomicilioEntity domicilioResult = domicilioRepository.findOne(domicilioTemp.getId());
			ContactoEntity contactoResult = contactoRepository.findOne(contactoTemp.getId());
			PersonaRelacionadaEntity personaRelacionadaResult = personaRelacionadaRepository
					.findOne(personaRelacionadaTemp.getId());

			assertTrue("La persona debe haberse eliminado", personaResult == null);
			assertTrue("Los datos comerciales deben haberse eliminado", personaComercialResult == null);
			assertTrue("El domicilio debe haberse eliminado", domicilioResult == null);
			assertTrue("El contacto debe haberse eliminado", contactoResult == null);
			assertTrue("La persona relacionada debe haberse eliminado", personaRelacionadaResult == null);

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}
}
