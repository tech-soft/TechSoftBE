package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.entities.ProductoStockEntity;
import com.guilleavi.techsoftapp.entities.QProductoStockEntity;
import com.guilleavi.techsoftapp.repositories.ProductoStockRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Transactional(timeout = 300)
public class ProductoStockServiceTest extends AbstractTest {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	ProductoStockService productoStockService = null;
	@Autowired
	ProductoStockRepository productoStockRepository = null;

	@Test
	@Rollback
	public void editStockEntranteSumaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = true;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockEntrante(productoId, cantidad, suma);

		ProductoStockEntity stockPosterior = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("Se debe actualizar el stock entrante",
				(stockPosterior.getStockEntrante() - ((stockPrevio != null && stockPrevio.getStockEntrante() != null)
						? stockPrevio.getStockEntrante()
						: 0)) == cantidad);

		assertTrue("No se debe modificar el stock disponible", (stockPosterior.getStockDisponible()
				- ((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock transito",
				(stockPosterior.getStockTransito() - ((stockPrevio != null && stockPrevio.getStockTransito() != null)
						? stockPrevio.getStockTransito()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock reservado",
				(stockPosterior.getStockReservado() - ((stockPrevio != null && stockPrevio.getStockReservado() != null)
						? stockPrevio.getStockReservado()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock vendido",
				(stockPosterior.getStockVendido() - ((stockPrevio != null && stockPrevio.getStockVendido() != null)
						? stockPrevio.getStockVendido()
						: 0)) == 0);

	}

	@Test
	@Rollback
	public void editStockEntranteRestaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = false;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockEntrante(productoId, cantidad, true);
		productoStockService.editStockEntrante(productoId, cantidad, suma);

		ProductoStockEntity stockCancelado = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("Se debe revertir el stock entrante",
				((stockPrevio != null && stockPrevio.getStockEntrante() != null) ? stockPrevio.getStockEntrante() : 0)
						- stockCancelado.getStockEntrante() == 0);

		assertTrue("No se debe modificar el stock disponible",
				((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0) - stockCancelado.getStockDisponible() == 0);

		assertTrue("No se debe modificar el stock transito",
				((stockPrevio != null && stockPrevio.getStockTransito() != null) ? stockPrevio.getStockTransito() : 0)
						- stockCancelado.getStockTransito() == 0);

		assertTrue("No se debe modificar el stock reservado",
				((stockPrevio != null && stockPrevio.getStockReservado() != null) ? stockPrevio.getStockReservado() : 0)
						- stockCancelado.getStockReservado() == 0);

		assertTrue("No se debe modificar el stock vendido",
				((stockPrevio != null && stockPrevio.getStockVendido() != null) ? stockPrevio.getStockVendido() : 0)
						- stockCancelado.getStockVendido() == 0);
	}

	@Test
	@Rollback
	public void editStockDisponibleSumaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = true;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockDisponible(productoId, cantidad, suma);

		ProductoStockEntity stockPosterior = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("No se debe modificar el stock entrante",
				(stockPosterior.getStockEntrante() - ((stockPrevio != null && stockPrevio.getStockEntrante() != null)
						? stockPrevio.getStockEntrante()
						: 0)) == 0);

		assertTrue("Se debe actualizar el stock disponible", (stockPosterior.getStockDisponible()
				- ((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0)) == cantidad);

		assertTrue("No se debe modificar el stock transito",
				(stockPosterior.getStockTransito() - ((stockPrevio != null && stockPrevio.getStockTransito() != null)
						? stockPrevio.getStockTransito()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock reservado",
				(stockPosterior.getStockReservado() - ((stockPrevio != null && stockPrevio.getStockReservado() != null)
						? stockPrevio.getStockReservado()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock vendido",
				(stockPosterior.getStockVendido() - ((stockPrevio != null && stockPrevio.getStockVendido() != null)
						? stockPrevio.getStockVendido()
						: 0)) == 0);

	}

	@Test
	@Rollback
	public void editStockDisponibleRestaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = false;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockDisponible(productoId, cantidad, true);
		productoStockService.editStockDisponible(productoId, cantidad, suma);

		ProductoStockEntity stockCancelado = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("No se debe modificar el stock entrante",
				((stockPrevio != null && stockPrevio.getStockEntrante() != null) ? stockPrevio.getStockEntrante() : 0)
						- stockCancelado.getStockEntrante() == 0);

		assertTrue("Se debe revertir el stock disponible",
				((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0) - stockCancelado.getStockDisponible() == 0);

		assertTrue("No se debe modificar el stock transito",
				((stockPrevio != null && stockPrevio.getStockTransito() != null) ? stockPrevio.getStockTransito() : 0)
						- stockCancelado.getStockTransito() == 0);

		assertTrue("No se debe modificar el stock reservado",
				((stockPrevio != null && stockPrevio.getStockReservado() != null) ? stockPrevio.getStockReservado() : 0)
						- stockCancelado.getStockReservado() == 0);

		assertTrue("No se debe modificar el stock vendido",
				((stockPrevio != null && stockPrevio.getStockVendido() != null) ? stockPrevio.getStockVendido() : 0)
						- stockCancelado.getStockVendido() == 0);
	}

	@Test
	@Rollback
	public void editStockTransitoSumaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = true;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockTransito(productoId, cantidad, suma);

		ProductoStockEntity stockPosterior = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("No se debe modificar el stock entrante",
				(stockPosterior.getStockEntrante() - ((stockPrevio != null && stockPrevio.getStockEntrante() != null)
						? stockPrevio.getStockEntrante()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock disponible", (stockPosterior.getStockDisponible()
				- ((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0)) == 0);

		assertTrue("Se debe actualizar el stock transito",
				(stockPosterior.getStockTransito() - ((stockPrevio != null && stockPrevio.getStockTransito() != null)
						? stockPrevio.getStockTransito()
						: 0)) == cantidad);

		assertTrue("No se debe modificar el stock reservado",
				(stockPosterior.getStockReservado() - ((stockPrevio != null && stockPrevio.getStockReservado() != null)
						? stockPrevio.getStockReservado()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock vendido",
				(stockPosterior.getStockVendido() - ((stockPrevio != null && stockPrevio.getStockVendido() != null)
						? stockPrevio.getStockVendido()
						: 0)) == 0);

	}

	@Test
	@Rollback
	public void editStockTransitoRestaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = false;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockTransito(productoId, cantidad, true);
		productoStockService.editStockTransito(productoId, cantidad, suma);

		ProductoStockEntity stockCancelado = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("No se debe modificar el stock entrante",
				((stockPrevio != null && stockPrevio.getStockEntrante() != null) ? stockPrevio.getStockEntrante() : 0)
						- stockCancelado.getStockEntrante() == 0);

		assertTrue("No se debe modificar el stock disponible",
				((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0) - stockCancelado.getStockDisponible() == 0);

		assertTrue("Se debe revertir el stock transito",
				((stockPrevio != null && stockPrevio.getStockTransito() != null) ? stockPrevio.getStockTransito() : 0)
						- stockCancelado.getStockTransito() == 0);

		assertTrue("No se debe modificar el stock reservado",
				((stockPrevio != null && stockPrevio.getStockReservado() != null) ? stockPrevio.getStockReservado() : 0)
						- stockCancelado.getStockReservado() == 0);

		assertTrue("No se debe modificar el stock vendido",
				((stockPrevio != null && stockPrevio.getStockVendido() != null) ? stockPrevio.getStockVendido() : 0)
						- stockCancelado.getStockVendido() == 0);
	}

	@Test
	@Rollback
	public void editStockReservadoSumaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = true;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockReservado(productoId, cantidad, suma);

		ProductoStockEntity stockPosterior = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("No se debe modificar el stock entrante",
				(stockPosterior.getStockEntrante() - ((stockPrevio != null && stockPrevio.getStockEntrante() != null)
						? stockPrevio.getStockEntrante()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock disponible", (stockPosterior.getStockDisponible()
				- ((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock transito",
				(stockPosterior.getStockTransito() - ((stockPrevio != null && stockPrevio.getStockTransito() != null)
						? stockPrevio.getStockTransito()
						: 0)) == 0);

		assertTrue("Se debe actualizar el stock reservado",
				(stockPosterior.getStockReservado() - ((stockPrevio != null && stockPrevio.getStockReservado() != null)
						? stockPrevio.getStockReservado()
						: 0)) == cantidad);

		assertTrue("No se debe modificar el stock vendido",
				(stockPosterior.getStockVendido() - ((stockPrevio != null && stockPrevio.getStockVendido() != null)
						? stockPrevio.getStockVendido()
						: 0)) == 0);
	}

	@Test
	@Rollback
	public void editStockReservadoRestaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = false;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockReservado(productoId, cantidad, true);
		productoStockService.editStockReservado(productoId, cantidad, suma);

		ProductoStockEntity stockCancelado = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("No se debe modificar el stock entrante",
				((stockPrevio != null && stockPrevio.getStockEntrante() != null) ? stockPrevio.getStockEntrante() : 0)
						- stockCancelado.getStockEntrante() == 0);

		assertTrue("No se debe modificar el stock disponible",
				((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0) - stockCancelado.getStockDisponible() == 0);

		assertTrue("No se debe modificar el stock transito",
				((stockPrevio != null && stockPrevio.getStockTransito() != null) ? stockPrevio.getStockTransito() : 0)
						- stockCancelado.getStockTransito() == 0);

		assertTrue("Se debe revertir el stock reservado",
				((stockPrevio != null && stockPrevio.getStockReservado() != null) ? stockPrevio.getStockReservado() : 0)
						- stockCancelado.getStockReservado() == 0);

		assertTrue("No se debe modificar el stock vendido",
				((stockPrevio != null && stockPrevio.getStockVendido() != null) ? stockPrevio.getStockVendido() : 0)
						- stockCancelado.getStockVendido() == 0);
	}

	@Test
	@Rollback
	public void editStockVendidoSumaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = true;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockVendido(productoId, cantidad, suma);

		ProductoStockEntity stockPosterior = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("No se debe modificar el stock entrante",
				(stockPosterior.getStockEntrante() - ((stockPrevio != null && stockPrevio.getStockEntrante() != null)
						? stockPrevio.getStockEntrante()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock disponible", (stockPosterior.getStockDisponible()
				- ((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock transito",
				(stockPosterior.getStockTransito() - ((stockPrevio != null && stockPrevio.getStockTransito() != null)
						? stockPrevio.getStockTransito()
						: 0)) == 0);

		assertTrue("No se debe modificar el stock reservado",
				(stockPosterior.getStockReservado() - ((stockPrevio != null && stockPrevio.getStockReservado() != null)
						? stockPrevio.getStockReservado()
						: 0)) == 0);

		assertTrue("Se debe actualizar el stock vendido",
				(stockPosterior.getStockVendido() - ((stockPrevio != null && stockPrevio.getStockVendido() != null)
						? stockPrevio.getStockVendido()
						: 0)) == cantidad);

	}

	@Test
	@Rollback
	public void editStockVendidoRestaTest() {

		Integer productoId = 1;
		Integer cantidad = 2;
		Boolean suma = false;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		productoStockService.editStockVendido(productoId, cantidad, true);
		productoStockService.editStockVendido(productoId, cantidad, suma);

		ProductoStockEntity stockCancelado = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();

		assertTrue("No se debe modificar el stock entrante",
				((stockPrevio != null && stockPrevio.getStockEntrante() != null) ? stockPrevio.getStockEntrante() : 0)
						- stockCancelado.getStockEntrante() == 0);

		assertTrue("No se debe modificar el stock disponible",
				((stockPrevio != null && stockPrevio.getStockDisponible() != null) ? stockPrevio.getStockDisponible()
						: 0) - stockCancelado.getStockDisponible() == 0);

		assertTrue("No se debe modificar el stock transito",
				((stockPrevio != null && stockPrevio.getStockTransito() != null) ? stockPrevio.getStockTransito() : 0)
						- stockCancelado.getStockTransito() == 0);

		assertTrue("No se debe modificar el stock reservado",
				((stockPrevio != null && stockPrevio.getStockReservado() != null) ? stockPrevio.getStockReservado() : 0)
						- stockCancelado.getStockReservado() == 0);

		assertTrue("Se debe revertir el stock vendido",
				((stockPrevio != null && stockPrevio.getStockVendido() != null) ? stockPrevio.getStockVendido() : 0)
						- stockCancelado.getStockVendido() == 0);
	}

}
