package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.EstadoDto;
import com.guilleavi.techsoftapp.dto.OrdenCompraDto;
import com.guilleavi.techsoftapp.dto.OrdenCompraItemDto;
import com.guilleavi.techsoftapp.dto.OrdenCompraPreviewDto;
import com.guilleavi.techsoftapp.dto.ProductoPreviewDto;
import com.guilleavi.techsoftapp.dto.ProveedorDto;
import com.guilleavi.techsoftapp.dto.PuestoDto;
import com.guilleavi.techsoftapp.entities.EstadoEntity;
import com.guilleavi.techsoftapp.entities.OrdenCompraEntity;
import com.guilleavi.techsoftapp.entities.OrdenCompraItemEntity;
import com.guilleavi.techsoftapp.entities.PersonaEntity;
import com.guilleavi.techsoftapp.entities.ProductoStockEntity;
import com.guilleavi.techsoftapp.entities.PuestoEntity;
import com.guilleavi.techsoftapp.entities.QOrdenCompraEntity;
import com.guilleavi.techsoftapp.entities.QOrdenCompraItemEntity;
import com.guilleavi.techsoftapp.entities.QProductoStockEntity;
import com.guilleavi.techsoftapp.enums.EstadoEnum;
import com.guilleavi.techsoftapp.repositories.EstadoRepository;
import com.guilleavi.techsoftapp.repositories.OrdenCompraItemRepository;
import com.guilleavi.techsoftapp.repositories.OrdenCompraRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;
import com.guilleavi.techsoftapp.repositories.ProductoRepository;
import com.guilleavi.techsoftapp.repositories.ProductoStockRepository;
import com.guilleavi.techsoftapp.repositories.PuestoRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Transactional(timeout = 300)
public class OrdenCompraServiceTest extends AbstractTest {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	EstadoRepository estadoRepository;
	@Autowired
	OrdenCompraRepository ordenCompraRepository;
	@Autowired
	OrdenCompraItemRepository ordenCompraItemRepository;
	@Autowired
	PersonaRepository personaRepository;
	@Autowired
	ProductoRepository productoRepository;
	@Autowired
	PuestoRepository puestoRepository;
	@Autowired
	ProductoStockRepository productoStockRepository;

	@Autowired
	OrdenCompraService ordenCompraService;

	@Test
	@Rollback
	public void getOrdenesPreviewTest() {

		OrdenCompraEntity ordenTest = new OrdenCompraEntity();
		PersonaEntity proveedor = personaRepository.findOne(1);

		ordenTest.setPuesto(new PuestoEntity(1, 1, "", new EstadoEntity(1, "ACTIVO")));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(proveedor);
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoEntity(1000, "NUEVA"));
		ordenCompraRepository.save(ordenTest);

		ordenTest = new OrdenCompraEntity();
		ordenTest.setPuesto(new PuestoEntity(1, 1, "", new EstadoEntity(1, "ACTIVO")));
		ordenTest.setNumero(2L);
		ordenTest.setProveedor(proveedor);
		ordenTest.setFechaCreacion(LocalDate.of(2018, 07, 01));
		ordenTest.setEstado(new EstadoEntity(1001, "EMITIDA"));
		ordenCompraRepository.save(ordenTest);

		// Buscar por proveedor
		List<OrdenCompraPreviewDto> ordenes = ordenCompraService.getOrdenesPreview(1, null, null, null, null);
		assertTrue("Se deben filtrar por proveedor",
				ordenes.stream().filter(orden -> orden.getProveedor().getId().equals(1)).count() == ordenes.size());

		// Buscar por fecha de creacion
		ordenes = ordenCompraService.getOrdenesPreview(null, LocalDate.of(2018, 06, 30), null, null, null);
		assertTrue("Se deben filtrar por fecha de creacion",
				ordenes.stream().filter(orden -> orden.getFechaCreacion().equals(LocalDate.of(2018, 06, 30)))
						.count() == ordenes.size());

		// Buscar por puesto
		ordenes = ordenCompraService.getOrdenesPreview(null, null, 1, null, null);
		assertTrue("Se deben filtrar por puesto",
				ordenes.stream().filter(orden -> orden.getPuesto().equals(1)).count() == ordenes.size());

		// Buscar por numero y puesto
		ordenes = ordenCompraService.getOrdenesPreview(null, null, 1, 2L, null);
		assertTrue("Se deben filtrar por numero y puesto", ordenes.stream()
				.filter(orden -> orden.getPuesto().equals(1) && orden.getNumero().equals(2L)).count() == ordenes.size());

		// Buscar por estado, numero y puesto
		ordenes = ordenCompraService.getOrdenesPreview(null, null, 1, 2L, 1001);
		assertTrue("Se deben filtrar por estado, numero y puesto",
				ordenes.stream().filter(
						orden -> orden.getPuesto().equals(1) && orden.getNumero().equals(2L) && orden.getEstado().getId().equals(1001))
						.count() == ordenes.size());

		// Sin filtros
		ordenes = ordenCompraService.getOrdenesPreview(null, null, null, null, null);
		assertTrue("Sin filtros no se deben devolver ordenes", ordenes == null);
	}

	@Test
	@Rollback
	public void getOrdenConIdNullTest() {

		Integer idNull = null;

		try {
			ordenCompraService.getOrden(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de orden no puede ser nulo.",
					e.getMessage().equals("El 'id' de orden no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getOrdenConItemsTest() {

		OrdenCompraEntity ordenTest = new OrdenCompraEntity();
		PersonaEntity proveedor = personaRepository.findOne(1);

		ordenTest.setPuesto(new PuestoEntity(1, 1, "", new EstadoEntity(1, "ACTIVO")));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(proveedor);
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoEntity(1000, "NUEVA"));
		ordenTest = ordenCompraRepository.save(ordenTest);

		OrdenCompraItemEntity itemTest = new OrdenCompraItemEntity();
		itemTest.setOrden(ordenTest);
		itemTest.setProducto(productoRepository.findOne(1));
		itemTest.setEstado(new EstadoEntity(1100, "ITEM NUEVO"));
		itemTest.setCantidad(1);
		ordenCompraItemRepository.save(itemTest);

		try {
			OrdenCompraDto ordenCompraDto = ordenCompraService.getOrden(ordenTest.getId());
			assertTrue("El id de la orden obtenida debe coincidir con el filtro",
					ordenCompraDto.getId().equals(ordenTest.getId()));
			assertTrue("La orden debe tener un item", ordenCompraDto.getItems().size() == 1);
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getOrdenSinItemsTest() {

		OrdenCompraEntity ordenTest = new OrdenCompraEntity();
		PersonaEntity proveedor = personaRepository.findOne(1);

		ordenTest.setPuesto(new PuestoEntity(1, 1, "", new EstadoEntity(1, "ACTIVO")));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(proveedor);
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoEntity(1000, "NUEVA"));
		ordenTest = ordenCompraRepository.save(ordenTest);

		try {
			OrdenCompraDto ordenCompraDto = ordenCompraService.getOrden(ordenTest.getId());
			assertTrue("El id de la orden obtenida debe coincidir con el filtro",
					ordenCompraDto.getId().equals(ordenTest.getId()));
			assertTrue("La orden no debe tener items", ordenCompraDto.getItems().size() == 0);
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getOrdenesEmitidasTest() {

		OrdenCompraEntity ordenTest = new OrdenCompraEntity();
		PersonaEntity proveedor = personaRepository.findOne(1);

		ordenTest.setPuesto(new PuestoEntity(1, 1, "", new EstadoEntity(1, "ACTIVO")));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(proveedor);
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(estadoRepository.findOne(EstadoEnum.EMITIDA.getValor()));
		ordenTest = ordenCompraRepository.save(ordenTest);

		ordenTest = new OrdenCompraEntity();
		proveedor = personaRepository.findOne(2);

		ordenTest.setPuesto(new PuestoEntity(1, 1, "", new EstadoEntity(1, "ACTIVO")));
		ordenTest.setNumero(2L);
		ordenTest.setProveedor(proveedor);
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(estadoRepository.findOne(EstadoEnum.EMITIDA.getValor()));
		ordenTest = ordenCompraRepository.save(ordenTest);

		try {
			Integer proveedorId = 1;
			List<OrdenCompraPreviewDto> ordenCompraDtos = ordenCompraService.getOrdenesEmitidas(proveedorId);

			assertTrue("Debe haber al menos una orden", ordenCompraDtos.size() > 0);

			ordenCompraDtos.forEach(orden -> {
				assertTrue("El id del proveedor debe ser 1", orden.getProveedor().getId().equals(1));
				assertTrue("El estado de la orden debe ser 'emitida'",
						orden.getEstado().getId().equals(EstadoEnum.EMITIDA.getValor()));
			});
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void insertConOrdenNullTest() {

		OrdenCompraDto ordenNull = null;

		try {
			ordenCompraService.insert(ordenNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La orden a insertar no se encuentra definida.",
					e.getMessage().equals("La orden a insertar no se encuentra definida."));
		}
	}

	@Test
	@Rollback
	public void insertOrdenYaExistenteTest() {

		OrdenCompraEntity ordenTest = new OrdenCompraEntity();
		ordenTest.setPuesto(puestoRepository.findOne(1));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(personaRepository.findOne(1));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(estadoRepository.findOne(1));
		ordenTest = ordenCompraRepository.save(ordenTest);

		try {
			ordenCompraService.insert(new OrdenCompraDto(ordenTest));
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de la orden a insertar ya existe en la base de datos.",
					e.getMessage().equals("El 'id' de la orden a insertar ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void insertOrdenInvalidaSinPuestoTest() {

		try {
			OrdenCompraDto ordenTest = new OrdenCompraDto();
			ordenCompraService.insert(ordenTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo puesto es obligatorio.", e.getMessage().equals("El campo puesto es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertOrdenInvalidaSinProveedorTest() {

		try {
			OrdenCompraDto ordenTest = new OrdenCompraDto();
			ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
			ordenCompraService.insert(ordenTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo proveedor es obligatorio.",
					e.getMessage().equals("El campo proveedor es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertOrdenInvalidaSinItemsTest() {

		try {
			OrdenCompraDto ordenTest = new OrdenCompraDto();
			ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
			ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
			ordenCompraService.insert(ordenTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La orden debe contener al menos un item.",
					e.getMessage().equals("La orden debe contener al menos un item."));
		}
	}

	@Test
	@Rollback
	public void insertOrdenInvalidaSinProductosTest() {

		try {
			OrdenCompraDto ordenTest = new OrdenCompraDto();
			ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
			ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));

			OrdenCompraItemDto item = new OrdenCompraItemDto();
			item.setId(999);
			List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
			items.add(item);
			ordenTest.setItems(items);

			ordenCompraService.insert(ordenTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo producto es obligatorio.", e.getMessage().equals("El campo producto es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertOrdenInvalidaSinCantidadTest() {

		try {
			OrdenCompraDto ordenTest = new OrdenCompraDto();
			ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
			ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));

			OrdenCompraItemDto item = new OrdenCompraItemDto();
			item.setId(999);
			item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
			List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
			items.add(item);
			ordenTest.setItems(items);

			ordenCompraService.insert(ordenTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo cantidad es obligatorio.", e.getMessage().equals("El campo cantidad es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertOrdenInvalidaConProductosDuplicadosTest() {

		try {
			OrdenCompraDto ordenTest = new OrdenCompraDto();
			ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
			ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));

			OrdenCompraItemDto item = new OrdenCompraItemDto();
			List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();			
			item.setId(998);
			item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
			item.setCantidad(1);			
			items.add(item);
			
			item = new OrdenCompraItemDto();			
			item.setId(999);
			item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
			item.setCantidad(1);			
			items.add(item);
			
			ordenTest.setItems(items);

			ordenCompraService.insert(ordenTest);

			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("No puede haber productos duplicados.",
					e.getMessage().equals("No puede haber productos duplicados."));
		}
	}

	@Test
	@Rollback
	public void insertOrdenNoPreexistenteConItemsTest() {

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.now());
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		try {
			OrdenCompraDto newOrden = ordenCompraService.insert(ordenTest);

			assertTrue("La orden se debe guardar con éxito", ordenCompraRepository.findOne(newOrden.getId()) != null);
			assertTrue("La fecha de creación debe ser igual a la del día",
					newOrden.getFechaCreacion().equals(LocalDate.now()));
			assertTrue("El estado debe ser NUEVA", newOrden.getEstado().getId().equals(EstadoEnum.NUEVA.getValor()));

			assertTrue("Debe haber items", newOrden.getItems().size() > 0);

			newOrden.getItems().forEach(it -> {
				assertTrue("El estado de los items debe ser NUEVO ITEM",
						it.getEstado().getId().equals(EstadoEnum.ITEM_NUEVO.getValor()));

			});

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void updateOrdenNullTest() {

		OrdenCompraDto ordenNull = null;

		try {
			ordenCompraService.update(ordenNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La orden a modificar no se encuentra definida.",
					e.getMessage().equals("La orden a modificar no se encuentra definida."));
		}
	}

	@Test
	@Rollback
	public void updateOrdenNoPreexistenteTest() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraEntity qordencompra = QOrdenCompraEntity.ordenCompraEntity;

		Integer lastId = queryFactory.select(qordencompra.id).from(qordencompra).orderBy(qordencompra.id.desc())
				.fetchFirst();
		if (lastId == null) {
			lastId = 0;
		}
		lastId++;

		OrdenCompraDto ordenPreexistente = new OrdenCompraDto();
		ordenPreexistente.setId(lastId);

		try {
			ordenCompraService.update(ordenPreexistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de la orden que desea modificar no existe en la base de datos.",
					e.getMessage().equals("El 'id' de la orden que desea modificar no existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void updateOrdenPreexistenteConItemsTest() {

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(1)));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		try {
			OrdenCompraDto newOrden = ordenCompraService.insert(ordenTest);

			newOrden.setObservaciones("OC ACTUALIZADA");

			OrdenCompraDto editOrden = ordenCompraService.update(newOrden);

			assertTrue("La orden se debe actualizar con éxito", editOrden.getObservaciones().equals("OC ACTUALIZADA"));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void emitirOrdenPreexistenteConItemsTest() {

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockInicial = new ProductoStockEntity();
		ProductoStockEntity stockEmitido = new ProductoStockEntity();
		ProductoStockEntity queryResult = new ProductoStockEntity();

		try {
			OrdenCompraDto newOrden = ordenCompraService.insert(ordenTest);

			queryResult = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(item.getProducto().getId()))
					.fetchOne();

			if (queryResult != null) {
				stockInicial.setStockEntrante(queryResult.getStockEntrante());
				stockInicial.setStockDisponible(queryResult.getStockDisponible());
				stockInicial.setStockTransito(queryResult.getStockTransito());
				stockInicial.setStockReservado(queryResult.getStockReservado());
				stockInicial.setStockVendido(queryResult.getStockVendido());
			}

			assertTrue("El estado de la orden debe ser NUEVA",
					newOrden.getEstado().getId().equals(EstadoEnum.NUEVA.getValor()));

			OrdenCompraDto ordenEmitida = ordenCompraService.emitirOrdenCompra(newOrden);

			assertTrue("La orden debe quedar EMITIDA",
					ordenEmitida.getEstado().getId().equals(EstadoEnum.EMITIDA.getValor()));

			assertTrue("Debe haber items", ordenEmitida.getItems().size() > 0);

			queryResult = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(item.getProducto().getId()))
					.fetchOne();

			if (queryResult != null) {
				stockEmitido.setStockEntrante(queryResult.getStockEntrante());
				stockEmitido.setStockDisponible(queryResult.getStockDisponible());
				stockEmitido.setStockTransito(queryResult.getStockTransito());
				stockEmitido.setStockReservado(queryResult.getStockReservado());
				stockEmitido.setStockVendido(queryResult.getStockVendido());
			}

			ordenEmitida.getItems().forEach(it -> {
				assertTrue("El estado de los items debe ser ITEM PEDIDO",
						it.getEstado().getId().equals(EstadoEnum.ITEM_PEDIDO.getValor()));

				assertTrue("Se debe actualizar el stock entrante",
						(stockEmitido.getStockEntrante()
								- (stockInicial != null && stockInicial.getStockEntrante() != null
										? stockInicial.getStockEntrante()
										: 0)) == it.getCantidad());

				assertTrue("No se debe modificar el stock disponible",
						(stockEmitido.getStockDisponible()
								- (stockInicial != null && stockInicial.getStockDisponible() != null
										? stockInicial.getStockDisponible()
										: 0)) == 0);

				assertTrue("No se debe modificar el stock transito",
						(stockEmitido.getStockTransito()
								- (stockInicial != null && stockInicial.getStockTransito() != null
										? stockInicial.getStockTransito()
										: 0)) == 0);

				assertTrue("No se debe modificar el stock reservado",
						(stockEmitido.getStockReservado()
								- (stockInicial != null && stockInicial.getStockReservado() != null
										? stockInicial.getStockReservado()
										: 0)) == 0);

				assertTrue("No se debe modificar el stock vendido",
						(stockEmitido.getStockVendido()
								- (stockInicial != null && stockInicial.getStockVendido() != null
										? stockInicial.getStockVendido()
										: 0)) == 0);
			});

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void emitirOrdenYaEmitidaTest() {

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		try {
			OrdenCompraDto newOrden = ordenCompraService.insert(ordenTest);

			OrdenCompraDto ordenEmitida = ordenCompraService.emitirOrdenCompra(newOrden);

			ordenCompraService.emitirOrdenCompra(ordenEmitida);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("Para emitir una orden su estado previo debe ser NUEVA.",
					e.getMessage().equals("Para emitir una orden su estado previo debe ser NUEVA."));
		}
	}

	@Test
	@Rollback
	public void cancelarEmisionOrdenPreexistenteConItemsTest() {

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockInicial = new ProductoStockEntity();
		ProductoStockEntity stockEmitido = new ProductoStockEntity();
		ProductoStockEntity stockCancelado = new ProductoStockEntity();
		ProductoStockEntity queryResult = new ProductoStockEntity();

		try {
			OrdenCompraDto newOrden = ordenCompraService.insert(ordenTest);

			queryResult = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(item.getProducto().getId()))
					.fetchOne();

			if (queryResult != null) {
				stockInicial.setStockEntrante(queryResult.getStockEntrante());
				stockInicial.setStockDisponible(queryResult.getStockDisponible());
				stockInicial.setStockTransito(queryResult.getStockTransito());
				stockInicial.setStockReservado(queryResult.getStockReservado());
				stockInicial.setStockVendido(queryResult.getStockVendido());
			}

			assertTrue("El estado de la orden debe ser NUEVA",
					newOrden.getEstado().getId().equals(EstadoEnum.NUEVA.getValor()));

			OrdenCompraDto ordenEmitida = ordenCompraService.emitirOrdenCompra(newOrden);

			assertTrue("La orden debe quedar EMITIDA",
					ordenEmitida.getEstado().getId().equals(EstadoEnum.EMITIDA.getValor()));

			assertTrue("Debe haber items", ordenEmitida.getItems().size() > 0);

			queryResult = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(item.getProducto().getId()))
					.fetchOne();

			if (queryResult != null) {
				stockEmitido.setStockEntrante(queryResult.getStockEntrante());
				stockEmitido.setStockDisponible(queryResult.getStockDisponible());
				stockEmitido.setStockTransito(queryResult.getStockTransito());
				stockEmitido.setStockReservado(queryResult.getStockReservado());
				stockEmitido.setStockVendido(queryResult.getStockVendido());
			}

			ordenEmitida.getItems().forEach(it -> {
				assertTrue("El estado de los items debe ser ITEM PEDIDO",
						it.getEstado().getId().equals(EstadoEnum.ITEM_PEDIDO.getValor()));

				assertTrue("Se debe actualizar el stock entrante",
						(stockEmitido.getStockEntrante()
								- (stockInicial != null && stockInicial.getStockEntrante() != null
										? stockInicial.getStockEntrante()
										: 0)) == it.getCantidad());

				assertTrue("No se debe modificar el stock disponible",
						(stockEmitido.getStockDisponible()
								- (stockInicial != null && stockInicial.getStockDisponible() != null
										? stockInicial.getStockDisponible()
										: 0)) == 0);

				assertTrue("No se debe modificar el stock transito",
						(stockEmitido.getStockTransito()
								- (stockInicial != null && stockInicial.getStockTransito() != null
										? stockInicial.getStockTransito()
										: 0)) == 0);

				assertTrue("No se debe modificar el stock reservado",
						(stockEmitido.getStockReservado()
								- (stockInicial != null && stockInicial.getStockReservado() != null
										? stockInicial.getStockReservado()
										: 0)) == 0);

				assertTrue("No se debe modificar el stock vendido",
						(stockEmitido.getStockVendido()
								- (stockInicial != null && stockInicial.getStockVendido() != null
										? stockInicial.getStockVendido()
										: 0)) == 0);
			});

			OrdenCompraDto ordenCancelada = ordenCompraService.cancelarEmisionOrdenCompra(ordenEmitida);

			assertTrue("La orden debe quedar NUEVA", ordenCancelada.getEstado().getId().equals(EstadoEnum.NUEVA.getValor()));

			assertTrue("Debe haber items", ordenCancelada.getItems().size() > 0);

			queryResult = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(item.getProducto().getId()))
					.fetchOne();

			if (queryResult != null) {
				stockCancelado.setStockEntrante(queryResult.getStockEntrante());
				stockCancelado.setStockDisponible(queryResult.getStockDisponible());
				stockCancelado.setStockTransito(queryResult.getStockTransito());
				stockCancelado.setStockReservado(queryResult.getStockReservado());
				stockCancelado.setStockVendido(queryResult.getStockVendido());
			}

			ordenCancelada.getItems().forEach(it -> {
				assertTrue("El estado de los items debe ser ITEM NUEVO",
						it.getEstado().getId().equals(EstadoEnum.ITEM_NUEVO.getValor()));

				assertTrue("Se debe revertir el stock entrante",
						(stockInicial != null && stockInicial.getStockEntrante() != null
								? stockInicial.getStockEntrante()
								: 0) - stockCancelado.getStockEntrante() == 0);

				assertTrue("No se debe modificar el stock disponible",
						(stockInicial != null && stockInicial.getStockDisponible() != null
								? stockInicial.getStockDisponible()
								: 0) - stockCancelado.getStockDisponible() == 0);

				assertTrue("No se debe modificar el stock transito",
						(stockInicial != null && stockInicial.getStockTransito() != null
								? stockInicial.getStockTransito()
								: 0) - stockCancelado.getStockTransito() == 0);

				assertTrue("No se debe modificar el stock reservado",
						(stockInicial != null && stockInicial.getStockReservado() != null
								? stockInicial.getStockReservado()
								: 0) - stockCancelado.getStockReservado() == 0);

				assertTrue("No se debe modificar el stock vendido",
						(stockInicial != null && stockInicial.getStockVendido() != null ? stockInicial.getStockVendido()
								: 0) - stockCancelado.getStockVendido() == 0);
			});

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void cancelarEmisionOrdenNoEmitidaTest() {

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		try {
			OrdenCompraDto newOrden = ordenCompraService.insert(ordenTest);

			ordenCompraService.cancelarEmisionOrdenCompra(newOrden);

			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("Para cancelar la emisión de una orden su estado previo debe ser EMITIDA.",
					e.getMessage().equals("Para cancelar la emisión de una orden su estado previo debe ser EMITIDA."));
		}
	}

	@Test
	@Rollback
	public void cancelarEmisionOrdenConItemsRecibidosTest() {

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		try {
			OrdenCompraDto newOrden = ordenCompraService.insert(ordenTest);
			ordenCompraService.emitirOrdenCompra(newOrden);

			JPAQueryFactory queryFactory = new JPAQueryFactory(em);
			QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

			List<OrdenCompraItemEntity> its = queryFactory.selectFrom(qitem).where(qitem.orden.id.eq(newOrden.getId()))
					.fetch();

			assertTrue("Debe haber items", its.size() > 0);

			its.forEach(it -> {
				it.setPendientes(0);
			});
			ordenCompraItemRepository.save(its);

			ordenCompraService.cancelarEmisionOrdenCompra(newOrden);

			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("No se puede cancelar la emisión de una orden de compras que posee productos RECIBIDOS.",
					e.getMessage().equals(
							"No se puede cancelar la emisión de una orden de compras que posee productos RECIBIDOS."));
		}
	}

	@Test
	@Rollback
	public void recibirOrdenCompraConItemsNoRecibidosTest() {

		Integer productoId = 1;
		Integer cantidadOriginal = 2;
		Integer cantidadRecibida = 1;

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(productoId)));
		item.setCantidad(cantidadOriginal);
		item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockEmitido = new ProductoStockEntity();
		ProductoStockEntity stockRecibido = new ProductoStockEntity();
		ProductoStockEntity queryResult = new ProductoStockEntity();

		try {
			OrdenCompraDto newOrden = ordenCompraService.insert(ordenTest);

			newOrden = ordenCompraService.emitirOrdenCompra(newOrden);

			queryResult = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(item.getProducto().getId()))
					.fetchOne();

			if (queryResult != null) {
				stockEmitido.setStockEntrante(queryResult.getStockEntrante());
				stockEmitido.setStockDisponible(queryResult.getStockDisponible());
				stockEmitido.setStockTransito(queryResult.getStockTransito());
				stockEmitido.setStockReservado(queryResult.getStockReservado());
				stockEmitido.setStockVendido(queryResult.getStockVendido());
			}

			ordenCompraService.recibirOrdenCompraItem(newOrden.getId(), productoId, cantidadRecibida);

			assertTrue("Debe haber items", newOrden.getItems().size() > 0);

			queryResult = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(item.getProducto().getId()))
					.fetchOne();

			if (queryResult != null) {
				stockRecibido.setStockEntrante(queryResult.getStockEntrante());
				stockRecibido.setStockDisponible(queryResult.getStockDisponible());
				stockRecibido.setStockTransito(queryResult.getStockTransito());
				stockRecibido.setStockReservado(queryResult.getStockReservado());
				stockRecibido.setStockVendido(queryResult.getStockVendido());
			}

			newOrden.getItems().forEach(it -> {

				OrdenCompraItemEntity itemEntity = ordenCompraItemRepository.findOne(it.getId());

				assertTrue("Al stock entrante se deben descontar los items recibidos",
						((stockEmitido != null && stockEmitido.getStockEntrante() != null
								? stockEmitido.getStockEntrante()
								: 0) - stockRecibido.getStockEntrante()) == cantidadRecibida);
				assertTrue("Al stock disponible se deben sumar lo items recibidos",
						((stockEmitido != null && stockEmitido.getStockDisponible() != null
								? stockEmitido.getStockDisponible()
								: 0) + cantidadRecibida) == stockRecibido.getStockDisponible());

				assertTrue("No deben quedar items pendientes", itemEntity.getPendientes().equals(1));
				assertTrue("El item debe figurar en estado Emitido",
						itemEntity.getEstado().getId().equals(EstadoEnum.ITEM_PEDIDO.getValor()));
			});

			ordenCompraService.recibirOrdenCompra(newOrden.getId());

			OrdenCompraEntity ordenRecibida = ordenCompraRepository.findOne(newOrden.getId());

			assertTrue("La orden debe quedar es estado EMITIDA",
					ordenRecibida.getEstado().getId().equals(EstadoEnum.EMITIDA.getValor()));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void recibirOrdenCompraConItemsRecibidosTest() {

		Integer productoId = 1;
		Integer cantidad = 1;

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(productoId)));
		item.setCantidad(cantidad);
		item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		ProductoStockEntity stockEmitido = new ProductoStockEntity();
		ProductoStockEntity stockRecibido = new ProductoStockEntity();
		ProductoStockEntity queryResult = new ProductoStockEntity();

		try {
			OrdenCompraDto newOrden = ordenCompraService.insert(ordenTest);

			newOrden = ordenCompraService.emitirOrdenCompra(newOrden);

			queryResult = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(item.getProducto().getId()))
					.fetchOne();

			if (queryResult != null) {
				stockEmitido.setStockEntrante(queryResult.getStockEntrante());
				stockEmitido.setStockDisponible(queryResult.getStockDisponible());
				stockEmitido.setStockTransito(queryResult.getStockTransito());
				stockEmitido.setStockReservado(queryResult.getStockReservado());
				stockEmitido.setStockVendido(queryResult.getStockVendido());
			}

			ordenCompraService.recibirOrdenCompraItem(newOrden.getId(), productoId, cantidad);

			assertTrue("Debe haber items", newOrden.getItems().size() > 0);

			queryResult = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(item.getProducto().getId()))
					.fetchOne();

			if (queryResult != null) {
				stockRecibido.setStockEntrante(queryResult.getStockEntrante());
				stockRecibido.setStockDisponible(queryResult.getStockDisponible());
				stockRecibido.setStockTransito(queryResult.getStockTransito());
				stockRecibido.setStockReservado(queryResult.getStockReservado());
				stockRecibido.setStockVendido(queryResult.getStockVendido());
			}

			newOrden.getItems().forEach(it -> {

				OrdenCompraItemEntity itemEntity = ordenCompraItemRepository.findOne(it.getId());

				assertTrue("Al stock entrante se deben descontar los items recibidos",
						((stockEmitido != null && stockEmitido.getStockEntrante() != null
								? stockEmitido.getStockEntrante()
								: 0) - stockRecibido.getStockEntrante()) == cantidad);
				assertTrue("Al stock disponible se deben sumar lo items recibidos",
						((stockEmitido != null && stockEmitido.getStockDisponible() != null
								? stockEmitido.getStockDisponible()
								: 0) + cantidad) == stockRecibido.getStockDisponible());

				assertTrue("No deben quedar items pendientes", itemEntity.getPendientes() == 0);
				assertTrue("El item debe figurar en estado Recibido",
						itemEntity.getEstado().getId().equals(EstadoEnum.ITEM_RECIBIDO.getValor()));
			});

			ordenCompraService.recibirOrdenCompra(newOrden.getId());

			OrdenCompraEntity ordenRecibida = ordenCompraRepository.findOne(newOrden.getId());

			assertTrue("La orden debe quedar es estado RECIBIDA",
					ordenRecibida.getEstado().getId().equals(EstadoEnum.RECIBIDA.getValor()));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void deleteOrdenCompraConIdNullTest() {

		Integer idNull = null;

		try {
			ordenCompraService.delete(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de la orden de compra no puede ser nulo.",
					e.getMessage().equals("El 'id' de la orden de compra no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void deleteOrdenCompraNoExistenteTest() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraEntity qordenCompra = QOrdenCompraEntity.ordenCompraEntity;

		Integer lastId = queryFactory.select(qordenCompra.id).from(qordenCompra).orderBy(qordenCompra.id.desc())
				.fetchFirst();
		if (lastId == null) {
			lastId = 0;
		}
		lastId++;

		try {
			ordenCompraService.delete(lastId);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La orden de compra que se desea eliminar no existe.",
					e.getMessage().equals("La orden de compra que se desea eliminar no existe."));
		}
	}

	@Test
	@Rollback
	public void deleteOrdenCompraExistenteTest() {

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		Integer newId = ordenCompraService.insert(ordenTest).getId();

		try {
			ordenCompraService.delete(newId);

			assertTrue("La orden de compra se debe eliminar con éxito", ordenCompraRepository.findOne(newId) == null);

			JPAQueryFactory queryFactory = new JPAQueryFactory(em);
			QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

			List<OrdenCompraItemDto> ordenCompraItemDtos = queryFactory.selectFrom(qitem)
					.where(qitem.orden.id.eq(newId)).fetch().stream().map(it -> new OrdenCompraItemDto(it))
					.collect(Collectors.toList());

			assertTrue("Los items de la orden de compra se deben eliminar con éxito", ordenCompraItemDtos.size() == 0);

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void deleteOrdenCompraEmitidaTest() {

		OrdenCompraDto ordenTest = new OrdenCompraDto();
		ordenTest.setPuesto(new PuestoDto(puestoRepository.findOne(1)));
		ordenTest.setNumero(1L);
		ordenTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		ordenTest.setFechaCreacion(LocalDate.of(2018, 06, 30));
		ordenTest.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		OrdenCompraItemDto item = new OrdenCompraItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		List<OrdenCompraItemDto> items = new ArrayList<OrdenCompraItemDto>();
		items.add(item);
		ordenTest.setItems(items);

		OrdenCompraDto ordenSaved = ordenCompraService.insert(ordenTest);

		ordenCompraService.emitirOrdenCompra(ordenSaved);

		try {
			ordenCompraService.delete(ordenSaved.getId());
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("Solo pueden eliminarse órdenes de compra en estado NUEVA.",
					e.getMessage().equals("Solo pueden eliminarse órdenes de compra en estado NUEVA."));
		}
	}

}
