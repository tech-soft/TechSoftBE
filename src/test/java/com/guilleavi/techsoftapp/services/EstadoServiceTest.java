package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.EstadoDto;

@Transactional(timeout = 300)
public class EstadoServiceTest extends AbstractTest {

	@Autowired
	EstadoService estadoService = null;

	@Test
	@Rollback
	public void getEstadosTest() {

		List<EstadoDto> estados = estadoService.getEstados();

		assertTrue("Se deben obtener los estados con ids entre 0 y 999", estados.size() == estados.stream()
				.filter(estado -> estado.getId() >= 0 && estado.getId() <= 999).count());
	}

	@Test
	@Rollback
	public void getOrdenesEstadosTest() {

		List<EstadoDto> estados = estadoService.getOrdenesEstados();

		assertTrue("Se deben obtener los estados con ids entre 1000 y 1099", estados.size() == estados.stream()
				.filter(estado -> estado.getId() >= 1000 && estado.getId() <= 1099).count());
	}
}
