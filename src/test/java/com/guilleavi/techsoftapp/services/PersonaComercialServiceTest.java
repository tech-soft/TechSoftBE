package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.IvaTipoDto;
import com.guilleavi.techsoftapp.dto.PagoFormaDto;
import com.guilleavi.techsoftapp.dto.PersonaComercialDto;
import com.guilleavi.techsoftapp.dto.PrecioListaDto;
import com.guilleavi.techsoftapp.entities.EstadoEntity;
import com.guilleavi.techsoftapp.entities.IvaTipoEntity;
import com.guilleavi.techsoftapp.entities.PagoFormaEntity;
import com.guilleavi.techsoftapp.entities.PersonaComercialEntity;
import com.guilleavi.techsoftapp.entities.PersonaEntity;
import com.guilleavi.techsoftapp.entities.PrecioListaEntity;
import com.guilleavi.techsoftapp.repositories.PersonaComercialRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;

public class PersonaComercialServiceTest extends AbstractTest {

	@Autowired
	PersonaComercialService personaComercialService = null;

	@Autowired
	PersonaRepository personaRepository = null;
	@Autowired
	PersonaComercialRepository personaComercialRepository = null;

	@Test
	@Rollback
	public void getDatosComercialesByPersonaId_NullId_Test() {

		Integer perconaIdNull = null;

		try {
			personaComercialService.getDatosComercialesByPersona(perconaIdNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de persona no puede ser nulo.",
					e.getMessage().equals("El 'id' de persona no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getDatosComercialesByPersonaId_PersonaIdInexistente_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		PersonaComercialDto result = personaComercialService.getDatosComercialesByPersona(personaTemp.getId() + 1);

		assertTrue("No debe obtenerse ningún resultado", result == null);

	}

	@Test
	@Rollback
	public void getDatosComercialesByPersonaId_PersonaSinDatosComerciales_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		PersonaComercialDto result = personaComercialService.getDatosComercialesByPersona(personaTemp.getId());

		assertTrue("No debe obtenerse ningún resultado", result == null);

	}

	@Test
	@Rollback
	public void getDatosComercialesByPersonaId_DatosComercialesExistente_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		PersonaComercialEntity personaComercialTemp = new PersonaComercialEntity();
		personaComercialTemp.setPersona(personaTemp);
		personaComercialTemp.setIvaTipo(new IvaTipoEntity(1, "IVA TIPO"));
		personaComercialTemp.setPagoForma(new PagoFormaEntity(1, "FORMA PAGO"));
		personaComercialTemp.setPrecioLista(new PrecioListaEntity(1, "LISTA 1"));
		personaComercialTemp.setDescuento(10.00);
		personaComercialTemp = personaComercialRepository.save(personaComercialTemp);

		try {
			PersonaComercialDto personaComercialDto = personaComercialService
					.getDatosComercialesByPersona(personaTemp.getId());

			assertTrue("El id de persona debe coincidir",
					personaComercialDto.getPersonaId().equals(personaTemp.getId()));
			assertTrue("El tipo de iva debe coincidir",
					personaComercialDto.getIvaTipo().equals(new IvaTipoDto(1, "IVA TIPO")));
			assertTrue("La forma de pago debe coincidir",
					personaComercialDto.getPagoForma().equals(new PagoFormaDto(1, "FORMA PAGO")));
			assertTrue("La lista de precio debe coincidir",
					personaComercialDto.getPrecioLista().equals(new PrecioListaDto(1, "LISTA 1")));
			assertTrue("El descuento debe coincidir", personaComercialDto.getDescuento().equals(10.00));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void insert_PersonaComercialNull_Test() {

		PersonaComercialDto personaComercialNull = null;

		try {
			personaComercialService.upsert(personaComercialNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("Los datos comerciales a insertar no se encuentra definido.",
					e.getMessage().equals("Los datos comerciales a insertar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void insert_PersonaComercialDefinida_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		PersonaComercialDto personaComercialTemp = new PersonaComercialDto();
		personaComercialTemp.setPersonaId(personaTemp.getId());
		personaComercialTemp.setIvaTipo(new IvaTipoDto(1, "IVA TIPO"));
		personaComercialTemp.setPagoForma(new PagoFormaDto(1, "FORMA PAGO"));
		personaComercialTemp.setPrecioLista(new PrecioListaDto(1, "LISTA 1"));
		personaComercialTemp.setDescuento(10.00);

		try {
			PersonaComercialDto personaComercialDto = personaComercialService.upsert(personaComercialTemp);

			assertTrue("El id de persona debe coincidir",
					personaComercialDto.getPersonaId().equals(personaTemp.getId()));
			assertTrue("El tipo de iva debe coincidir",
					personaComercialDto.getIvaTipo().equals(new IvaTipoDto(1, "IVA TIPO")));
			assertTrue("La forma de pago debe coincidir",
					personaComercialDto.getPagoForma().equals(new PagoFormaDto(1, "FORMA PAGO")));
			assertTrue("La lista de precio debe coincidir",
					personaComercialDto.getPrecioLista().equals(new PrecioListaDto(1, "LISTA 1")));
			assertTrue("El descuento debe coincidir", personaComercialDto.getDescuento().equals(10.00));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void update_PersonaComercialNull_Test() {

		PersonaComercialDto perconaComercialNull = null;

		try {
			personaComercialService.upsert(perconaComercialNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("Los datos comerciales a modificar no se encuentra definido.",
					e.getMessage().equals("Los datos comerciales a modificar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void update_PersonaComercialDefinida_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		PersonaComercialDto personaComercialTemp = new PersonaComercialDto();
		personaComercialTemp.setPersonaId(personaTemp.getId());
		personaComercialTemp.setIvaTipo(new IvaTipoDto(1, "IVA TIPO"));
		personaComercialTemp.setPagoForma(new PagoFormaDto(1, "FORMA PAGO"));
		personaComercialTemp.setPrecioLista(new PrecioListaDto(1, "LISTA 1"));
		personaComercialTemp.setDescuento(10.00);

		try {
			PersonaComercialDto personaComercialDto = personaComercialService.upsert(personaComercialTemp);

			personaComercialDto.setPersonaId(personaTemp.getId());
			personaComercialDto.setIvaTipo(new IvaTipoDto(2, "IVA TIPO 2"));
			personaComercialDto.setPagoForma(new PagoFormaDto(2, "FORMA PAGO 2"));
			personaComercialDto.setPrecioLista(new PrecioListaDto(2, "LISTA 2"));
			personaComercialDto.setDescuento(20.00);

			personaComercialDto = personaComercialService.upsert(personaComercialDto);

			assertTrue("El id de persona debe coincidir",
					personaComercialDto.getPersonaId().equals(personaTemp.getId()));
			assertTrue("El tipo de iva debe coincidir",
					personaComercialDto.getIvaTipo().equals(new IvaTipoDto(2, "IVA TIPO 2")));
			assertTrue("La forma de pago debe coincidir",
					personaComercialDto.getPagoForma().equals(new PagoFormaDto(2, "FORMA PAGO 2")));
			assertTrue("La lista de precio debe coincidir",
					personaComercialDto.getPrecioLista().equals(new PrecioListaDto(2, "LISTA 2")));
			assertTrue("El descuento debe coincidir", personaComercialDto.getDescuento().equals(20.00));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void deleteByPersonaId_PersonaIdNull_Test() {

		Integer idNull = null;

		try {
			personaComercialService.deleteByPersonaId(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de persona no puede ser nulo.",
					e.getMessage().equals("El 'id' de persona no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void deleteByPersonaId_PersonaIdInexistente_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		try {
			personaComercialService.deleteByPersonaId(personaTemp.getId() + 1);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La persona no posee datos comerciales relacionados.",
					e.getMessage().equals("La persona no posee datos comerciales relacionados."));
		}

	}

	@Test
	@Rollback
	public void deleteByPersonaId_PersonaIdExistenteSinDatosComerciales_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		try {
			personaComercialService.deleteByPersonaId(personaTemp.getId());
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La persona no existe o no posee datos comerciales relacionados.",
					e.getMessage().equals("La persona no existe o no posee datos comerciales relacionados."));
		}

	}

	@Test
	@Rollback
	public void deleteByPersonaId_PersonaIdExistenteConDatosComerciales_Test() {

		PersonaEntity personaTemp = new PersonaEntity();
		personaTemp.setNombre("Test x1b2c3");
		personaTemp.setEstado(new EstadoEntity(1, "Activo"));
		personaTemp = personaRepository.save(personaTemp);

		PersonaComercialEntity personaComercialTemp = new PersonaComercialEntity();
		personaComercialTemp.setPersona(personaTemp);
		personaComercialTemp.setDescuento(10.00);
		personaComercialTemp = personaComercialRepository.save(personaComercialTemp);

		try {
			personaComercialService.deleteByPersonaId(personaTemp.getId());

			PersonaComercialDto result = personaComercialService.getDatosComercialesByPersona(personaTemp.getId());

			assertTrue("No debe obtenerse ningún resultado", result == null);

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

}
