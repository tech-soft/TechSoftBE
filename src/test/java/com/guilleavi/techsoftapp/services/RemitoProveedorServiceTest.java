package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.ComprobanteTipoDto;
import com.guilleavi.techsoftapp.dto.OrdenCompraPreviewDto;
import com.guilleavi.techsoftapp.dto.ProductoPreviewDto;
import com.guilleavi.techsoftapp.dto.ProveedorDto;
import com.guilleavi.techsoftapp.dto.RemitoProveedorDto;
import com.guilleavi.techsoftapp.dto.RemitoProveedorItemDto;
import com.guilleavi.techsoftapp.dto.RemitoProveedorPreviewDto;
import com.guilleavi.techsoftapp.entities.PersonaEntity;
import com.guilleavi.techsoftapp.entities.ProductoSerieEntity;
import com.guilleavi.techsoftapp.entities.ProductoStockEntity;
import com.guilleavi.techsoftapp.entities.QProductoStockEntity;
import com.guilleavi.techsoftapp.entities.QRemitoProveedorEntity;
import com.guilleavi.techsoftapp.entities.RemitoProveedorEntity;
import com.guilleavi.techsoftapp.entities.RemitoProveedorItemEntity;
import com.guilleavi.techsoftapp.repositories.ComprobanteTipoRepository;
import com.guilleavi.techsoftapp.repositories.EstadoRepository;
import com.guilleavi.techsoftapp.repositories.OrdenCompraRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;
import com.guilleavi.techsoftapp.repositories.ProductoRepository;
import com.guilleavi.techsoftapp.repositories.ProductoSerieRepository;
import com.guilleavi.techsoftapp.repositories.PuestoRepository;
import com.guilleavi.techsoftapp.repositories.RemitoProveedorItemRepository;
import com.guilleavi.techsoftapp.repositories.RemitoProveedorRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Transactional(timeout = 300)
public class RemitoProveedorServiceTest extends AbstractTest {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	ComprobanteTipoRepository comprobanteTipoRepository;
	@Autowired
	EstadoRepository estadoRepository;
	@Autowired
	OrdenCompraRepository ordenCompraRepository;
	@Autowired
	PersonaRepository personaRepository;
	@Autowired
	ProductoRepository productoRepository;
	@Autowired
	ProductoSerieRepository productoSerieRepository;
	@Autowired
	PuestoRepository puestoRepository;
	@Autowired
	RemitoProveedorRepository remitoProveedorRepository;
	@Autowired
	RemitoProveedorItemRepository remitoProveedorItemRepository;

	@Autowired
	RemitoProveedorService remitoProveedorService;
	@Autowired
	OrdenCompraService ordenCompraService;

	@Test
	@Rollback
	public void getRemitosPreviewTest() {

		RemitoProveedorEntity remitoTest = new RemitoProveedorEntity();
		PersonaEntity proveedor = personaRepository.findOne(1);

		remitoTest.setPuesto(1);
		remitoTest.setNumero(1L);
		remitoTest.setProveedor(proveedor);
		remitoTest.setFecha(LocalDate.of(2018, 06, 30));
		remitoTest.setComprobanteTipo(comprobanteTipoRepository.findOne(1));
		remitoProveedorRepository.save(remitoTest);

		remitoTest = new RemitoProveedorEntity();
		remitoTest.setPuesto(2);
		remitoTest.setNumero(2L);
		remitoTest.setProveedor(proveedor);
		remitoTest.setFecha(LocalDate.of(2018, 07, 01));
		remitoTest.setComprobanteTipo(comprobanteTipoRepository.findOne(1));
		remitoProveedorRepository.save(remitoTest);

		// Buscar por proveedor
		List<RemitoProveedorPreviewDto> remitos = remitoProveedorService.getRemitosPreview(1, null, null);
		assertTrue("Se deben filtrar por proveedor",
				remitos.stream().filter(remito -> remito.getProveedor().getId().equals(1)).count() == remitos.size());

		// Buscar por fecha
		remitos = remitoProveedorService.getRemitosPreview(null, LocalDate.of(2018, 06, 30), null);
		assertTrue("Se deben filtrar por fecha de creacion", remitos.stream()
				.filter(remito -> remito.getFecha().equals(LocalDate.of(2018, 06, 30))).count() == remitos.size());

		// Buscar por numero
		remitos = remitoProveedorService.getRemitosPreview(null, null, 2L);
		assertTrue("Se deben filtrar por numero y puesto",
				remitos.stream().filter(remito -> remito.getNumero().equals(2L)).count() == remitos.size());

		// Buscar por proveedor, fecha y numero
		remitos = remitoProveedorService.getRemitosPreview(1, LocalDate.of(2018, 06, 30), 1L);
		assertTrue("Se deben filtrar por estado, numero y puesto", remitos.stream()
				.filter(remito -> remito.getProveedor().getId().equals(1)
						&& remito.getFecha().equals(LocalDate.of(2018, 06, 30)) && remito.getNumero().equals(1L))
				.count() == remitos.size());

		// Sin filtros
		remitos = remitoProveedorService.getRemitosPreview(null, null, null);
		assertTrue("Sin filtros no se deben devolver remitos", remitos == null);
	}

	@Test
	@Rollback
	public void getRemitoConIdNullTest() {

		Integer idNull = null;

		try {
			remitoProveedorService.getRemito(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de remito no puede ser nulo.",
					e.getMessage().equals("El 'id' de remito no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getRemitoConItemsTest() {

		RemitoProveedorEntity remitoTest = new RemitoProveedorEntity();
		PersonaEntity proveedor = personaRepository.findOne(1);

		remitoTest.setPuesto(1);
		remitoTest.setNumero(1L);
		remitoTest.setProveedor(proveedor);
		remitoTest.setFecha(LocalDate.of(2018, 06, 30));
		remitoTest.setComprobanteTipo(comprobanteTipoRepository.findOne(1));
		remitoTest = remitoProveedorRepository.save(remitoTest);

		RemitoProveedorItemEntity itemTest = new RemitoProveedorItemEntity();
		itemTest.setRemito(remitoTest);
		itemTest.setProducto(productoRepository.findOne(1));
		itemTest.setCantidad(1);
		itemTest = remitoProveedorItemRepository.save(itemTest);

		ProductoSerieEntity series = new ProductoSerieEntity();
		series.setNumeroSerie("aaabbbcccddd");
		series.setRemitoProveedorItem(itemTest);
		series = productoSerieRepository.save(series);

		try {
			RemitoProveedorDto remitoProveedorDto = remitoProveedorService.getRemito(remitoTest.getId());

			assertTrue("El id del remito obtenido debe coincidir con el filtro",
					remitoProveedorDto.getId().equals(remitoTest.getId()));
			assertTrue("El remito debe tener un item", remitoProveedorDto.getItems().size() == 1);
			assertTrue("El item debe contener su numero de serie",
					remitoProveedorDto.getItems().get(0).getNumerosSerie().get(0).equals("aaabbbcccddd"));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getRemitoSinItemsTest() {

		RemitoProveedorEntity remitoTest = new RemitoProveedorEntity();
		PersonaEntity proveedor = personaRepository.findOne(1);

		remitoTest.setPuesto(1);
		remitoTest.setNumero(1L);
		remitoTest.setProveedor(proveedor);
		remitoTest.setFecha(LocalDate.of(2018, 06, 30));
		remitoTest.setComprobanteTipo(comprobanteTipoRepository.findOne(1));
		remitoTest = remitoProveedorRepository.save(remitoTest);

		try {
			RemitoProveedorDto remitoProveedorDto = remitoProveedorService.getRemito(remitoTest.getId());
			assertTrue("El id del remito obtenido debe coincidir con el filtro",
					remitoProveedorDto.getId().equals(remitoTest.getId()));
			assertTrue("El remito no debe tener items", remitoProveedorDto.getItems().size() == 0);
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void insertConRemitoNullTest() {

		RemitoProveedorDto remitoNull = null;

		try {
			remitoProveedorService.insert(remitoNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El remito a insertar no se encuentra definido.",
					e.getMessage().equals("El remito a insertar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoYaExistenteTest() {

		RemitoProveedorEntity remitoTest = new RemitoProveedorEntity();
		remitoTest.setPuesto(1);
		remitoTest.setNumero(1L);
		remitoTest.setProveedor(personaRepository.findOne(1));
		remitoTest.setFecha(LocalDate.of(2018, 06, 30));
		remitoTest.setComprobanteTipo(comprobanteTipoRepository.findOne(1));
		remitoTest = remitoProveedorRepository.save(remitoTest);

		try {
			remitoProveedorService.insert(new RemitoProveedorDto(remitoTest));
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del remito a insertar ya existe en la base de datos.",
					e.getMessage().equals("El 'id' del remito a insertar ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoSinComprobanteTipoTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo tipo de comprobante es obligatorio.",
					e.getMessage().equals("El campo tipo de comprobante es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoSinProveedorTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo proveedor es obligatorio.",
					e.getMessage().equals("El campo proveedor es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoSinPuestoTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
			remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo puesto es obligatorio.", e.getMessage().equals("El campo puesto es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoSinNumeroTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
			remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
			remitoTest.setPuestoId(1);
			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo número es obligatorio.", e.getMessage().equals("El campo número es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoSinFechaTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
			remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
			remitoTest.setPuestoId(1);
			remitoTest.setNumero(1L);
			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo fecha es obligatorio.", e.getMessage().equals("El campo fecha es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoDuplicadoTest() {

		RemitoProveedorDto remitoTest = new RemitoProveedorDto();
		remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
		remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		remitoTest.setPuestoId(1);
		remitoTest.setNumero(1L);
		remitoTest.setFecha(LocalDate.now());

		RemitoProveedorItemDto item = new RemitoProveedorItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidadOrdenCompra(0);
		item.setCantidad(1);
		List<RemitoProveedorItemDto> items = new ArrayList<RemitoProveedorItemDto>();
		items.add(item);
		remitoTest.setItems(items);

		try {
			remitoProveedorService.insert(remitoTest);
			remitoProveedorService.insert(remitoTest);

			fail("Exception not thrown");
		} catch (Exception e) {
			assertTrue("Ya existe un remito cargado con el mismo puesto y numero para el proveedor seleccionado.",
					e.getMessage().equals(
							"Ya existe un remito cargado con el mismo puesto y numero para el proveedor seleccionado."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoSinItemsTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
			remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
			remitoTest.setPuestoId(1);
			remitoTest.setNumero(1L);
			remitoTest.setFecha(LocalDate.now());
			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El remito debe contener al menos un item.",
					e.getMessage().equals("El remito debe contener al menos un item."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoSinProductosTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
			remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
			remitoTest.setPuestoId(1);
			remitoTest.setNumero(1L);
			remitoTest.setFecha(LocalDate.of(2018, 10, 25));

			RemitoProveedorItemDto item = new RemitoProveedorItemDto();
			item.setId(999);
			List<RemitoProveedorItemDto> items = new ArrayList<RemitoProveedorItemDto>();
			items.add(item);
			remitoTest.setItems(items);

			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("Todos los items deben contener un producto.",
					e.getMessage().equals("Todos los items deben contener un producto."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoSinCantidadTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
			remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
			remitoTest.setPuestoId(1);
			remitoTest.setNumero(1L);
			remitoTest.setFecha(LocalDate.of(2018, 10, 25));

			RemitoProveedorItemDto item = new RemitoProveedorItemDto();
			item.setId(999);
			item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
			item.setOrdenCompra(new OrdenCompraPreviewDto(ordenCompraRepository.findOne(1)));
			item.setCantidadOrdenCompra(1);
			item.setCantidad(2);
			List<RemitoProveedorItemDto> items = new ArrayList<RemitoProveedorItemDto>();
			items.add(item);
			remitoTest.setItems(items);

			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La cantidad de un item con orden de compra no puede superar la cantidad indicada en la misma.",
					e.getMessage().equals(
							"La cantidad de un item con orden de compra no puede superar la cantidad indicada en la misma."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoNrosSerieIncorrectaCantidadTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
			remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
			remitoTest.setPuestoId(1);
			remitoTest.setNumero(1L);
			remitoTest.setFecha(LocalDate.of(2018, 10, 25));

			RemitoProveedorItemDto item = new RemitoProveedorItemDto();
			item.setId(999);
			item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
			item.setOrdenCompra(new OrdenCompraPreviewDto(ordenCompraRepository.findOne(1)));
			item.setCantidadOrdenCompra(1);
			item.setCantidad(1);
			List<String> series = new ArrayList<String>();
			series.add("aaaaaaa");
			series.add("bbbbbbbb");
			item.setNumerosSerie(series);
			List<RemitoProveedorItemDto> items = new ArrayList<RemitoProveedorItemDto>();
			items.add(item);
			remitoTest.setItems(items);

			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("La cantidad de números de serie no puede ser mayor a la cantidad de productos del item.",
					e.getMessage().equals(
							"La cantidad de números de serie no puede ser mayor a la cantidad de productos del item."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoNrosSerieDuplicadosTest() {

		try {
			RemitoProveedorDto remitoTest = new RemitoProveedorDto();
			remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
			remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
			remitoTest.setPuestoId(1);
			remitoTest.setNumero(1L);
			remitoTest.setFecha(LocalDate.of(2018, 10, 25));

			RemitoProveedorItemDto item = new RemitoProveedorItemDto();
			item.setId(999);
			item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
			item.setOrdenCompra(new OrdenCompraPreviewDto(ordenCompraRepository.findOne(1)));
			item.setCantidadOrdenCompra(2);
			item.setCantidad(2);
			List<String> series = new ArrayList<String>();
			series.add("aaaaaaa");
			series.add("aaaaaaa");
			item.setNumerosSerie(series);
			List<RemitoProveedorItemDto> items = new ArrayList<RemitoProveedorItemDto>();
			items.add(item);
			remitoTest.setItems(items);

			remitoProveedorService.insert(remitoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("No puede haber números de serie duplicados.",
					e.getMessage().equals("No puede haber números de serie duplicados."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoInvalidoNrosSerieDuplicadosEnBaseDatosTest() {

		RemitoProveedorDto remitoTest = new RemitoProveedorDto();
		remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
		remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		remitoTest.setPuestoId(1);
		remitoTest.setNumero(1L);
		remitoTest.setFecha(LocalDate.now());

		RemitoProveedorItemDto item = new RemitoProveedorItemDto();
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidad(1);
		List<String> series = new ArrayList<String>();
		series.add("xxxx");
		item.setNumerosSerie(series);
		List<RemitoProveedorItemDto> items = new ArrayList<RemitoProveedorItemDto>();
		items.add(item);
		remitoTest.setItems(items);

		try {
			remitoTest = remitoProveedorService.insert(remitoTest);
			remitoTest.setId(null);
			remitoTest.setPuestoId(1);
			remitoTest.setNumero(2L);
			remitoProveedorService.insert(remitoTest);

			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El número de serie xxxx ya existe en la base de datos.",
					e.getMessage().equals("El número de serie xxxx ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void insertRemitoNoPreexistenteConItemsTest() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		RemitoProveedorDto remitoTest = new RemitoProveedorDto();
		remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
		remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		remitoTest.setPuestoId(1);
		remitoTest.setNumero(1L);
		remitoTest.setFecha(LocalDate.now());

		RemitoProveedorItemDto item = new RemitoProveedorItemDto();
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setCantidadOrdenCompra(0);
		item.setCantidad(1);
		List<String> series = new ArrayList<String>();
		series.add("aaaaaaa");
		item.setNumerosSerie(series);
		List<RemitoProveedorItemDto> items = new ArrayList<RemitoProveedorItemDto>();
		items.add(item);
		remitoTest.setItems(items);

		try {
			ProductoStockEntity stockPrevio = queryFactory.selectFrom(qstock)
					.where(qstock.producto.id.eq(item.getProducto().getId())).fetchOne();

			RemitoProveedorDto newRemito = remitoProveedorService.insert(remitoTest);

			assertTrue("El remito se debe guardar con éxito",
					remitoProveedorRepository.findOne(newRemito.getId()) != null);

			assertTrue("Debe haber items", newRemito.getItems().size() > 0);

			newRemito.getItems().forEach(it -> {

				ProductoStockEntity stockPosterior = queryFactory.selectFrom(qstock)
						.where(qstock.producto.id.eq(it.getProducto().getId())).fetchOne();

				assertTrue("No se debe modificar el stock entrante",
						((stockPrevio != null && stockPrevio.getStockEntrante() != null ? stockPrevio.getStockEntrante()
								: 0) - stockPosterior.getStockEntrante()) == 0);

				assertTrue("Se debe actualizar el stock disponible",
						(stockPosterior.getStockDisponible()
								- (stockPrevio != null && stockPrevio.getStockDisponible() != null
										? stockPrevio.getStockDisponible()
										: 0)) == it.getCantidad());

				assertTrue("No se debe modificar el stock transito",
						(stockPosterior.getStockTransito()
								- (stockPrevio != null && stockPrevio.getStockTransito() != null
										? stockPrevio.getStockTransito()
										: 0)) == 0);

				assertTrue("No se debe modificar el stock reservado",
						(stockPosterior.getStockReservado()
								- (stockPrevio != null && stockPrevio.getStockReservado() != null
										? stockPrevio.getStockReservado()
										: 0)) == 0);

				assertTrue("No se debe modificar el stock vendido", (stockPosterior.getStockVendido()
						- (stockPrevio != null && stockPrevio.getStockVendido() != null ? stockPrevio.getStockVendido()
								: 0)) == 0);

			});
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void updateRemitoNoPreexistenteTest() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QRemitoProveedorEntity qremitocompra = QRemitoProveedorEntity.remitoProveedorEntity;

		Integer lastId = queryFactory.select(qremitocompra.id).from(qremitocompra).orderBy(qremitocompra.id.desc())
				.fetchFirst();
		if (lastId == null) {
			lastId = 0;
		}
		lastId++;

		RemitoProveedorDto remitoPreexistente = new RemitoProveedorDto();
		remitoPreexistente.setId(lastId);

		try {
			remitoProveedorService.update(remitoPreexistente.getId(), "");
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del remito que desea modificar no existe en la base de datos.",
					e.getMessage().equals("El 'id' del remito que desea modificar no existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void updateRemitoPreexistenteConItemsTest() {

		RemitoProveedorDto remitoTest = new RemitoProveedorDto();
		remitoTest.setComprobanteTipo(new ComprobanteTipoDto(comprobanteTipoRepository.findOne(1)));
		remitoTest.setProveedor(new ProveedorDto(personaRepository.findOne(1)));
		remitoTest.setPuestoId(1);
		remitoTest.setNumero(1L);
		remitoTest.setFecha(LocalDate.now());

		RemitoProveedorItemDto item = new RemitoProveedorItemDto();
		item.setId(999);
		item.setProducto(new ProductoPreviewDto(productoRepository.findOne(1)));
		item.setOrdenCompra(new OrdenCompraPreviewDto(ordenCompraRepository.findOne(1)));
		item.setCantidadOrdenCompra(0);
		item.setCantidad(1);
		List<RemitoProveedorItemDto> items = new ArrayList<RemitoProveedorItemDto>();
		items.add(item);
		remitoTest.setItems(items);

		try {
			RemitoProveedorDto newRemito = remitoProveedorService.insert(remitoTest);

			newRemito.setNumero(2L);

			RemitoProveedorDto editRemito = remitoProveedorService.update(newRemito.getId(), "EDITADO");

			assertTrue("El remito no debe modificar su número", editRemito.getNumero().equals(1L));
			assertTrue("El remito debe actualizar su observacion", editRemito.getObservaciones().equals("EDITADO"));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

}
