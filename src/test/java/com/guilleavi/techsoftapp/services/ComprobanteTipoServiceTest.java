package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.ComprobanteTipoDto;
import com.guilleavi.techsoftapp.entities.ComprobanteTipoEntity;
import com.guilleavi.techsoftapp.entities.QComprobanteTipoEntity;
import com.guilleavi.techsoftapp.enums.ComprobanteTipoEnum;
import com.guilleavi.techsoftapp.repositories.ComprobanteTipoRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Transactional(timeout = 300)
public class ComprobanteTipoServiceTest extends AbstractTest {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	ComprobanteTipoService comprobanteTipoService = null;
	@Autowired
	ComprobanteTipoRepository comprobanteTipoRepository = null;

	@Test
	@Rollback
	public void getComprobanteTipoConIdNullTest() {

		Integer idNull = null;

		try {
			comprobanteTipoService.getComprobanteTipo(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del tipo de comprobante no puede ser nulo.",
					e.getMessage().equals("El 'id' del tipo de comprobante no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getComprobanteTipoConIdExistenteTest() {

		ComprobanteTipoEntity comprobanteTipoTest = new ComprobanteTipoEntity();
		comprobanteTipoTest.setLetra("X");
		comprobanteTipoTest.setTipo(ComprobanteTipoEnum.REMITO.getValor());

		Integer idTest = comprobanteTipoRepository.save(comprobanteTipoTest).getId();

		try {
			ComprobanteTipoDto comprobanteTipoDto = comprobanteTipoService.getComprobanteTipo(idTest);
			assertTrue("El id del tipo de comprobante debe coincidir con el filtro",
					comprobanteTipoDto.getId().equals(idTest));
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getComprobanteTipoConIdInexistenteTest() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QComprobanteTipoEntity qcomprobanteTipo = QComprobanteTipoEntity.comprobanteTipoEntity;

		Integer lastId = queryFactory.select(qcomprobanteTipo.id).from(qcomprobanteTipo)
				.orderBy(qcomprobanteTipo.id.desc()).fetchFirst();
		lastId++;

		try {
			ComprobanteTipoDto comprobanteTipoDto = comprobanteTipoService.getComprobanteTipo(lastId);
			assertTrue("El resultado debe ser nulo", comprobanteTipoDto == null);
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getRemitosTiposTest() {

		ComprobanteTipoEntity comprobanteTipoTest = new ComprobanteTipoEntity();
		comprobanteTipoTest.setLetra("R");
		comprobanteTipoTest.setTipo(ComprobanteTipoEnum.REMITO.getValor());
		comprobanteTipoRepository.save(comprobanteTipoTest);

		comprobanteTipoTest = new ComprobanteTipoEntity();
		comprobanteTipoTest.setLetra("F");
		comprobanteTipoTest.setTipo(ComprobanteTipoEnum.FACTURA.getValor());
		comprobanteTipoRepository.save(comprobanteTipoTest);

		try {
			List<ComprobanteTipoDto> comprobanteTipoDtos = comprobanteTipoService.getRemitoTipos();
			Long remitosFiltrados = comprobanteTipoDtos.stream()
					.filter(comprobante -> comprobante.getTipo().equals(ComprobanteTipoEnum.REMITO.getValor())).count();
			assertTrue("Todos los comprobantes deben ser del tipo remitos",
					comprobanteTipoDtos.size() == remitosFiltrados);
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void insertComprobanteTipoConComprobanteTipoNullTest() {

		ComprobanteTipoDto comprobanteTipoNull = null;

		try {
			comprobanteTipoService.insert(comprobanteTipoNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El tipo de comprobante a insertar no se encuentra definido.",
					e.getMessage().equals("El tipo de comprobante a insertar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void insertComprobanteTipoYaExistenteTest() {

		ComprobanteTipoEntity comprobanteTipoTest = new ComprobanteTipoEntity();
		comprobanteTipoTest.setLetra("Z");
		comprobanteTipoTest.setTipo(ComprobanteTipoEnum.REMITO.getValor());
		Integer idTest = comprobanteTipoRepository.save(comprobanteTipoTest).getId();

		ComprobanteTipoDto comprobanteTipoExistente = new ComprobanteTipoDto();
		comprobanteTipoExistente.setId(idTest);

		try {
			comprobanteTipoService.insert(comprobanteTipoExistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del tipo de comprobante a insertar ya existe en la base de datos.",
					e.getMessage().equals("El 'id' del tipo de comprobante a insertar ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void insertComprobanteTipoSinLetraTest() {

		ComprobanteTipoDto comprobanteTipoTest = new ComprobanteTipoDto();
		comprobanteTipoTest.setTipo(ComprobanteTipoEnum.REMITO.getValor());

		try {
			comprobanteTipoService.insert(comprobanteTipoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo letra es obligatorio.",
					e.getMessage().equals("El campo letra es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertComprobanteTipoSinTipoTest() {

		ComprobanteTipoDto comprobanteTipoTest = new ComprobanteTipoDto();
		comprobanteTipoTest.setLetra("Z");

		try {
			comprobanteTipoService.insert(comprobanteTipoTest);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El campo tipo es obligatorio.",
					e.getMessage().equals("El campo tipo es obligatorio."));
		}
	}

	@Test
	@Rollback
	public void insertComprobanteTipoNoPreexistenteTest() {

		ComprobanteTipoDto comprobanteTipoTest = new ComprobanteTipoDto();
		comprobanteTipoTest.setLetra("Z");
		comprobanteTipoTest.setTipo(ComprobanteTipoEnum.REMITO.getValor());

		try {
			Integer newId = comprobanteTipoService.insert(comprobanteTipoTest).getId();

			assertTrue("El comprobante tipo se debe guardar con éxito",
					comprobanteTipoRepository.findOne(newId) != null);

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}
	
	@Test
	@Rollback
	public void updateComprobanteTipoConComprobanteTipoNullTest() {

		ComprobanteTipoDto comprobanteTipoNull = null;

		try {
			comprobanteTipoService.update(comprobanteTipoNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El tipo de comprobante a modificar no se encuentra definido.",
					e.getMessage().equals("El tipo de comprobante a modificar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void updateComprobanteTipoNoPreexistenteTest() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QComprobanteTipoEntity qcomprobanteTipo = QComprobanteTipoEntity.comprobanteTipoEntity;

		Integer lastId = queryFactory.select(qcomprobanteTipo.id).from(qcomprobanteTipo)
				.orderBy(qcomprobanteTipo.id.desc()).fetchFirst();
		lastId++;

		ComprobanteTipoDto comprobanteTipoNoPreexistente = new ComprobanteTipoDto();
		comprobanteTipoNoPreexistente.setId(lastId);

		try {
			comprobanteTipoService.update(comprobanteTipoNoPreexistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del tipo de comprobante que desea modificar no existe en la base de datos.",
					e.getMessage().equals(
							"El 'id' del tipo de comprobante que desea modificar no existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void updateComprobanteTipoPreexistenteTest() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QComprobanteTipoEntity qcomprobanteTipo = QComprobanteTipoEntity.comprobanteTipoEntity;

		ComprobanteTipoEntity comprobanteTipo = queryFactory.selectFrom(qcomprobanteTipo)
				.orderBy(qcomprobanteTipo.id.desc()).fetchFirst();
		comprobanteTipo.setLetra("T");

		try {
			ComprobanteTipoDto result = comprobanteTipoService.update(new ComprobanteTipoDto(comprobanteTipo));

			assertTrue("El comprobante tipo se debe actualizar con éxito", result.getLetra().equals("T"));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void deleteComprobanteTipoConIdNullTest() {

		Integer idNull = null;

		try {
			comprobanteTipoService.delete(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de tipo de comprobante no puede ser nulo.",
					e.getMessage().equals("El 'id' de tipo de comprobante no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void deleteComprobanteTipoNoExistenteTest() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QComprobanteTipoEntity qcomprobanteTipo = QComprobanteTipoEntity.comprobanteTipoEntity;

		Integer lastId = queryFactory.select(qcomprobanteTipo.id).from(qcomprobanteTipo)
				.orderBy(qcomprobanteTipo.id.desc()).fetchFirst();
		lastId++;

		try {
			comprobanteTipoService.delete(lastId);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El tipo de comprobante que se desea eliminar no existe.",
					e.getMessage().equals("El tipo de comprobante que se desea eliminar no existe."));
		}
	}

	@Test
	@Rollback
	public void deleteComprobanteTipoExistenteTest() {

		ComprobanteTipoDto comprobanteTipoTest = new ComprobanteTipoDto();
		comprobanteTipoTest.setLetra("Z");
		comprobanteTipoTest.setTipo(ComprobanteTipoEnum.REMITO.getValor());

		Integer newId = comprobanteTipoService.insert(comprobanteTipoTest).getId();

		try {
			comprobanteTipoService.delete(newId);

			assertTrue("El comprobante tipo se debe eliminar con éxito",
					comprobanteTipoRepository.findOne(newId) == null);

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

}
