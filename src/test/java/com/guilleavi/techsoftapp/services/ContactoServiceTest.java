package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.ContactoDto;
import com.guilleavi.techsoftapp.entities.ContactoEntity;
import com.guilleavi.techsoftapp.entities.EstadoEntity;
import com.guilleavi.techsoftapp.entities.PersonaEntity;
import com.guilleavi.techsoftapp.entities.SysContactoTipoEntity;
import com.guilleavi.techsoftapp.enums.ContactoEnum;
import com.guilleavi.techsoftapp.repositories.ContactoRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;

@Transactional(timeout = 300)
public class ContactoServiceTest extends AbstractTest {

	@Autowired
	ContactoService contactoService = null;
	@Autowired
	ContactoRepository contactoRepository = null;
	@Autowired
	PersonaRepository personaRepository = null;

	@Test
	@Rollback
	public void getConctactoConIdNullTest() {

		Integer idNull = null;

		try {
			contactoService.getContacto(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de contacto no puede ser nulo.",
					e.getMessage().equals("El 'id' de contacto no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getContactoConIdTest() {

		Integer id = 1;

		try {
			ContactoDto contactoDto = contactoService.getContacto(id);
			assertTrue("El id del contacto debe coincidir con el filtro", contactoDto.getId().equals(id));
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getContactosConPersonIdNullTest() {

		Integer personaIdNull = null;

		try {
			contactoService.getContactosByPersona(personaIdNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de persona no puede ser nulo.",
					e.getMessage().equals("El 'id' de persona no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getContactosConTipoNullTest() {

		Integer personaId = 1;
		Integer tipoNull = null;

		try {
			contactoService.getContactosByPersona(personaId, tipoNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'tipo' de contacto no puede ser nulo.",
					e.getMessage().equals("El 'tipo' de contacto no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getContactosConPersonaYTipoTest() {

		Integer personaId = 1;
		Integer tipo = ContactoEnum.TELEFONO.getValor();

		try {
			List<ContactoDto> contactoDtos = contactoService.getContactosByPersona(personaId, tipo);

			Long matchedCount = contactoDtos.stream()
					.filter(c -> (c.getPersonaId().equals(personaId) && c.getTipo().equals(tipo))).count();

			assertTrue("El id de persona y el tipo de contacto deben coincidir con los filtros",
					contactoDtos.size() == matchedCount);

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getTelefonoPrincipalTest() {

		PersonaEntity personaTest = new PersonaEntity();
		personaTest.setNombre("PERSONA TEST");
		personaTest.setEstado(new EstadoEntity(1, "ACTIVO"));
		personaTest = personaRepository.save(personaTest);

		ContactoEntity contactoTest = new ContactoEntity();
		contactoTest.setTipo(new SysContactoTipoEntity(ContactoEnum.TELEFONO.getValor(), "TELEFONO"));
		contactoTest.setPersona(personaTest);
		contactoTest.setContacto("11-1234-1234");

		contactoRepository.save(contactoTest);

		try {
			String telefonoPrincipal = contactoService.getTelefonoPrincipal(personaTest.getId());

			assertTrue("El telefono devuelte debe ser el principal", telefonoPrincipal.equals("11-1234-1234"));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void insertContactoConContactoNullTest() {

		ContactoDto contactoNull = null;

		try {
			contactoService.upsert(contactoNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El contacto a insertar no se encuentra definido.",
					e.getMessage().equals("El contacto a insertar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void insertContactoYaExistenteTest() {

		ContactoDto contactoExistente = new ContactoDto();
		contactoExistente.setId(1);

		try {
			contactoService.upsert(contactoExistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del contacto a insertar ya existe en la base de datos.",
					e.getMessage().equals("El 'id' del contacto a insertar ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void deleteContactoConIdNullTest() {

		Integer idNull = null;

		try {
			contactoService.delete(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de contacto no puede ser nulo.",
					e.getMessage().equals("El 'id' de contacto no puede ser nulo."));
		}
	}

}
