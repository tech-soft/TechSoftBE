package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.BancoDto;
import com.guilleavi.techsoftapp.entities.BancoEntity;
import com.guilleavi.techsoftapp.repositories.BancoRepository;

@Transactional(timeout = 300)
public class BancoServiceTest extends AbstractTest {

	@Autowired
	BancoService bancoService = null;
	@Autowired
	BancoRepository bancoRepository = null;

	@Test
	@Rollback
	public void getBancoConIdNullTest() {

		Integer idNull = null;

		try {
			bancoService.getBanco(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de banco no puede ser nulo.",
					e.getMessage().equals("El 'id' de banco no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getBancoConIdTest() {

		// datos de prueba
		BancoEntity bancoTest = new BancoEntity();
		bancoTest.setNombre("BANCO TEST");
		Integer idTest = bancoRepository.save(bancoTest).getId();

		// test get banco by id
		try {
			BancoDto bancoDto = bancoService.getBanco(idTest);
			assertTrue("El id del banco obtenido debe coincidir con el filtro", bancoDto.getId().equals(idTest));
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void insertBancoConBancoNullTest() {

		BancoDto bancoNull = null;

		try {
			bancoService.insert(bancoNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El banco a insertar no se encuentra definido.",
					e.getMessage().equals("El banco a insertar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void insertBancoYaExistenteTest() {

		// datos de prueba
		BancoEntity bancoTest = new BancoEntity();
		bancoTest.setNombre("BANCO TEST");
		Integer idTest = bancoRepository.save(bancoTest).getId();

		BancoDto bancoExistente = new BancoDto();
		bancoExistente.setId(idTest);

		try {
			bancoService.insert(bancoExistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del banco a insertar ya existe en la base de datos.",
					e.getMessage().equals("El 'id' del banco a insertar ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void updateBancoConBancoNullTest() {

		BancoDto bancoNull = null;

		try {
			bancoService.update(bancoNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El banco a modificar no se encuentra definido.",
					e.getMessage().equals("El banco a modificar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void updateBancoNoPreexistenteTest() {

		// datos de prueba
		Integer idTest = 999;
		if (bancoRepository.findOne(idTest) != null) {
			bancoRepository.delete(idTest);
		}

		BancoDto bancoNoPreexistente = new BancoDto();
		bancoNoPreexistente.setId(idTest);

		try {
			bancoService.update(bancoNoPreexistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del banco que desea modificar no existe en la base de datos.",
					e.getMessage().equals("El 'id' del banco que desea modificar no existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void deleteBancoConIdNullTest() {

		Integer idNull = null;

		try {
			bancoService.delete(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de banco no puede ser nulo.",
					e.getMessage().equals("El 'id' de banco no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void deleteBancoNoExistenteTest() {

		Integer idInexistente = 30000;

		try {
			bancoService.delete(idInexistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El banco que se desea eliminar no existe.",
					e.getMessage().equals("El banco que se desea eliminar no existe."));
		}
	}

}
