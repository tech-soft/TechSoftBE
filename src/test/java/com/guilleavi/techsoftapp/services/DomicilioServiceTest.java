package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.DomicilioDto;
import com.guilleavi.techsoftapp.entities.DomicilioEntity;
import com.guilleavi.techsoftapp.entities.EstadoEntity;
import com.guilleavi.techsoftapp.entities.PersonaEntity;
import com.guilleavi.techsoftapp.repositories.CiudadRepository;
import com.guilleavi.techsoftapp.repositories.DomicilioRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;

@Transactional(timeout = 300)
public class DomicilioServiceTest extends AbstractTest {

	@Autowired
	DomicilioService domicilioService = null;

	@Autowired
	CiudadRepository ciudadRepository = null;
	@Autowired
	DomicilioRepository domicilioRepository = null;
	@Autowired
	PersonaRepository personaRepository = null;

	@Test
	@Rollback
	public void getDomicilio_NullId_Test() {

		Integer idNull = null;

		try {
			domicilioService.getDomicilio(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de domicilio no puede ser nulo.",
					e.getMessage().equals("El 'id' de domicilio no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getDomicilio_IdInexistente_Test() {

	}

	@Test
	@Rollback
	public void getDomicilio_IdExistente_Test() {

		Integer id = 1;

		try {
			DomicilioDto domicilioDto = domicilioService.getDomicilio(id);
			assertTrue("El id de domicilio debe coincidir con el filtro", domicilioDto.getId().equals(id));
		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getDomiciliosConPersonIdNullTest() {

		Integer personaIdNull = null;

		try {
			domicilioService.getDomiciliosByPersona(personaIdNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de persona no puede ser nulo.",
					e.getMessage().equals("El 'id' de persona no puede ser nulo."));
		}
	}

	@Test
	@Rollback
	public void getDomiciliosConPersonaIdTest() {

		Integer personaId = 1;

		try {
			List<DomicilioDto> domicilioDtos = domicilioService.getDomiciliosByPersona(personaId);

			Long matchedCount = domicilioDtos.stream().filter(c -> (c.getPersonaId().equals(personaId))).count();

			assertTrue("El id de persona debe coincidir con el filtro", domicilioDtos.size() == matchedCount);

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void getDomicilioPrincipalTest() {

		PersonaEntity personaTest = new PersonaEntity();
		personaTest.setNombre("PERSONA TEST");
		personaTest.setEstado(new EstadoEntity(1, "ACTIVO"));
		personaTest = personaRepository.save(personaTest);

		DomicilioEntity domicilioTest = new DomicilioEntity();
		domicilioTest.setCalle("CALLE TEST");
		domicilioTest.setNro("1234");
		domicilioTest.setCiudad(ciudadRepository.findOne(1));
		domicilioTest.setPersona(personaTest);
		domicilioTest.setPrincipal(false);
		domicilioRepository.save(domicilioTest);

		domicilioTest.setCalle("CALLE PRUEBA");
		domicilioTest.setNro("4758");
		domicilioTest.setCiudad(ciudadRepository.findOne(1));
		domicilioTest.setPersona(personaTest);
		domicilioTest.setPrincipal(true);
		domicilioRepository.save(domicilioTest);

		try {
			String domicilioPrincipal = domicilioService.getDomicilioPrincipal(personaTest.getId());

			System.out.println(domicilioPrincipal);
			assertTrue("El domicilio debe coincidir con el principal",
					domicilioPrincipal.equals("CALLE PRUEBA 4758 ROSARIO"));

		} catch (Exception e) {
			fail("Should not throw exception");
		}
	}

	@Test
	@Rollback
	public void insertDomicilioConDomicilioNullTest() {

		DomicilioDto domicilioNull = null;

		try {
			domicilioService.upsert(domicilioNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El domicilio a insertar no se encuentra definido.",
					e.getMessage().equals("El domicilio a insertar no se encuentra definido."));
		}
	}

	@Test
	@Rollback
	public void insertDomicilioYaExistenteTest() {

		DomicilioDto domicilioExistente = new DomicilioDto();
		domicilioExistente.setId(1);

		try {
			domicilioService.upsert(domicilioExistente);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' del domicilio a insertar ya existe en la base de datos.",
					e.getMessage().equals("El 'id' del domicilio a insertar ya existe en la base de datos."));
		}
	}

	@Test
	@Rollback
	public void deleteDomicilioConIdNullTest() {

		Integer idNull = null;

		try {
			domicilioService.delete(idNull);
			fail("Exception not thrown");

		} catch (Exception e) {
			assertTrue("El 'id' de domicilio no puede ser nulo.",
					e.getMessage().equals("El 'id' de domicilio no puede ser nulo."));
		}
	}

}
