package com.guilleavi.techsoftapp.services;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.AbstractTest;
import com.guilleavi.techsoftapp.dto.ProveedorDto;
import com.guilleavi.techsoftapp.entities.PersonaEntity;
import com.guilleavi.techsoftapp.enums.EstadoEnum;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;

@Transactional(timeout = 300)
public class ProveedorServiceTest extends AbstractTest {

	@Autowired
	ProveedorService proveedorService = null;
	@Autowired
	PersonaRepository personaRepository = null;

	@Test
	@Rollback
	public void getProveedoresActivosTest() {

		List<ProveedorDto> proveedores = proveedorService.getProveedoresActivos();

		proveedores.forEach(proveedor -> {
			PersonaEntity persona = personaRepository.findOne(proveedor.getId());

			assertTrue("La persona debe ser proveedor", persona.getProveedor());
			assertTrue("El proveedor debe estar activo", persona.getEstado().getId().equals(EstadoEnum.ACTIVO.getValor()));
		});
	}

}
