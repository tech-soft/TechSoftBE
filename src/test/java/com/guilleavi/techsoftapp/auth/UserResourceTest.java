package com.guilleavi.techsoftapp.auth;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.guilleavi.techsoftapp.AbstractApiTest;
import com.guilleavi.techsoftapp.dto.UsuarioDto;
import com.guilleavi.techsoftapp.enums.Authority;

public class UserResourceTest extends AbstractApiTest {

	@Test
	public void getUsersAsAnonymous() {
		Response response = cliente.target(baseUri).path("usuarios").request().get();
		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}

	@Test
	public void getUsersAsAsUser() {

		String authorizationHeader = composeAuthorizationHeader(getTokenForUser());

		Response response = cliente.target(baseUri).path("usuarios").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();
		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}

	@Test
	public void getUsersAsAsAdmin() {

		String authorizationHeader = composeAuthorizationHeader(getTokenForAdmin());

		Response response = cliente.target(baseUri).path("usuarios").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		List<UsuarioDto> queryResults = response.readEntity(new GenericType<List<UsuarioDto>>() {
		});
		assertNotNull(queryResults);
		assertThat(queryResults, hasSize(2));
	}

	@Test
	public void getUserAsAnonymous() {

		Integer userId = 1;

		Response response = cliente.target(baseUri).path("usuarios").path(userId.toString()).request().get();
		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}

	@Test
	public void getUserAsAsUser() {

		Integer userId = 1;

		String authorizationHeader = composeAuthorizationHeader(getTokenForUser());

		Response response = cliente.target(baseUri).path("usuarios").path(userId.toString()).request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();
		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}

	@Test
	public void getUserAsAsAdmin() {

		Integer userId = 1;

		String authorizationHeader = composeAuthorizationHeader(getTokenForAdmin());

		Response response = cliente.target(baseUri).path("usuarios").path(userId.toString()).request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		UsuarioDto queryResults = response.readEntity(UsuarioDto.class);
		assertNotNull(queryResults);
		assertEquals(userId, queryResults.getId());
	}

	@Test
	public void getAuthenticatedUserAsAsUser() {

		String authorizationHeader = composeAuthorizationHeader(getTokenForUser());

		Response response = cliente.target(baseUri).path("usuarios").path("me").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		UsuarioDto queryResults = response.readEntity(UsuarioDto.class);
		assertNotNull(queryResults.getId());
		assertEquals("user", queryResults.getUsername());
		assertThat(queryResults.getGrupos(), containsInAnyOrder(Authority.USER));
	}

	@Test
	public void getAuthenticatedUserAsAsAdmin() {

		String authorizationHeader = composeAuthorizationHeader(getTokenForAdmin());

		Response response = cliente.target(baseUri).path("usuarios").path("me").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		UsuarioDto queryResults = response.readEntity(UsuarioDto.class);
		assertNotNull(queryResults.getId());
		assertEquals("admin", queryResults.getUsername());
		assertThat(queryResults.getGrupos(), containsInAnyOrder(Authority.USER, Authority.ADMIN));
	}
}