package com.guilleavi.techsoftapp.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.guilleavi.techsoftapp.AbstractApiTest;
import com.guilleavi.techsoftapp.dto.AuthenticationTokenDto;
import com.guilleavi.techsoftapp.dto.CredencialDto;

public class AuthenticationResourceTest extends AbstractApiTest {

	@Test
	public void authenticateWithValidCredentials() {

		CredencialDto credenciales = new CredencialDto();
		credenciales.setUsername("admin");
		credenciales.setPassword("password");

		Response response = cliente.target(baseUri).path("auth").request()
				.post(Entity.entity(credenciales, MediaType.APPLICATION_JSON));
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		AuthenticationTokenDto authenticationToken = response.readEntity(AuthenticationTokenDto.class);
		assertNotNull(authenticationToken);
		assertNotNull(authenticationToken.getToken());
	}

	@Test
	public void authenticateWithInvalidCredentials() {

		CredencialDto credenciales = new CredencialDto();
		credenciales.setUsername("invalid-user");
		credenciales.setPassword("wrong-password");

		Response response = cliente.target(baseUri).path("auth").request()
				.post(Entity.entity(credenciales, MediaType.APPLICATION_JSON));
		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}
}
