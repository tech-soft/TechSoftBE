package com.guilleavi.techsoftapp.datasources;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import com.guilleavi.techsoftapp.AbstractApiTest;

public class UsuariosDataSourceTest extends AbstractApiTest {

	String authorizationHeader = null;

	@Before
	public void setup() {

		authorizationHeader = composeAuthorizationHeader(getTokenForAdmin());
	}

	@Test
	public void getUsuarioTest() {

		Response response = cliente.target(baseUri).path("/usuarios/1").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	public void getUsuariosTest() {

		Response response = cliente.target(baseUri).path("/usuarios").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	public void getUsuarioAutenticadoTest() {

		Response response = cliente.target(baseUri).path("/usuarios/me").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}
}