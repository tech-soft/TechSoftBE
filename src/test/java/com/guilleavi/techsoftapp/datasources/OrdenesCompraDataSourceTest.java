package com.guilleavi.techsoftapp.datasources;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import com.guilleavi.techsoftapp.AbstractApiTest;

public class OrdenesCompraDataSourceTest extends AbstractApiTest {

	String authorizationHeader = null;

	@Before
	public void setup() {

		authorizationHeader = composeAuthorizationHeader(getTokenForAdmin());
	}

	@Test
	public void getOrdenCompraTest() {

		Response response = cliente.target(baseUri).path("/ordenes-compra/1").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	public void getOrdenesCompraTest() {

		Response response = cliente.target(baseUri).path("/ordenes-compra").queryParam("proveedorId", 1).request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	public void getOrdenesEmitidasTest() {

		Response response = cliente.target(baseUri).path("/ordenes-compra/emitidas").queryParam("proveedorId", 1).request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

}