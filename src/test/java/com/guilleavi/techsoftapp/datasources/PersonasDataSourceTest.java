package com.guilleavi.techsoftapp.datasources;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import com.guilleavi.techsoftapp.AbstractApiTest;

public class PersonasDataSourceTest extends AbstractApiTest {

	String authorizationHeader = null;

	@Before
	public void setup() {

		authorizationHeader = composeAuthorizationHeader(getTokenForAdmin());
	}

	@Test
	public void getPersonaTest() {

		Response response = cliente.target(baseUri).path("/personas/1").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	public void getPersonasTest() {

		Response response = cliente.target(baseUri).path("/personas").queryParam("q", "name").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	public void getDatosComercialesTest() {

		Response response = cliente.target(baseUri).path("/personas/1/datos-comerciales").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	public void getPersonasRelacionadasTest() {

		Response response = cliente.target(baseUri).path("/personas/1/relacionadas").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	public void getContactosTest() {

		Response response = cliente.target(baseUri).path("/personas/1/contactos/1").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	public void getDomiciliosTest() {

		Response response = cliente.target(baseUri).path("/personas/1/domicilios").request()
				.header(HttpHeaders.AUTHORIZATION, authorizationHeader).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

}