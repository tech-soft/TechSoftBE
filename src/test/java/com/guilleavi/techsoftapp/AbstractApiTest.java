package com.guilleavi.techsoftapp;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.guilleavi.techsoftapp.dto.AuthenticationTokenDto;
import com.guilleavi.techsoftapp.dto.CredencialDto;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractApiTest {

	@LocalServerPort
	protected int puerto;

	protected URI baseUri;

	protected Client cliente;

	@Before
	public void setUp() throws Exception {
		this.baseUri = new URI("http://localhost:" + puerto + "/techsoftapp");
		this.cliente = ClientBuilder.newClient();
	}

	public String getTokenForUser() {

		CredencialDto credenciales = new CredencialDto();
		credenciales.setUsername("user");
		credenciales.setPassword("password");

		AuthenticationTokenDto authenticationToken = cliente.target(baseUri).path("auth").request()
				.post(Entity.entity(credenciales, MediaType.APPLICATION_JSON), AuthenticationTokenDto.class);
		return authenticationToken.getToken();
	}

	public String getTokenForAdmin() {

		CredencialDto credenciales = new CredencialDto();
		credenciales.setUsername("admin");
		credenciales.setPassword("password");

		AuthenticationTokenDto authenticationToken = cliente.target(baseUri).path("auth").request()
				.post(Entity.entity(credenciales, MediaType.APPLICATION_JSON), AuthenticationTokenDto.class);
		return authenticationToken.getToken();
	}

	protected String composeAuthorizationHeader(String authenticationToken) {
		return "Bearer" + " " + authenticationToken;
	}
}
