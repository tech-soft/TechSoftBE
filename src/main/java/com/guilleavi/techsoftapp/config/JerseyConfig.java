package com.guilleavi.techsoftapp.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.auth.mappers.ObjectMapperProvider;
import com.guilleavi.techsoftapp.auth.mappers.exceptions.AccessDeniedExceptionMapper;
import com.guilleavi.techsoftapp.auth.mappers.exceptions.AuthenticationExceptionMapper;
import com.guilleavi.techsoftapp.auth.mappers.exceptions.AuthenticationTokenRefreshmentExceptionMapper;
import com.guilleavi.techsoftapp.datasources.AuthenticationsDataSource;
import com.guilleavi.techsoftapp.datasources.BancosDataSource;
import com.guilleavi.techsoftapp.datasources.CiudadesDataSource;
import com.guilleavi.techsoftapp.datasources.ComprobanteTiposDataSource;
import com.guilleavi.techsoftapp.datasources.ContactosDataSource;
import com.guilleavi.techsoftapp.datasources.DocumentoTiposDataSource;
import com.guilleavi.techsoftapp.datasources.DomiciliosDataSource;
import com.guilleavi.techsoftapp.datasources.EstadosDataSource;
import com.guilleavi.techsoftapp.datasources.GravadosDataSources;
import com.guilleavi.techsoftapp.datasources.IvaTiposDataSource;
import com.guilleavi.techsoftapp.datasources.LogJSErrorsDataSource;
import com.guilleavi.techsoftapp.datasources.MonedasDataSource;
import com.guilleavi.techsoftapp.datasources.OrdenesCompraDataSource;
import com.guilleavi.techsoftapp.datasources.OrigenesDataSource;
import com.guilleavi.techsoftapp.datasources.PagoFormasDataSource;
import com.guilleavi.techsoftapp.datasources.PaisesDataSource;
import com.guilleavi.techsoftapp.datasources.PersonaComercialesDataSource;
import com.guilleavi.techsoftapp.datasources.PersonaRelacionTiposDataSource;
import com.guilleavi.techsoftapp.datasources.PersonasDataSource;
import com.guilleavi.techsoftapp.datasources.PersonasRelacionadasDataSource;
import com.guilleavi.techsoftapp.datasources.PrecioListasDataSource;
import com.guilleavi.techsoftapp.datasources.ProductosDataSource;
import com.guilleavi.techsoftapp.datasources.ProveedoresDataSource;
import com.guilleavi.techsoftapp.datasources.ProvinciasDataSource;
import com.guilleavi.techsoftapp.datasources.PuestosDataSource;
import com.guilleavi.techsoftapp.datasources.RemitosProveedorDataSource;
import com.guilleavi.techsoftapp.datasources.SucursalesDataSource;
import com.guilleavi.techsoftapp.datasources.UsuariosDataSource;

@Component
@ApplicationPath("techsoftapp")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {

		register(AccessDeniedExceptionMapper.class);
		register(AuthenticationExceptionMapper.class);
		register(AuthenticationTokenRefreshmentExceptionMapper.class);
		register(ObjectMapperProvider.class);

		register(AuthenticationsDataSource.class);
		register(BancosDataSource.class);
		register(CiudadesDataSource.class);
		register(ComprobanteTiposDataSource.class);
		register(ContactosDataSource.class);
		register(DocumentoTiposDataSource.class);
		register(DomiciliosDataSource.class);
		register(EstadosDataSource.class);
		register(GravadosDataSources.class);
		register(IvaTiposDataSource.class);
		register(LogJSErrorsDataSource.class);
		register(MonedasDataSource.class);
		register(OrdenesCompraDataSource.class);
		register(OrigenesDataSource.class);
		register(PagoFormasDataSource.class);
		register(PaisesDataSource.class);
		register(PersonaComercialesDataSource.class);
		register(PersonaRelacionTiposDataSource.class);
		register(PersonasDataSource.class);
		register(PersonasRelacionadasDataSource.class);
		register(PrecioListasDataSource.class);
		register(ProductosDataSource.class);
		register(ProveedoresDataSource.class);
		register(ProvinciasDataSource.class);
		register(PuestosDataSource.class);
		register(RemitosProveedorDataSource.class);
		register(SucursalesDataSource.class);
		register(UsuariosDataSource.class);

	}

}
