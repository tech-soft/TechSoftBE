package com.guilleavi.techsoftapp.auth;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.auth.exceptions.InvalidAuthenticationTokenException;
import com.guilleavi.techsoftapp.enums.Authority;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.InvalidClaimException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenParser {

	@Autowired
	private JwtSettings settings;

	public AuthenticationTokenDetails parseToken(String token) {

		try {

			Claims claims = Jwts.parser().setSigningKey(settings.getSecret()).requireAudience(settings.getAudience())
					.setAllowedClockSkewSeconds(settings.getClockSkew()).parseClaimsJws(token).getBody();

			return new AuthenticationTokenDetails.Builder().withId(extractTokenIdFromClaims(claims))
					.withUsername(extractUsernameFromClaims(claims))
					.withAuthorities(extractAuthoritiesFromClaims(claims))
					.withIssuedDate(extractIssuedDateFromClaims(claims))
					.withExpirationDate(extractExpirationDateFromClaims(claims))
					.withRefreshCount(extractRefreshCountFromClaims(claims))
					.withRefreshLimit(extractRefreshLimitFromClaims(claims)).build();

		} catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException e) {
			throw new InvalidAuthenticationTokenException("Token Inválido", e);
		} catch (ExpiredJwtException e) {
			throw new InvalidAuthenticationTokenException("Token Expirado", e);
		} catch (InvalidClaimException e) {
			throw new InvalidAuthenticationTokenException("Invalid value for claim \"" + e.getClaimName() + "\"", e);
		} catch (Exception e) {
			throw new InvalidAuthenticationTokenException("Token Inválido", e);
		}
	}

	private String extractTokenIdFromClaims(@NotNull Claims claims) {
		return (String) claims.get(Claims.ID);
	}

	private String extractUsernameFromClaims(@NotNull Claims claims) {
		return claims.getSubject();
	}

	private Set<Authority> extractAuthoritiesFromClaims(@NotNull Claims claims) {
		@SuppressWarnings("unchecked")
		List<String> rolesAsString = (List<String>) claims.getOrDefault(settings.getAuthoritiesClaimName(),
				new ArrayList<>());
		return rolesAsString.stream().map(Authority::valueOf).collect(Collectors.toSet());
	}

	private ZonedDateTime extractIssuedDateFromClaims(@NotNull Claims claims) {
		return ZonedDateTime.ofInstant(claims.getIssuedAt().toInstant(), ZoneId.systemDefault());
	}

	private ZonedDateTime extractExpirationDateFromClaims(@NotNull Claims claims) {
		return ZonedDateTime.ofInstant(claims.getExpiration().toInstant(), ZoneId.systemDefault());
	}

	private int extractRefreshCountFromClaims(@NotNull Claims claims) {
		return (int) claims.get(settings.getRefreshCountClaimName());
	}

	private int extractRefreshLimitFromClaims(@NotNull Claims claims) {
		return (int) claims.get(settings.getRefreshLimitClaimName());
	}
}
