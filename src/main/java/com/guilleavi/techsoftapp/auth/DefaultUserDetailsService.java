package com.guilleavi.techsoftapp.auth;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.guilleavi.techsoftapp.entities.UsuarioEntity;
import com.guilleavi.techsoftapp.enums.Authority;
import com.guilleavi.techsoftapp.services.UsuarioService;

@Service
public class DefaultUserDetailsService implements UserDetailsService {

	@Autowired
	private UsuarioService usuarioService = null;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		UsuarioEntity usuario = usuarioService.findByUsername(username);
		if (usuario == null) {
			throw new UsernameNotFoundException(
					String.format("No se encontró ningún usuario con el username '%s'.", username));
		}

		return new AuthenticatedUserDetails.Builder().withUsername(usuario.getUsername())
				.withPassword(usuario.getPassword())
				.withAuthorities(mapToGrantedAuthorities(usuarioService.findGrupos(usuario.getId())))
				.withActive(usuario.getActivo()).build();
	}

	// @SuppressWarnings("unused")
	private Set<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
		return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.toString()))
				.collect(Collectors.toSet());
	}
}