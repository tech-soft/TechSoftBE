package com.guilleavi.techsoftapp.auth.exceptions;

import org.springframework.security.core.AuthenticationException;

public class InvalidAuthenticationTokenException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public InvalidAuthenticationTokenException(String message, Throwable cause) {
		super(message, cause);
	}
}
