package com.guilleavi.techsoftapp.auth;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenIssuer {

	@Autowired
	private JwtSettings settings;

	public String issueToken(AuthenticationTokenDetails authenticationTokenDetails) {

		return Jwts.builder().setId(authenticationTokenDetails.getId()).setIssuer(settings.getIssuer())
				.setAudience(settings.getAudience()).setSubject(authenticationTokenDetails.getUsername())
				.setIssuedAt(Date.from(authenticationTokenDetails.getIssuedDate().toInstant()))
				.setExpiration(Date.from(authenticationTokenDetails.getExpirationDate().toInstant()))
				.claim(settings.getAuthoritiesClaimName(), authenticationTokenDetails.getAuthorities())
				.claim(settings.getRefreshCountClaimName(), authenticationTokenDetails.getRefreshCount())
				.claim(settings.getRefreshLimitClaimName(), authenticationTokenDetails.getRefreshLimit())
				.signWith(SignatureAlgorithm.HS256, settings.getSecret()).compact();
	}
}
