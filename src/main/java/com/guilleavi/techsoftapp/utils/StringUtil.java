package com.guilleavi.techsoftapp.utils;

import java.util.StringJoiner;

public class StringUtil {

	public static String concat(String[] strs) {

		StringJoiner joiner = new StringJoiner(" ");

		for (String str : strs) {
			joiner.add(str);
		}

		return joiner.toString();
	}
}
