package com.guilleavi.techsoftapp.datasources;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.UsuarioDto;
import com.guilleavi.techsoftapp.entities.UsuarioEntity;
import com.guilleavi.techsoftapp.services.UsuarioService;

@Component
@Path("usuarios")
public class UsuariosDataSource {

	@Context
	private UriInfo uriInfo = null;

	@Autowired
	private UsuarioService usuarioService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getUsuario(@PathParam("id") Integer id) {

		UsuarioEntity usuarioEntity = usuarioService.findUsuario(id);
		if (usuarioEntity == null) {
			throw new NotFoundException();
		}

		UsuarioDto usuario = toDto(usuarioEntity);
		return Response.ok(usuario).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getUsuarios() {

		Iterable<UsuarioEntity> iterable = usuarioService.findAllUsuarios();

		List<UsuarioDto> usuarios = StreamSupport.stream(iterable.spliterator(), false).map(this::toDto)
				.collect(Collectors.toList());

		return Response.ok(usuarios).build();
	}

	@GET
	@Path("me")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getUsuarioAutenticado() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication instanceof AnonymousAuthenticationToken) {

			UsuarioDto usuario = new UsuarioDto();
			usuario.setUsername(authentication.getName());
			usuario.setGrupos(new ArrayList<>());

			return Response.ok(usuario).build();
		}

		UsuarioEntity usuarioEntity = usuarioService.findByUsername(authentication.getName());
		UsuarioDto usuario = toDto(usuarioEntity);

		return Response.ok(usuario).build();
	}

	@GET
	@Path("username/{username}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getUsuarioByUsername(@PathParam("username") String username) {

		UsuarioEntity usuarioEntity = usuarioService.findByUsername(username);
		UsuarioDto usuario = toDto(usuarioEntity);

		return Response.ok(usuario).build();
	}

	private UsuarioDto toDto(UsuarioEntity usuarioEntity) {

		UsuarioDto usuario = new UsuarioDto();
		usuario.setId(usuarioEntity.getId());
		usuario.setPersona(usuarioEntity.getPersona());
		usuario.setUsername(usuarioEntity.getUsername());
		usuario.setGrupos(usuarioService.findGrupos(usuarioEntity.getId()));
		usuario.setActivo(usuarioEntity.getActivo());
		usuario.setLogo(usuarioEntity.getLogo());

		return usuario;
	}
}
