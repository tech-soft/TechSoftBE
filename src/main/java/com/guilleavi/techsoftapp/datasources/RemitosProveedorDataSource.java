package com.guilleavi.techsoftapp.datasources;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.RemitoProveedorDto;
import com.guilleavi.techsoftapp.dto.RemitoProveedorPreviewDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.RemitoProveedorService;

@Component
@Path("/proveedores/remitos")
public class RemitosProveedorDataSource {

	@Autowired
	private RemitoProveedorService remitoProveedorService = null;

	@GET
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getRemito(@PathParam("id") Integer id) throws ServiceException {

		RemitoProveedorDto remito = remitoProveedorService.getRemito(id);

		return Response.ok(remito).build();

	}

	@GET
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getRemitos(@QueryParam("proveedorId") Integer proveedorId, @QueryParam("fecha") Long fechaLong,
			@QueryParam("numero") Long numero) {

		LocalDate fecha = null;

		if (fechaLong != null) {
			fecha = Instant.ofEpochSecond(fechaLong).atZone(ZoneId.systemDefault()).toLocalDate();
		}

		List<RemitoProveedorPreviewDto> ordenes = remitoProveedorService.getRemitosPreview(proveedorId, fecha, numero);

		return Response.ok(ordenes).build();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addRemito(RemitoProveedorDto remito) throws ServiceException {

		remito = remitoProveedorService.insert(remito);

		return Response.ok(remito).build();

	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response updateRemito(RemitoProveedorDto remito) throws ServiceException {

		remito = remitoProveedorService.update(remito.getId(), remito.getObservaciones());

		return Response.ok(remito).build();

	}
}
