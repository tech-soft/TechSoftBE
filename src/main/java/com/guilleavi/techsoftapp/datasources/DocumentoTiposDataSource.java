package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.DocumentoTipoDto;
import com.guilleavi.techsoftapp.services.DocumentoTipoService;

@Component
@Path("/documento-tipos")
public class DocumentoTiposDataSource {

	@Autowired
	private DocumentoTipoService documentoTipoService = null;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getDocumentoTipos() {

		List<DocumentoTipoDto> documentoTipos = documentoTipoService.getDocumentoTipos();

		return Response.ok(documentoTipos).build();

	}
}
