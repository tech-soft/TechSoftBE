package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.IvaTipoDto;
import com.guilleavi.techsoftapp.services.IvaTipoService;

@Component
@Path("/iva-tipos")
public class IvaTiposDataSource {

	@Autowired
	private IvaTipoService ivaTipoService = null;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getIvaTipos() {

		List<IvaTipoDto> ivaTipos = ivaTipoService.getIvaTipos();

		return Response.ok(ivaTipos).build();

	}
}