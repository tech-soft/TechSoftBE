package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import com.guilleavi.techsoftapp.dto.PuestoDto;
import com.guilleavi.techsoftapp.services.PuestoService;

@Controller
@Path("/puestos")
public class PuestosDataSource {

	@Autowired
	private PuestoService puestoService = null;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPuestos() {

		List<PuestoDto> puestos = puestoService.getPuestos();

		return Response.ok(puestos).build();

	}

	@GET
	@Path("/activos")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPuestosActivos() {

		List<PuestoDto> puestos = puestoService.getPuestosActivos();

		return Response.ok(puestos).build();

	}
}
