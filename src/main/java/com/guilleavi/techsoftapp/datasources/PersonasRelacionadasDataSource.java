package com.guilleavi.techsoftapp.datasources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.PersonaRelacionadaDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.PersonaRelacionadaService;

@Component
@Path("/personas/relacionadas")
public class PersonasRelacionadasDataSource {

	@Autowired
	private PersonaRelacionadaService personaRelacionadaService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPersonaRelacionada(@PathParam("id") Integer id) throws ServiceException {

		PersonaRelacionadaDto relacion = personaRelacionadaService.getPersonaRelacionada(id);

		return Response.ok(relacion).build();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response upsertRelacion(PersonaRelacionadaDto relacion) throws ServiceException {

		relacion = personaRelacionadaService.upsert(relacion);

		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deleteRelacion(@PathParam("id") Integer id) throws ServiceException {

		personaRelacionadaService.delete(id);

		return Response.noContent().build();

	}
}
