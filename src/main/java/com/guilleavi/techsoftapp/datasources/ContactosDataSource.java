package com.guilleavi.techsoftapp.datasources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.ContactoDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.ContactoService;

@Component
@Path("/personas/contactos")
public class ContactosDataSource {

	@Autowired
	private ContactoService contactoService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getContacto(@PathParam("id") Integer id) throws ServiceException {

		ContactoDto contacto = contactoService.getContacto(id);

		return Response.ok(contacto).build();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response upsertContacto(ContactoDto contacto) throws ServiceException {

		contactoService.upsert(contacto);

		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deleteContacto(@PathParam("id") Integer id) throws ServiceException {

		contactoService.delete(id);

		return Response.noContent().build();

	}
}
