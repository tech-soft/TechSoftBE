package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.PagoFormaDto;
import com.guilleavi.techsoftapp.services.PagoFormaService;

@Component
@Path("/pago-formas")
public class PagoFormasDataSource {

	@Autowired
	private PagoFormaService pagoFormaService = null;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPagoFormas() {

		List<PagoFormaDto> pagoFormas = pagoFormaService.getPagoFormas();

		return Response.ok(pagoFormas).build();

	}
}