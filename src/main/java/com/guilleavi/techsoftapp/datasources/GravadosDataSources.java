package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.GravadoDto;
import com.guilleavi.techsoftapp.services.GravadoService;

@Component
@Path("/gravados")
public class GravadosDataSources {

	@Autowired
	private GravadoService gravadoService = null;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getGravados() {

		List<GravadoDto> gravados = gravadoService.getGravados();

		return Response.ok(gravados).build();

	}
}
