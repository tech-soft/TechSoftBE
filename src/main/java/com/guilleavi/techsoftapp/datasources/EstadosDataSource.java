package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.EstadoDto;
import com.guilleavi.techsoftapp.services.EstadoService;

@Component
@Path("/estados")
public class EstadosDataSource {

	@Autowired
	private EstadoService estadoService = null;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getEstados() {

		List<EstadoDto> estados = estadoService.getEstados();

		return Response.ok(estados).build();

	}

	@GET
	@Path("/ordenes-compra")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getEstadosOC() {

		List<EstadoDto> estados = estadoService.getOrdenesEstados();

		return Response.ok(estados).build();

	}
}