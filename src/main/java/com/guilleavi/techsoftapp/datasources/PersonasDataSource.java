package com.guilleavi.techsoftapp.datasources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.ContactoDto;
import com.guilleavi.techsoftapp.dto.DomicilioDto;
import com.guilleavi.techsoftapp.dto.PersonaComercialDto;
import com.guilleavi.techsoftapp.dto.PersonaDto;
import com.guilleavi.techsoftapp.dto.PersonaRelacionadaDto;
import com.guilleavi.techsoftapp.dto.PersonaResumenDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.ContactoService;
import com.guilleavi.techsoftapp.services.DomicilioService;
import com.guilleavi.techsoftapp.services.PersonaComercialService;
import com.guilleavi.techsoftapp.services.PersonaRelacionadaService;
import com.guilleavi.techsoftapp.services.PersonaService;

@Component
@Path("/personas")
public class PersonasDataSource {

	@Autowired
	private PersonaService personaService = null;
	@Autowired
	private DomicilioService domicilioService = null;
	@Autowired
	private ContactoService contactoService = null;
	@Autowired
	private PersonaComercialService personaComercialService = null;
	@Autowired
	private PersonaRelacionadaService personaRelacionadaService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPersona(@PathParam("id") Integer id) throws ServiceException {

		PersonaDto persona = personaService.getPersona(id);

		return Response.ok(persona).build();

	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPersonas() {

		List<PersonaResumenDto> personas = personaService.getPersonas();

		return Response.ok(personas).build();

	}

	@GET
	@Path("/keyword")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPersonasByKeyword(@QueryParam("q") String q) {

		List<PersonaResumenDto> personas = personaService.getPersonasByKeyword(q);

		return Response.ok(personas).build();

	}

	@GET
	@Path("/{personaId}/datos-comerciales")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPersonaComercial(@PathParam("personaId") Integer id) throws ServiceException {

		PersonaComercialDto personaComercial = personaComercialService.getDatosComercialesByPersona(id);

		return Response.ok(personaComercial).build();

	}

	@GET
	@Path("/{personaId}/relacionadas")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPersonasRelacionadas(@PathParam("personaId") Integer personaId)
			throws ServiceException {

		List<PersonaRelacionadaDto> relaciones;

		try {
			relaciones = personaRelacionadaService.getPersonasRelacionadasByPersona(personaId);

			return Response.ok(relaciones).build();
		} catch (ServiceException e) {

			relaciones = new ArrayList<PersonaRelacionadaDto>();

			return Response.ok(relaciones).build();
		}

	}

	@GET
	@Path("/{personaId}/contactos/{tipoId}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getContactos(@PathParam("personaId") Integer personaId,
			@PathParam("tipoId") Integer tipoId) throws ServiceException {

		List<ContactoDto> contactos = contactoService.getContactosByPersona(personaId, tipoId);

		return Response.ok(contactos).build();

	}

	@GET
	@Path("/{personaId}/domicilios")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getDomicilio(@PathParam("personaId") Integer id) throws ServiceException {

		List<DomicilioDto> domicilios = domicilioService.getDomiciliosByPersona(id);

		return Response.ok(domicilios).build();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addPersona(PersonaDto persona) throws ServiceException {

		persona = personaService.insert(persona);

		return Response.ok(persona).build();

	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response editPersona(PersonaDto persona) throws ServiceException {

		persona = personaService.update(persona);

		return Response.ok(persona).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deletePersona(@PathParam("id") Integer id) throws ServiceException {

		personaService.delete(id);

		return Response.noContent().build();

	}

}
