package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.CiudadDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.CiudadService;

@Component
@Path("/ciudades")
public class CiudadesDataSource {

	@Autowired
	private CiudadService ciudadService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getCiudad(@PathParam("id") Integer id) throws ServiceException {

		CiudadDto ciudad = ciudadService.getCiudad(id);

		return Response.ok(ciudad).build();

	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getCiudades() {

		List<CiudadDto> ciudades = ciudadService.getCiudades();

		return Response.ok(ciudades).build();

	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addCiudad(CiudadDto ciudad) throws ServiceException {

		ciudad = ciudadService.insert(ciudad);

		return Response.ok(ciudad).build();

	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response updateCiudad(CiudadDto ciudad) throws ServiceException {

		ciudad = ciudadService.update(ciudad);

		return Response.ok(ciudad).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deleteCiudad(@PathParam("id") Integer id) throws ServiceException {

		ciudadService.delete(id);

		return Response.noContent().build();

	}
}
