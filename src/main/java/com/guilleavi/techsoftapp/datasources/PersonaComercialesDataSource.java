package com.guilleavi.techsoftapp.datasources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.PersonaComercialDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.PersonaComercialService;

@Component
@Path("/personas/datos-comerciales")
public class PersonaComercialesDataSource {

	@Autowired
	private PersonaComercialService personaComercialService = null;

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response upsertPersonaComercial(PersonaComercialDto personaComercial) throws ServiceException {

		personaComercial = personaComercialService.upsert(personaComercial);

		return Response.ok(personaComercial).build();

	}

	@DELETE
	@Path("/{personaId}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deletePersonaComercialByPersona(@PathParam("personaId") Integer personaId) throws ServiceException {

		personaComercialService.deleteByPersonaId(personaId);

		return Response.noContent().build();

	}
}
