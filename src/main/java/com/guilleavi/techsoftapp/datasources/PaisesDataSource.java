package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.PaisDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.PaisService;

@Component
@Path("/paises")
public class PaisesDataSource {

	@Autowired
	private PaisService paisService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPais(@PathParam("id") Integer id) throws ServiceException {

		PaisDto pais = paisService.getPais(id);

		return Response.ok(pais).build();

	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPaises() {

		List<PaisDto> paises = paisService.getPaises();

		return Response.ok(paises).build();

	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addPais(PaisDto pais) throws ServiceException {

		pais = paisService.insert(pais);

		return Response.ok(pais).build();
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response updatePais(PaisDto pais) throws ServiceException {

		pais = paisService.update(pais);

		return Response.ok(pais).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deletePais(@PathParam("id") Integer id) throws ServiceException {

		paisService.delete(id);
		
		return Response.noContent().build();

	}
}