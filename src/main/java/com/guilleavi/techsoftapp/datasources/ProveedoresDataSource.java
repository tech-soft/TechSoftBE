package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import com.guilleavi.techsoftapp.dto.ProveedorDto;
import com.guilleavi.techsoftapp.services.ProveedorService;

@Controller
@Path("/proveedores")
public class ProveedoresDataSource {

	@Autowired
	private ProveedorService proveedorService = null;

	@GET
	@Path("/activos")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getProveedoresActivos() {

		List<ProveedorDto> proveedores = proveedorService.getProveedoresActivos();

		return Response.ok(proveedores).build();

	}
}
