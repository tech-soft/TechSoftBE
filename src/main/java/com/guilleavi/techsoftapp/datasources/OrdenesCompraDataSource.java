package com.guilleavi.techsoftapp.datasources;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.OrdenCompraDto;
import com.guilleavi.techsoftapp.dto.OrdenCompraPreviewDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.OrdenCompraService;

@Component
@Path("/ordenes-compra")
public class OrdenesCompraDataSource {

	@Autowired
	private OrdenCompraService ordenCompraService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getOrdenCompra(@PathParam("id") Integer id) throws ServiceException {

		OrdenCompraDto orden = ordenCompraService.getOrden(id);

		return Response.ok(orden).build();

	}

	@GET
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getOrdenesCompra(@QueryParam("proveedorId") Integer proveedorId,
			@QueryParam("fechaCreacion") Long fechaCreacionLong, @QueryParam("puestoId") Integer puestoId,
			@QueryParam("numero") Long numero, @QueryParam("estadoId") Integer estadoId) {

		LocalDate fechaCreacion = null;

		if (fechaCreacionLong != null) {
			fechaCreacion = Instant.ofEpochSecond(fechaCreacionLong).atZone(ZoneId.systemDefault()).toLocalDate();
		}

		List<OrdenCompraPreviewDto> ordenes = ordenCompraService.getOrdenesPreview(proveedorId, fechaCreacion, puestoId,
				numero, estadoId);

		return Response.ok(ordenes).build();

	}

	@GET
	@Path("/emitidas")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getOrdenesEmitidas(@QueryParam("proveedorId") Integer proveedorId) {

		List<OrdenCompraPreviewDto> ordenes = ordenCompraService.getOrdenesEmitidas(proveedorId);

		return Response.ok(ordenes).build();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addOrdenCompra(OrdenCompraDto orden) throws ServiceException {

		orden = ordenCompraService.insert(orden);

		return Response.ok(orden).build();

	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response updateOrdenCompra(OrdenCompraDto orden) throws ServiceException {

		orden = ordenCompraService.update(orden);

		return Response.ok(orden).build();

	}

	@PUT
	@Path("/emitir")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response emitirOrdenCompra(OrdenCompraDto orden) throws ServiceException {

		orden = ordenCompraService.emitirOrdenCompra(orden);

		return Response.ok(orden).build();

	}

	@PUT
	@Path("/cancelar")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response cancelarOrdenCompra(OrdenCompraDto orden) throws ServiceException {

		orden = ordenCompraService.cancelarEmisionOrdenCompra(orden);

		return Response.ok(orden).build();

	}

	@PUT
	@Path("/recibir/{ordenId}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response recibirOrdenCompra(Integer ordenId) throws ServiceException {

		ordenCompraService.recibirOrdenCompra(ordenId);

		return Response.ok().build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response deleteOrdenCompra(@PathParam("id") Integer id) throws ServiceException {

		ordenCompraService.delete(id);

		return Response.noContent().build();

	}

}
