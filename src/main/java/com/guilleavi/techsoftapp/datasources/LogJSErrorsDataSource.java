package com.guilleavi.techsoftapp.datasources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

@Component
@Path("/api/log-js")
public class LogJSErrorsDataSource {

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response logJS(String message) {

		System.out.print(message);

		return Response.ok().build();

	}
}
