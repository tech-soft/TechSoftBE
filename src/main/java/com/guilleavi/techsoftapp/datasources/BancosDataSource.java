package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.BancoDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.BancoService;

@Component
@Path("/bancos")
public class BancosDataSource {

	@Autowired
	private BancoService bancoService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getBanco(@PathParam("id") Integer id) throws ServiceException {

		BancoDto banco = bancoService.getBanco(id);

		return Response.ok(banco).build();

	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getBancos() {

		List<BancoDto> bancos = bancoService.getBancos();

		return Response.ok(bancos).build();

	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addBanco(BancoDto banco) throws ServiceException {

		banco = bancoService.insert(banco);

		return Response.ok(banco).build();

	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response updateBanco(BancoDto banco) throws ServiceException {

		banco = bancoService.update(banco);

		return Response.ok(banco).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deleteBanco(@PathParam("id") Integer id) throws ServiceException {

		bancoService.delete(id);

		return Response.noContent().build();

	}
}
