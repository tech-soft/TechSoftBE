package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.ComprobanteTipoDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.ComprobanteTipoService;

@Component
@Path("/comprobante-tipos")
public class ComprobanteTiposDataSource {

	@Autowired
	private ComprobanteTipoService comprobanteTipoService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getComprobanteTipo(@PathParam("id") Integer id) throws ServiceException {

		ComprobanteTipoDto comprobanteTipo = comprobanteTipoService.getComprobanteTipo(id);

		return Response.ok(comprobanteTipo).build();

	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getComprobanteTipos() {

		List<ComprobanteTipoDto> comprobanteTipos = comprobanteTipoService.getComprobanteTipos();

		return Response.ok(comprobanteTipos).build();

	}

	@GET
	@Path("/remitos")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getRemitoTipos() {

		List<ComprobanteTipoDto> comprobanteTipos = comprobanteTipoService.getRemitoTipos();

		return Response.ok(comprobanteTipos).build();

	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addComprobanteTipo(ComprobanteTipoDto comprobanteTipo) throws ServiceException {

		comprobanteTipo = comprobanteTipoService.insert(comprobanteTipo);

		return Response.ok(comprobanteTipo).build();

	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response updateComprobanteTipo(ComprobanteTipoDto comprobanteTipo) throws ServiceException {

		comprobanteTipo = comprobanteTipoService.update(comprobanteTipo);

		return Response.ok(comprobanteTipo).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deleteComprobanteTipo(@PathParam("id") Integer id) throws ServiceException {

		comprobanteTipoService.delete(id);

		return Response.noContent().build();

	}

}
