package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.PersonaRelacionTipoDto;
import com.guilleavi.techsoftapp.services.PersonaRelacionTipoService;

@Component
@Path("/persona-relaciones-tipos")
public class PersonaRelacionTiposDataSource {

	@Autowired
	private PersonaRelacionTipoService personaRelacionTipoService = null;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPersonaRelacionTipos() {

		List<PersonaRelacionTipoDto> personaRelacionTipos = personaRelacionTipoService.getPersonaRelacionTipos();

		return Response.ok(personaRelacionTipos).build();

	}
}
