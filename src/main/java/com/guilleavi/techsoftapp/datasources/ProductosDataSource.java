package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import com.guilleavi.techsoftapp.dto.ProductoDto;
import com.guilleavi.techsoftapp.dto.ProductoPreciosPreviewDto;
import com.guilleavi.techsoftapp.dto.ProductoPreviewDto;
import com.guilleavi.techsoftapp.dto.ProductoStockDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.ProductoService;

@RestController
@Path("/productos")
public class ProductosDataSource {

	@Autowired
	private ProductoService productoService = null;

	@GET
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getProducto(@PathParam("id") Integer id) throws ServiceException {

		ProductoDto producto = productoService.getProducto(id);

		return Response.ok(producto).build();

	}

	@GET
	@Path("/previews")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getProductosPreviews() {

		List<ProductoPreviewDto> productos = productoService.getProductosPreviewActivos();

		return Response.ok(productos).build();

	}

	@GET
	@Path("/previews/listas")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getProductosByKeyword(@QueryParam("conStockDisponible") Boolean conStockDisponible,
			@QueryParam("conStockEntrante") Boolean conStockEntrante, @QueryParam("q") String q) {

		List<ProductoPreciosPreviewDto> productos = productoService.getProductosByKeyword(conStockDisponible, conStockEntrante, q);

		return Response.ok(productos).build();

	}

	@GET
	@Path("/{id}/stock")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getProductoStock(@PathParam("id") Integer id) throws ServiceException {

		ProductoStockDto stock = productoService.getProductoStock(id);

		return Response.ok(stock).build();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addProducto(ProductoDto producto) throws ServiceException {

		producto = productoService.insert(producto);

		return Response.ok(producto).build();

	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response updateProducto(ProductoDto producto) throws ServiceException {

		producto = productoService.update(producto);

		return Response.ok(producto).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response deleteProducto(@PathParam("id") Integer id) throws ServiceException {

		productoService.delete(id);

		return Response.noContent().build();

	}
}
