package com.guilleavi.techsoftapp.datasources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.DomicilioDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.DomicilioService;

@Component
@Path("/personas/domicilios")
public class DomiciliosDataSource {

	@Autowired
	private DomicilioService domicilioService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getDomicilio(@PathParam("id") Integer id) throws ServiceException {

		DomicilioDto domicilio = domicilioService.getDomicilio(id);

		return Response.ok(domicilio).build();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response upsertDomicilio(DomicilioDto domicilio) throws ServiceException {

		domicilio = domicilioService.upsert(domicilio);

		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deleteDomicilio(@PathParam("id") Integer id) throws ServiceException {

		domicilioService.delete(id);

		return Response.noContent().build();

	}
}
