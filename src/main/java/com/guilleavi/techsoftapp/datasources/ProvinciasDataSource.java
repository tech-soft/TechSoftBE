package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.guilleavi.techsoftapp.dto.CiudadDto;
import com.guilleavi.techsoftapp.dto.ProvinciaDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.CiudadService;
import com.guilleavi.techsoftapp.services.ProvinciaService;

@Component
@Path("/provincias")
public class ProvinciasDataSource {

	@Autowired
	private ProvinciaService provinciaService = null;
	@Autowired
	private CiudadService ciudadService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getProvincia(@PathParam("id") Integer id) throws ServiceException {

		ProvinciaDto provincia = provinciaService.getProvincia(id);

		return Response.ok(provincia).build();

	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getProvincias() {

		List<ProvinciaDto> provincias = provinciaService.getProvincias();

		return Response.ok(provincias).build();

	}

	@GET
	@Path("/{id}/ciudades")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getCiudades(@PathParam("id") Integer id) throws ServiceException {

		List<CiudadDto> ciudades = ciudadService.getCiudadesByProvincia(id);

		return Response.ok(ciudades).build();

	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addProvincia(ProvinciaDto provincia) throws ServiceException {

		provincia = provinciaService.insert(provincia);

		return Response.ok(provincia).build();
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response updateProvincia(ProvinciaDto provincia) throws ServiceException {

		provincia = provinciaService.update(provincia);
		
		return Response.ok(provincia).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deleteProvincia(@PathParam("id") Integer id) throws ServiceException {

		provinciaService.delete(id);
		
		return Response.noContent().build();

	}
}
