package com.guilleavi.techsoftapp.datasources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import com.guilleavi.techsoftapp.dto.PuestoDto;
import com.guilleavi.techsoftapp.dto.SucursalDto;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.services.PuestoService;
import com.guilleavi.techsoftapp.services.SucursalService;

@Controller
@Path("/sucursales")
public class SucursalesDataSource {

	@Autowired
	private SucursalService sucursalService = null;
	@Autowired
	private PuestoService puestoService = null;

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getSucursal(@PathParam("id") Integer id) throws ServiceException {

		SucursalDto sucursal = sucursalService.getSucursal(id);

		return Response.ok(sucursal).build();

	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getSucursales() {

		List<SucursalDto> sucursales = sucursalService.getSucursales();

		return Response.ok(sucursales).build();

	}

	@GET
	@Path("{id}/puestos/activos")
	@Produces({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response getPuestosActivos(@PathParam("id") Integer id) {

		PuestoDto puesto = puestoService.getPuestoBySucursal(id);

		return Response.ok(puesto).build();

	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response addSucursal(SucursalDto sucursal) throws ServiceException {

		sucursal = sucursalService.insert(sucursal);

		return Response.ok(sucursal).build();
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response updateSucursal(SucursalDto sucursal) throws ServiceException {

		sucursal = sucursalService.update(sucursal);

		return Response.ok(sucursal).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@PreAuthorize("hasAuthority('ADMIN')")
	public Response deleteSucursal(@PathParam("id") Integer id) throws ServiceException {

		sucursalService.delete(id);

		return Response.noContent().build();

	}
}
