package com.guilleavi.techsoftapp.datasources;

import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

import com.guilleavi.techsoftapp.auth.AuthenticationTokenDetails;
import com.guilleavi.techsoftapp.dto.AuthenticationTokenDto;
import com.guilleavi.techsoftapp.dto.CredencialDto;
import com.guilleavi.techsoftapp.enums.Authority;
import com.guilleavi.techsoftapp.services.AuthenticationTokenService;

@RestController
@Path("auth")
public class AuthenticationsDataSource {

	@Autowired
	private AuthenticationManager authenticationManager = null;

	@Autowired
	private AuthenticationTokenService authenticationTokenService = null;

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response autenticar(CredencialDto credenciales) {

		Authentication solicitud = new UsernamePasswordAuthenticationToken(credenciales.getUsername(),
				credenciales.getPassword());
		Authentication respuesta = authenticationManager.authenticate(solicitud);
		SecurityContextHolder.getContext().setAuthentication(respuesta);

		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		Set<Authority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
				.map(grantedAuthority -> Authority.valueOf(grantedAuthority.toString())).collect(Collectors.toSet());

		String token = authenticationTokenService.issueToken(username, authorities);

		AuthenticationTokenDto authenticationTokenDto = new AuthenticationTokenDto();
		authenticationTokenDto.setToken(token);

		return Response.ok(authenticationTokenDto).build();
	}

	@POST
	@Path("refresh")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response refresh() {

		AuthenticationTokenDetails tokenDetails = (AuthenticationTokenDetails) SecurityContextHolder.getContext()
				.getAuthentication().getDetails();

		String token = authenticationTokenService.refreshToken(tokenDetails);
		AuthenticationTokenDto authenticationTokenDto = new AuthenticationTokenDto();
		authenticationTokenDto.setToken(token);

		return Response.ok(authenticationTokenDto).build();
	}
}
