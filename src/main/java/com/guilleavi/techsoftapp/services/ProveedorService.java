package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.ProveedorDto;
import com.guilleavi.techsoftapp.entities.QPersonaEntity;
import com.guilleavi.techsoftapp.enums.EstadoEnum;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class ProveedorService {

	@PersistenceContext
	protected EntityManager em = null;

	@Transactional(readOnly = true)
	public List<ProveedorDto> getProveedoresActivos() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QPersonaEntity qpersona = QPersonaEntity.personaEntity;

		return queryFactory.selectFrom(qpersona)
				.where(qpersona.proveedor.eq(true).and(qpersona.estado.id.eq(EstadoEnum.ACTIVO.getValor()))).fetch()
				.stream().map(persona -> new ProveedorDto(persona)).collect(Collectors.toList());
	}
}
