package com.guilleavi.techsoftapp.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.CharMatcher;
import com.guilleavi.techsoftapp.dto.ProductoCostoDto;
import com.guilleavi.techsoftapp.dto.ProductoDto;
import com.guilleavi.techsoftapp.dto.ProductoPrecioDto;
import com.guilleavi.techsoftapp.dto.ProductoPreciosPreviewDto;
import com.guilleavi.techsoftapp.dto.ProductoPreviewDto;
import com.guilleavi.techsoftapp.dto.ProductoRentaDto;
import com.guilleavi.techsoftapp.dto.ProductoStockDto;
import com.guilleavi.techsoftapp.entities.ProductoCostoEntity;
import com.guilleavi.techsoftapp.entities.ProductoEntity;
import com.guilleavi.techsoftapp.entities.ProductoPrecioEntity;
import com.guilleavi.techsoftapp.entities.ProductoStockEntity;
import com.guilleavi.techsoftapp.entities.QProductoCostoEntity;
import com.guilleavi.techsoftapp.entities.QProductoEntity;
import com.guilleavi.techsoftapp.entities.QProductoPrecioEntity;
import com.guilleavi.techsoftapp.entities.QProductoStockEntity;
import com.guilleavi.techsoftapp.enums.EstadoEnum;
import com.guilleavi.techsoftapp.enums.ListaPrecioEnum;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.EstadoRepository;
import com.guilleavi.techsoftapp.repositories.GravadoRepository;
import com.guilleavi.techsoftapp.repositories.OrigenRepository;
import com.guilleavi.techsoftapp.repositories.ProductoRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class ProductoService {

	@PersistenceContext
	protected EntityManager em = null;
	@Autowired
	private ProductoRepository productoRepository = null;
	@Autowired
	private EstadoRepository estadoRepository = null;
	@Autowired
	private GravadoRepository gravadoRepository = null;
	@Autowired
	private OrigenRepository origenRepository = null;

	@Transactional(readOnly = true)
	public List<ProductoPreviewDto> getProductosPreviewActivos() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoEntity qproducto = QProductoEntity.productoEntity;

		return queryFactory.selectFrom(qproducto).where(qproducto.estado.id.eq(EstadoEnum.ACTIVO.getValor())).fetch()
				.stream().map(producto -> new ProductoPreviewDto(producto)).collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	public List<ProductoPreciosPreviewDto> getProductosByKeyword(Boolean conStockDisponible, Boolean conStockEntrante,
			String q) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoEntity qproducto = QProductoEntity.productoEntity;

		List<ProductoEntity> productos = queryFactory.selectFrom(qproducto)
				.where(qproducto.descripcion.containsIgnoreCase(CharMatcher.is('\'').trimFrom(q))
						.or(qproducto.descripcionReducida.containsIgnoreCase(CharMatcher.is('\'').trimFrom(q))))
				.fetch();

		if (conStockDisponible) {
			productos = productos.stream().filter(producto -> conStockDisponible(producto))
					.collect(Collectors.toList());
		}

		if (conStockEntrante) {
			productos = productos.stream().filter(producto -> conStockEntrante(producto)).collect(Collectors.toList());
		}

		List<ProductoPreciosPreviewDto> productosPreciosPreview = productos.stream()
				.map(producto -> setDatosAdicionales(producto)).collect(Collectors.toList());

		return productosPreciosPreview;
	}

	@Transactional(readOnly = true)
	public ProductoStockDto getProductoStock(Integer productoId) {

		ProductoStockEntity productoStockEntity = getStock(productoId);

		return (productoStockEntity != null) ? new ProductoStockDto(productoStockEntity) : null;
	}

	private Boolean conStockDisponible(ProductoEntity producto) {

		ProductoStockEntity stock = getStock(producto.getId());

		return (stock != null && stock.getStockDisponible() > 0) ? true : false;
	}

	private Boolean conStockEntrante(ProductoEntity producto) {

		ProductoStockEntity stock = getStock(producto.getId());

		return (stock != null && stock.getStockEntrante() > 0) ? true : false;

	}

	private ProductoStockEntity getStock(Integer productoId) {
		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;

		return queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId)).fetchOne();

	}

	private ProductoPreciosPreviewDto setDatosAdicionales(ProductoEntity producto) {

		ProductoPreciosPreviewDto productoPreciosPreviewDto = setPreciosActuales(producto);

		ProductoStockEntity productoStockEntity = getStock(producto.getId());

		productoPreciosPreviewDto
				.setStockEntrante((productoStockEntity != null && productoStockEntity.getStockEntrante() != null)
						? productoStockEntity.getStockEntrante()
						: 0);
		productoPreciosPreviewDto
				.setStockDisponible((productoStockEntity != null && productoStockEntity.getStockDisponible() != null)
						? productoStockEntity.getStockDisponible()
						: 0);
		productoPreciosPreviewDto
				.setStockTransito((productoStockEntity != null && productoStockEntity.getStockTransito() != null)
						? productoStockEntity.getStockTransito()
						: 0);

		return productoPreciosPreviewDto;
	}

	private ProductoPreciosPreviewDto setPreciosActuales(ProductoEntity producto) {

		Double rentaLista1 = getRentaLista(producto, ListaPrecioEnum.LISTA_1.getValor());
		Double rentaLista2 = getRentaLista(producto, ListaPrecioEnum.LISTA_2.getValor());
		Double rentaLista3 = getRentaLista(producto, ListaPrecioEnum.LISTA_3.getValor());
		Double rentaLista4 = getRentaLista(producto, ListaPrecioEnum.LISTA_4.getValor());
		Double rentaLista5 = getRentaLista(producto, ListaPrecioEnum.LISTA_5.getValor());
		Double rentaLista6 = getRentaLista(producto, ListaPrecioEnum.LISTA_6.getValor());

		Double costoActual = getCostoActual(producto);

		Double porcentajeIva = (1 + producto.getGravado().getPorcentaje() / 100);

		Double precioLita1SinIva = costoActual * (1 + rentaLista1 / 100);
		Double precioLita2SinIva = costoActual * (1 + rentaLista2 / 100);
		Double precioLita3SinIva = costoActual * (1 + rentaLista3 / 100);
		Double precioLita4SinIva = costoActual * (1 + rentaLista4 / 100);
		Double precioLita5SinIva = costoActual * (1 + rentaLista5 / 100);
		Double precioLita6SinIva = costoActual * (1 + rentaLista6 / 100);

		ProductoPreciosPreviewDto productoPreciosPreviewDto = new ProductoPreciosPreviewDto(producto);

		productoPreciosPreviewDto.setId(producto.getId());
		productoPreciosPreviewDto.setDescripcion(producto.getDescripcion());

		productoPreciosPreviewDto.setLista1SinIva(precioLita1SinIva);
		productoPreciosPreviewDto.setLista2SinIva(precioLita2SinIva);
		productoPreciosPreviewDto.setLista3SinIva(precioLita3SinIva);
		productoPreciosPreviewDto.setLista4SinIva(precioLita4SinIva);
		productoPreciosPreviewDto.setLista5SinIva(precioLita5SinIva);
		productoPreciosPreviewDto.setLista6SinIva(precioLita6SinIva);

		productoPreciosPreviewDto.setLista1ConIva(precioLita1SinIva * porcentajeIva);
		productoPreciosPreviewDto.setLista2ConIva(precioLita2SinIva * porcentajeIva);
		productoPreciosPreviewDto.setLista3ConIva(precioLita3SinIva * porcentajeIva);
		productoPreciosPreviewDto.setLista4ConIva(precioLita4SinIva * porcentajeIva);
		productoPreciosPreviewDto.setLista5ConIva(precioLita5SinIva * porcentajeIva);
		productoPreciosPreviewDto.setLista6ConIva(precioLita6SinIva * porcentajeIva);

		return productoPreciosPreviewDto;
	}

	private Double getRentaLista(ProductoEntity producto, Integer lista) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoPrecioEntity qproductoprecio = QProductoPrecioEntity.productoPrecioEntity;

		Double renta = queryFactory.select(qproductoprecio.renta).from(qproductoprecio)
				.where(qproductoprecio.producto.id.eq(producto.getId()).and(qproductoprecio.listaPrecio.id.eq(lista))
						.and(qproductoprecio.fecha.loe(LocalDate.now())))
				.orderBy(qproductoprecio.fecha.desc()).fetchOne();

		return (renta != null) ? renta : 0;
	}

	private Double getCostoActual(ProductoEntity producto) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoCostoEntity qproductoCosto = QProductoCostoEntity.productoCostoEntity;

		Double costo = queryFactory.select(qproductoCosto.costo).from(qproductoCosto)
				.where(qproductoCosto.producto.id.eq(producto.getId()).and(qproductoCosto.fecha.loe(LocalDate.now())))
				.orderBy(qproductoCosto.fecha.desc()).fetchOne();

		return (costo != null) ? costo : 0;
	}

	//
	@Transactional(readOnly = true)
	public ProductoDto getProducto(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de producto no puede ser nulo.");
		}

		ProductoEntity productoEntity = productoRepository.findOne(id);

		if (productoEntity != null) {
			ProductoDto productoDto = new ProductoDto(productoEntity);

			Double iva = (productoDto.getGravado() != null) ? productoDto.getGravado().getPorcenaje() : 0;

			List<ProductoPrecioDto> preciosActuales = getPreciosActuales(id, iva);

			if (preciosActuales != null) {
				productoDto.setPrecios(preciosActuales);
			}

			return productoDto;

		} else {
			return null;
		}

	}

	@Transactional
	public ProductoDto insert(ProductoDto producto) throws ServiceException {

		if (producto == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El producto a insertar no se encuentra definido."));
		}

		if (producto.getId() != null && productoRepository.findOne(producto.getId()) != null) {
			throw new ServiceException(ServiceException.DATA_INTEGRITY_VIOLATION,
					String.format("El 'id' del producto a insertar ya existe en la base de datos."));
		}

		validarProducto(producto);

		return save(producto);

	}

	@Transactional
	public ProductoDto update(ProductoDto producto) throws ServiceException {

		if (producto == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El producto a insertar no se encuentra definido."));
		}

		if (producto.getId() == null || productoRepository.findOne(producto.getId()) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' del producto que desea modificar no existe en la base de datos."));
		}

		validarProducto(producto);

		return save(producto);
	}

	@Transactional
	public void delete(Integer productoId) throws ServiceException {

		if (productoId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' del producto no puede ser nulo.");
		}

		if (productoRepository.findOne(productoId) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					"El producto que se desea eliminar no existe.");
		}

		// TODO: no eliminar productos con algun tipo de stock

		productoRepository.delete(productoId);
	}

	private List<ProductoPrecioDto> getPreciosActuales(Integer productoId, Double iva) {

		ProductoCostoDto costoDto = getCostoActual(productoId);
		Double costo = (costoDto != null) ? costoDto.getCosto() : 0L;

		return getRentasActuales(productoId).stream().map(renta -> toPrecioDto(renta, costo, iva))
				.collect(Collectors.toList());
	}

	private ProductoPrecioDto toPrecioDto(ProductoRentaDto productoRentaDto, Double costo, Double iva) {
		Double renta = productoRentaDto.getRenta();
		Double precioFinal = costo * (1 + renta / 100) * (1 + iva / 100);

		ProductoPrecioDto productoPrecioDto = new ProductoPrecioDto();
		productoPrecioDto.setListaId(productoRentaDto.getListaId());
		productoPrecioDto.setRenta(renta);
		productoPrecioDto.setIva(iva);
		productoPrecioDto.setCosto(costo);
		productoPrecioDto.setPrecioFinal(precioFinal);

		return productoPrecioDto;

	}

	private ProductoCostoDto getCostoActual(Integer productoId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoCostoEntity qproductoCosto = QProductoCostoEntity.productoCostoEntity;

		ProductoCostoEntity costoEntity = queryFactory.selectFrom(qproductoCosto)
				.where(qproductoCosto.id.eq(productoId).and(qproductoCosto.fecha.loe(LocalDate.now())))
				.orderBy(qproductoCosto.fecha.desc()).fetchFirst();

		return (costoEntity != null) ? new ProductoCostoDto(costoEntity) : null;
	}

	private List<ProductoCostoDto> getCostos(Integer productoId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoCostoEntity qproductoCosto = QProductoCostoEntity.productoCostoEntity;

		return queryFactory.selectFrom(qproductoCosto).where(qproductoCosto.id.eq(productoId))
				.orderBy(qproductoCosto.fecha.desc()).fetch().stream().map(costo -> new ProductoCostoDto(costo))
				.collect(Collectors.toList());
	}

	private List<ProductoRentaDto> getRentasActuales(Integer productoId) {

		List<ProductoRentaDto> rentas = new ArrayList<ProductoRentaDto>();

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoPrecioEntity qproductoPrecio = QProductoPrecioEntity.productoPrecioEntity;

		ProductoPrecioEntity rentaLista1 = queryFactory.selectFrom(qproductoPrecio)
				.where(qproductoPrecio.id.eq(productoId).and(qproductoPrecio.fecha.loe(LocalDate.now()))
						.and(qproductoPrecio.listaPrecio.id.eq(ListaPrecioEnum.LISTA_1.getValor())))
				.orderBy(qproductoPrecio.fecha.desc()).fetchFirst();

		ProductoPrecioEntity rentaLista2 = queryFactory.selectFrom(qproductoPrecio)
				.where(qproductoPrecio.id.eq(productoId).and(qproductoPrecio.fecha.loe(LocalDate.now()))
						.and(qproductoPrecio.listaPrecio.id.eq(ListaPrecioEnum.LISTA_2.getValor())))
				.orderBy(qproductoPrecio.fecha.desc()).fetchFirst();

		ProductoPrecioEntity rentaLista3 = queryFactory.selectFrom(qproductoPrecio)
				.where(qproductoPrecio.id.eq(productoId).and(qproductoPrecio.fecha.loe(LocalDate.now()))
						.and(qproductoPrecio.listaPrecio.id.eq(ListaPrecioEnum.LISTA_3.getValor())))
				.orderBy(qproductoPrecio.fecha.desc()).fetchFirst();

		ProductoPrecioEntity rentaLista4 = queryFactory.selectFrom(qproductoPrecio)
				.where(qproductoPrecio.id.eq(productoId).and(qproductoPrecio.fecha.loe(LocalDate.now()))
						.and(qproductoPrecio.listaPrecio.id.eq(ListaPrecioEnum.LISTA_4.getValor())))
				.orderBy(qproductoPrecio.fecha.desc()).fetchFirst();

		ProductoPrecioEntity rentaLista5 = queryFactory.selectFrom(qproductoPrecio)
				.where(qproductoPrecio.id.eq(productoId).and(qproductoPrecio.fecha.loe(LocalDate.now()))
						.and(qproductoPrecio.listaPrecio.id.eq(ListaPrecioEnum.LISTA_5.getValor())))
				.orderBy(qproductoPrecio.fecha.desc()).fetchFirst();

		ProductoPrecioEntity rentaLista6 = queryFactory.selectFrom(qproductoPrecio)
				.where(qproductoPrecio.id.eq(productoId).and(qproductoPrecio.fecha.loe(LocalDate.now()))
						.and(qproductoPrecio.listaPrecio.id.eq(ListaPrecioEnum.LISTA_6.getValor())))
				.orderBy(qproductoPrecio.fecha.desc()).fetchFirst();

		if (rentaLista1 != null) {
			rentas.add(new ProductoRentaDto(rentaLista1));
		}
		if (rentaLista2 != null) {
			rentas.add(new ProductoRentaDto(rentaLista2));
		}
		if (rentaLista3 != null) {
			rentas.add(new ProductoRentaDto(rentaLista3));
		}
		if (rentaLista4 != null) {
			rentas.add(new ProductoRentaDto(rentaLista4));
		}
		if (rentaLista5 != null) {
			rentas.add(new ProductoRentaDto(rentaLista5));
		}
		if (rentaLista6 != null) {
			rentas.add(new ProductoRentaDto(rentaLista6));
		}

		return rentas;
	}

	private void validarProducto(ProductoDto producto) throws ServiceException {

		if (producto.getDescripcion() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					String.format("El campo descripcion es obligatorio."));
		}

		if (producto.getEstado() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, String.format("El campo estado es obligatorio."));
		}

		if (producto.getGravado() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					String.format("El campo gravado es obligatorio."));
		}
	}

	private List<ProductoRentaDto> getRentas(Integer productoId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoPrecioEntity qproductoPrecio = QProductoPrecioEntity.productoPrecioEntity;

		return queryFactory.selectFrom(qproductoPrecio).where(qproductoPrecio.id.eq(productoId))
				.orderBy(qproductoPrecio.fecha.desc()).fetch().stream().map(renta -> new ProductoRentaDto(renta))
				.collect(Collectors.toList());

	}

	private ProductoDto save(ProductoDto producto) {

		final ProductoEntity productoSaved = productoRepository.save(toEntity(producto));

		return new ProductoDto(productoSaved);
	}

	private ProductoEntity toEntity(ProductoDto producto) {
		ProductoEntity entity = new ProductoEntity();
		entity.setDescripcion(producto.getDescripcion());
		entity.setDescripcionReducida(producto.getDescripcionReducida());
		entity.setCodigoBarras(producto.getCodigoBarras());
		entity.setEstado(estadoRepository.findOne(producto.getEstado().getId()));
		entity.setGravado(gravadoRepository.findOne(producto.getGravado().getId()));
		if (producto.getOrigen() != null && producto.getOrigen().getId() != null) {
			entity.setOrigen(origenRepository.findOne(producto.getOrigen().getId()));
		}

		return entity;
	}
}
