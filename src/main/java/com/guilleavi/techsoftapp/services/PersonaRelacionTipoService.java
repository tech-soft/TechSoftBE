package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.PersonaRelacionTipoDto;
import com.guilleavi.techsoftapp.repositories.PersonaRelacionTipoRepository;

@Service
public class PersonaRelacionTipoService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private PersonaRelacionTipoRepository personaRelacionTipoRepository = null;

	@Transactional(readOnly = true)
	public List<PersonaRelacionTipoDto> getPersonaRelacionTipos() {

		return personaRelacionTipoRepository.findAll().stream()
				.map(personaRelacionTipo -> new PersonaRelacionTipoDto(personaRelacionTipo))
				.collect(Collectors.toList());
	}
}
