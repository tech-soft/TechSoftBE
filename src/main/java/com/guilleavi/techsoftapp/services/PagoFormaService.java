package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.PagoFormaDto;
import com.guilleavi.techsoftapp.repositories.PagoFormaRepository;

@Service
public class PagoFormaService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private PagoFormaRepository pagoFormaRepository = null;

	@Transactional(readOnly = true)
	public List<PagoFormaDto> getPagoFormas() {

		return pagoFormaRepository.findAll().stream().map(pagoForma -> new PagoFormaDto(pagoForma)).collect(Collectors.toList());

	}
}
