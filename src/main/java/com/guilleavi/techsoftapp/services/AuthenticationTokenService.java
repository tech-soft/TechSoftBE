package com.guilleavi.techsoftapp.services;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.guilleavi.techsoftapp.auth.AuthenticationTokenDetails;
import com.guilleavi.techsoftapp.auth.JwtTokenIssuer;
import com.guilleavi.techsoftapp.auth.JwtTokenParser;
import com.guilleavi.techsoftapp.auth.exceptions.AuthenticationTokenRefreshmentException;
import com.guilleavi.techsoftapp.enums.Authority;

@Service
public class AuthenticationTokenService {

	@Value("${authentication.jwt.tiempoValidez}")
	private Long tiempoValidez;

	@Value("${authentication.jwt.cantidadRefrescos}")
	private Integer cantidadRefrescos;

	@Autowired
	private JwtTokenIssuer tokenIssuer = null;

	@Autowired
	private JwtTokenParser tokenParser = null;

	public String issueToken(String username, Set<Authority> authorities) {

		String id = generateTokenIdentifier();
		ZonedDateTime issuedDate = ZonedDateTime.now();
		ZonedDateTime expirationDate = calculateExpirationDate(issuedDate);

		AuthenticationTokenDetails authenticationTokenDetails = new AuthenticationTokenDetails.Builder().withId(id)
				.withUsername(username).withAuthorities(authorities).withIssuedDate(issuedDate)
				.withExpirationDate(expirationDate).withRefreshCount(0).withRefreshLimit(cantidadRefrescos).build();

		return tokenIssuer.issueToken(authenticationTokenDetails);
	}

	public AuthenticationTokenDetails parseToken(String token) {
		return tokenParser.parseToken(token);
	}

	public String refreshToken(AuthenticationTokenDetails currentTokenDetails) {

		if (!currentTokenDetails.isEligibleForRefreshment()) {
			throw new AuthenticationTokenRefreshmentException("This token cannot be refreshed.");
		}

		ZonedDateTime issuedDate = ZonedDateTime.now();
		ZonedDateTime expirationDate = calculateExpirationDate(issuedDate);

		AuthenticationTokenDetails newTokenDetails = new AuthenticationTokenDetails.Builder()
				.withId(currentTokenDetails.getId()) // Reuse the same id
				.withUsername(currentTokenDetails.getUsername()).withAuthorities(currentTokenDetails.getAuthorities())
				.withIssuedDate(issuedDate).withExpirationDate(expirationDate)
				.withRefreshCount(currentTokenDetails.getRefreshCount() + 1).withRefreshLimit(cantidadRefrescos)
				.build();

		return tokenIssuer.issueToken(newTokenDetails);
	}

	private ZonedDateTime calculateExpirationDate(ZonedDateTime issuedDate) {
		return issuedDate.plusSeconds(tiempoValidez);
	}

	private String generateTokenIdentifier() {
		return UUID.randomUUID().toString();
	}
}
