package com.guilleavi.techsoftapp.services;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.RemitoProveedorDto;
import com.guilleavi.techsoftapp.dto.RemitoProveedorItemDto;
import com.guilleavi.techsoftapp.dto.RemitoProveedorPreviewDto;
import com.guilleavi.techsoftapp.entities.ProductoEntity;
import com.guilleavi.techsoftapp.entities.ProductoSerieEntity;
import com.guilleavi.techsoftapp.entities.QProductoSerieEntity;
import com.guilleavi.techsoftapp.entities.QRemitoProveedorEntity;
import com.guilleavi.techsoftapp.entities.QRemitoProveedorItemEntity;
import com.guilleavi.techsoftapp.entities.RemitoProveedorEntity;
import com.guilleavi.techsoftapp.entities.RemitoProveedorItemEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.ComprobanteTipoRepository;
import com.guilleavi.techsoftapp.repositories.OrdenCompraRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;
import com.guilleavi.techsoftapp.repositories.ProductoRepository;
import com.guilleavi.techsoftapp.repositories.ProductoSerieRepository;
import com.guilleavi.techsoftapp.repositories.RemitoProveedorItemRepository;
import com.guilleavi.techsoftapp.repositories.RemitoProveedorRepository;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class RemitoProveedorService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private ComprobanteTipoRepository comprobanteTipoRepository = null;
	@Autowired
	private OrdenCompraRepository ordenCompraRepository = null;
	@Autowired
	private PersonaRepository personaRepository = null;
	@Autowired
	private ProductoRepository productoRepository = null;
	@Autowired
	private ProductoSerieRepository productoSerieRepository = null;
	@Autowired
	private RemitoProveedorRepository remitoProveedorRepository = null;
	@Autowired
	private RemitoProveedorItemRepository remitoProveedorItemRepository = null;

	@Autowired
	private OrdenCompraService ordenCompraService = null;
	@Autowired
	private ProductoStockService productoStockService = null;

	@Transactional(readOnly = true)
	public List<RemitoProveedorPreviewDto> getRemitosPreview(Integer proveedorId, LocalDate fecha, Long numero) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QRemitoProveedorEntity qremito = QRemitoProveedorEntity.remitoProveedorEntity;
		BooleanBuilder builder = new BooleanBuilder();

		if (proveedorId != null) {
			builder.and(qremito.proveedor.id.eq(proveedorId));
		}

		if (fecha != null) {
			builder.and(qremito.fecha.eq(fecha));
		}

		if (numero != null) {
			builder.and(qremito.numero.eq(numero));
		}

		if (builder == null || builder.getValue() == null) {
			return null;
		}

		return queryFactory.selectFrom(qremito).where(builder).fetch().stream()
				.map(remito -> new RemitoProveedorPreviewDto(remito)).collect(Collectors.toList());

	}

	@Transactional(readOnly = true)
	public RemitoProveedorDto getRemito(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de remito no puede ser nulo.");
		}

		RemitoProveedorEntity remitoEntity = remitoProveedorRepository.findOne(id);

		if (remitoEntity != null) {

			RemitoProveedorDto remitoProveedorDto = new RemitoProveedorDto(remitoEntity);

			List<RemitoProveedorItemDto> items = getItems(id);

			remitoProveedorDto.setItems(items);

			return remitoProveedorDto;
		} else {
			return null;
		}

	}

	@Transactional
	public RemitoProveedorDto insert(RemitoProveedorDto remito) throws ServiceException {

		if (remito == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El remito a insertar no se encuentra definido."));
		}

		if (remito.getId() != null && remitoProveedorRepository.findOne(remito.getId()) != null) {
			throw new ServiceException(ServiceException.DATA_INTEGRITY_VIOLATION,
					String.format("El 'id' del remito a insertar ya existe en la base de datos."));
		}

		validarRemito(remito);

		actualizarStock(remito);

		return save(remito);

	}

	@Transactional
	public RemitoProveedorDto update(Integer remitoId, String observaciones) throws ServiceException {

		if (remitoId == null || remitoProveedorRepository.findOne(remitoId) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' del remito que desea modificar no existe en la base de datos."));
		}

		RemitoProveedorEntity remito = remitoProveedorRepository.findOne(remitoId);

		remito.setObservaciones(observaciones);

		RemitoProveedorEntity remitoSaved = remitoProveedorRepository.save(remito);

		return new RemitoProveedorDto(remitoSaved);
	}

	private void actualizarStock(RemitoProveedorDto remito) {
		remito.getItems().forEach(item -> {

			if (item.getOrdenCompra() != null) {
				if (ordenCompraRepository.findOne(item.getOrdenCompra().getId()) != null) {
					ordenCompraService.recibirOrdenCompraItem(item.getOrdenCompra().getId(), item.getProducto().getId(),
							item.getCantidad());
					ordenCompraService.recibirOrdenCompra(item.getOrdenCompra().getId());
				}

			} else {
				productoStockService.editStockDisponible(item.getProducto().getId(), item.getCantidad(), true);
			}
		});
	}

	private List<RemitoProveedorItemDto> getItems(Integer remitoId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QRemitoProveedorItemEntity qitem = QRemitoProveedorItemEntity.remitoProveedorItemEntity;
		QProductoSerieEntity qproductoSerie = QProductoSerieEntity.productoSerieEntity;

		List<RemitoProveedorItemEntity> remitoItems = queryFactory.selectFrom(qitem).where(qitem.remito.id.eq(remitoId))
				.fetch();

		List<RemitoProveedorItemDto> remitoItemDtos = remitoItems.stream().map(item -> {

			RemitoProveedorItemDto itemDto = new RemitoProveedorItemDto(item);
			List<String> series = queryFactory.select(qproductoSerie.numeroSerie).from(qproductoSerie)
					.where(qproductoSerie.remitoProveedorItem.id.eq(item.getId())).fetch();

			if (series != null) {
				itemDto.setNumerosSerie(series);
			}
			return itemDto;

		}).collect(Collectors.toList());

		return remitoItemDtos;
	}

	private void validarRemito(RemitoProveedorDto remito) throws ServiceException {

		if (remito.getComprobanteTipo() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					String.format("El campo tipo de comprobante es obligatorio."));
		}

		if (remito.getProveedor() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					String.format("El campo proveedor es obligatorio."));
		}

		if (remito.getPuestoId() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, String.format("El campo puesto es obligatorio."));
		}

		if (remito.getNumero() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, String.format("El campo número es obligatorio."));
		}

		if (remito.getFecha() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, String.format("El campo fecha es obligatorio."));
		}

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QRemitoProveedorEntity qremito = QRemitoProveedorEntity.remitoProveedorEntity;

		Long remitoRepetido = queryFactory.selectFrom(qremito)
				.where(qremito.proveedor.id.eq(remito.getProveedor().getId())
						.and(qremito.puesto.eq(remito.getPuestoId())).and(qremito.numero.eq(remito.getNumero())))
				.fetchCount();

		if (remitoRepetido > 0) {
			throw new ServiceException(ServiceException.MISSING_DATA, String.format(
					"Ya existe un remito cargado con el mismo puesto y numero para el proveedor seleccionado."));
		}

		if (remito.getItems() == null || remito.getItems().size() == 0) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					String.format("El remito debe contener al menos un item."));
		}

		validarItems(remito.getItems());
	}

	private void validarItems(List<RemitoProveedorItemDto> items) throws ServiceException {

		LinkedList<String> numerosSerie = new LinkedList<>();

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoSerieEntity qserie = QProductoSerieEntity.productoSerieEntity;

		items.forEach(item -> {
			if (item.getProducto() == null) {
				throw new ServiceException(ServiceException.MISSING_DATA,
						String.format("Todos los items deben contener un producto."));
			}
			if (item.getCantidad() == null || item.getCantidad() < 1) {
				throw new ServiceException(ServiceException.MISSING_DATA,
						String.format("Todos los items deben tener especificada su cantidad."));
			}
			if (item.getOrdenCompra() != null && item
					.getCantidad() > (item.getCantidadOrdenCompra() != null ? item.getCantidadOrdenCompra() : 0)) {
				throw new ServiceException(ServiceException.MISSING_DATA, String.format(
						"La cantidad de un item con orden de compra no puede superar la cantidad indicada en la misma."));
			}
			if (item.getCantidad() < ((item.getNumerosSerie() != null) ? item.getNumerosSerie().size() : 0)) {
				throw new ServiceException(ServiceException.MISSING_DATA, String.format(
						"La cantidad de números de serie no puede ser mayor a la cantidad de productos del item."));
			}

			if (item.getNumerosSerie() != null) {
				item.getNumerosSerie().forEach(serie -> {
					Long serieRepetido = queryFactory.selectFrom(qserie).where(qserie.numeroSerie.eq(serie.trim()))
							.fetchCount();

					if (serieRepetido > 0) {
						throw new ServiceException(ServiceException.MISSING_DATA,
								String.format("El número de serie " + serie + " ya existe en la base de datos."));
					}

					numerosSerie.add(serie);
				});
			}
		});

		Set<String> series = new HashSet<>();

		Set<String> numerosSerieRepetidos = numerosSerie.stream().filter(serie -> !series.add(serie))
				.collect(Collectors.toSet());

		if (numerosSerieRepetidos.size() > 0) {
			throw new ServiceException(ServiceException.DATA_INTEGRITY_VIOLATION,
					String.format("No puede haber números de serie duplicados."));

		}

	}

	private RemitoProveedorDto save(RemitoProveedorDto remito) {

		final RemitoProveedorEntity remitoSaved = remitoProveedorRepository.save(toEntity(remito));

		remito.getItems().forEach(item -> {
			RemitoProveedorItemEntity itemEntity = toEntity(item);
			itemEntity.setRemito(remitoSaved);

			if (itemEntity.getOrden() != null && itemEntity.getOrden().getId() == null) {
				itemEntity.setOrden(null);
			}

			RemitoProveedorItemEntity itemSaved = remitoProveedorItemRepository.save(itemEntity);

			item.getNumerosSerie().forEach(serie -> {
				ProductoSerieEntity serieEntity = new ProductoSerieEntity();
				serieEntity.setNumeroSerie(serie);
				serieEntity.setRemitoProveedorItem(itemSaved);
				productoSerieRepository.save(serieEntity);
			});
		});

		RemitoProveedorDto result = new RemitoProveedorDto(remitoSaved);
		result.setItems(getItems(result.getId()));

		return result;

	}

	private RemitoProveedorEntity toEntity(RemitoProveedorDto remito) {
		RemitoProveedorEntity entity = new RemitoProveedorEntity();
		entity.setComprobanteTipo(comprobanteTipoRepository.findOne(remito.getComprobanteTipo().getId()));
		entity.setPuesto(remito.getPuestoId());
		entity.setNumero(remito.getNumero());
		entity.setProveedor(personaRepository.findOne(remito.getProveedor().getId()));
		entity.setFecha(remito.getFecha());
		entity.setObservaciones(remito.getObservaciones());

		return entity;
	}

	private RemitoProveedorItemEntity toEntity(RemitoProveedorItemDto item) {
		RemitoProveedorItemEntity entity = new RemitoProveedorItemEntity();
		entity.setProducto((item.getProducto() != null && item.getProducto().getId() != null)
				? productoRepository.findOne(item.getProducto().getId())
				: new ProductoEntity());
		entity.setCantidad(item.getCantidad());
		entity.setOrden(
				(item.getOrdenCompra() != null) ? ordenCompraRepository.findOne(item.getOrdenCompra().getId()) : null);
		entity.setCantidadOrdenCompra(item.getCantidadOrdenCompra());

		return entity;
	}
}
