package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.PersonaDto;
import com.guilleavi.techsoftapp.dto.PersonaRelacionadaDto;
import com.guilleavi.techsoftapp.entities.PersonaRelacionadaEntity;
import com.guilleavi.techsoftapp.entities.QPersonaRelacionadaEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.PersonaRelacionTipoRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRelacionadaRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class PersonaRelacionadaService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private PersonaRepository personaRepository = null;
	@Autowired
	private PersonaRelacionadaRepository personaRelacionadaRepository = null;
	@Autowired
	private PersonaRelacionTipoRepository personaRelacionTipoRepository = null;

	@Transactional(readOnly = true)
	public PersonaRelacionadaDto getPersonaRelacionada(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					"El 'id' de persona relacionada no puede ser nulo.");
		}

		PersonaRelacionadaEntity personaRelacionadaEntity = personaRelacionadaRepository.findOne(id);

		return (personaRelacionadaEntity != null) ? new PersonaRelacionadaDto(personaRelacionadaEntity) : null;

	}

	@Transactional(readOnly = true)
	public List<PersonaRelacionadaDto> getPersonasRelacionadasByPersona(Integer personaId) throws ServiceException {

		if (personaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QPersonaRelacionadaEntity qpersonaRelacionada = QPersonaRelacionadaEntity.personaRelacionadaEntity;

		return queryFactory.selectFrom(qpersonaRelacionada)
				.where(qpersonaRelacionada.personaPrincipal.id.eq(personaId)
						.or(qpersonaRelacionada.personaRelacionada.id.eq(personaId)))
				.fetch().stream().map(personaRelacionada -> getRelacionada(personaRelacionada, personaId))
				.collect(Collectors.toList());
	}

	private PersonaRelacionadaDto getRelacionada(PersonaRelacionadaEntity personaRelacionada, Integer personaId) {

		PersonaRelacionadaDto personaRelacionadaDto = new PersonaRelacionadaDto(personaRelacionada);

		if (personaRelacionada.getPersonaPrincipal().getId().equals(personaId)) {
			personaRelacionadaDto.setPersonaRelacionada(new PersonaDto(personaRelacionada.getPersonaRelacionada()));
		} else if (personaRelacionada.getPersonaRelacionada().getId().equals(personaId)) {
			personaRelacionadaDto.setPersonaRelacionada(new PersonaDto(personaRelacionada.getPersonaPrincipal()));
		}

		return personaRelacionadaDto;
	}

	@Transactional
	public PersonaRelacionadaDto upsert(PersonaRelacionadaDto personaRelacionada) throws ServiceException {

		if (personaRelacionada == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La relación de persona a insertar no se encuentra definido."));
		}

		return save(personaRelacionada);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					"El 'id' de persona relacionada no puede ser nulo.");
		}

		personaRelacionadaRepository.delete(personaRelacionadaRepository.findOne(id));
	}

	@Transactional
	public void deleteByPersonaId(Integer personaId) throws ServiceException {

		if (personaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		getPersonasRelacionadasByPersona(personaId)
				.forEach(personaRelacionada -> personaRelacionadaRepository.delete(personaRelacionada.getId()));

	}

	private PersonaRelacionadaDto save(PersonaRelacionadaDto personaRelacionada) {

		PersonaRelacionadaEntity personaRelacionadaEntity = new PersonaRelacionadaEntity();
		if (personaRelacionada.getId() != null) {
			personaRelacionadaEntity.setId(personaRelacionada.getId());
		}
		personaRelacionadaEntity
				.setPersonaPrincipal(personaRepository.findOne(personaRelacionada.getPersonaPrincipal().getId()));
		personaRelacionadaEntity
				.setPersonaRelacionada(personaRepository.findOne(personaRelacionada.getPersonaRelacionada().getId()));
		personaRelacionadaEntity.setTipo(personaRelacionTipoRepository.findOne(personaRelacionada.getTipo().getId()));

		PersonaRelacionadaEntity result = personaRelacionadaRepository.save(personaRelacionadaEntity);
		
		return (result != null) ? new PersonaRelacionadaDto(result) : null;

	}
}
