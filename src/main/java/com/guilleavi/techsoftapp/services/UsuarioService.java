package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.CharMatcher;
import com.guilleavi.techsoftapp.entities.QUsuarioEntity;
import com.guilleavi.techsoftapp.entities.QUsuarioGrupoEntity;
import com.guilleavi.techsoftapp.entities.UsuarioEntity;
import com.guilleavi.techsoftapp.entities.UsuarioGrupoEntity;
import com.guilleavi.techsoftapp.enums.Authority;
import com.guilleavi.techsoftapp.repositories.UsuarioRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class UsuarioService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private UsuarioRepository usuarioRepository = null;

	public List<UsuarioEntity> findAllUsuarios() {

		List<UsuarioEntity> usuarios = usuarioRepository.findAll();

		return usuarios;
	}

	public UsuarioEntity findUsuario(Integer id) {
		return usuarioRepository.findOne(id);
	}

	public UsuarioEntity findByUsername(String username) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);

		QUsuarioEntity qUsuario = QUsuarioEntity.usuarioEntity;
		UsuarioEntity usuario = queryFactory.selectFrom(qUsuario)
				.where(qUsuario.username.contains(CharMatcher.is('\'').trimFrom(username).toUpperCase())).fetchOne();

		return usuario;
	}

	public List<Authority> findGrupos(Integer id) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);

		QUsuarioGrupoEntity qUsuarioGrupo = QUsuarioGrupoEntity.usuarioGrupoEntity;
		List<UsuarioGrupoEntity> usuarioGrupos = queryFactory.selectFrom(qUsuarioGrupo)
				.where(qUsuarioGrupo.usuario.id.eq(id)).fetch();

		List<Authority> authorities = usuarioGrupos.stream().map(entity -> {
			if (entity.getGrupo().getDescripcion().toUpperCase().equals("ADMIN")) {
				Authority authority = Authority.ADMIN;
				return authority;
			} else {
				Authority authority = Authority.USER;
				return authority;
			}
		}).collect(Collectors.toList());

		return authorities;

	}

}
