package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.ContactoDto;
import com.guilleavi.techsoftapp.entities.ContactoEntity;
import com.guilleavi.techsoftapp.entities.QContactoEntity;
import com.guilleavi.techsoftapp.enums.ContactoEnum;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.ContactoRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;
import com.guilleavi.techsoftapp.repositories.SysContactoTipoRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class ContactoService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private ContactoRepository contactoRepository = null;
	@Autowired
	private PersonaRepository personaRepository = null;
	@Autowired
	private SysContactoTipoRepository sysContactoTipoRepository = null;

	@Transactional(readOnly = true)
	public ContactoDto getContacto(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de contacto no puede ser nulo.");
		}

		ContactoEntity contactoEntity = contactoRepository.findOne(id);

		return (contactoEntity != null) ? new ContactoDto(contactoEntity) : null;

	}

	@Transactional(readOnly = true)
	public List<ContactoDto> getContactosByPersona(Integer personaId, Integer tipo) throws ServiceException {

		if (personaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		if (tipo == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'tipo' de contacto no puede ser nulo.");
		}

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QContactoEntity qcontacto = QContactoEntity.contactoEntity;

		return queryFactory.selectFrom(qcontacto)
				.where(qcontacto.persona.id.eq(personaId).and(qcontacto.tipo.id.eq(tipo))).fetch().stream()
				.map(contacto -> new ContactoDto(contacto)).collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	public List<ContactoDto> getContactosByPersona(Integer personaId) throws ServiceException {

		if (personaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QContactoEntity qcontacto = QContactoEntity.contactoEntity;

		return queryFactory.selectFrom(qcontacto).where(qcontacto.persona.id.eq(personaId)).fetch().stream()
				.map(contacto -> new ContactoDto(contacto)).collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	public String getTelefonoPrincipal(Integer personaId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QContactoEntity qtelefono = QContactoEntity.contactoEntity;

		ContactoEntity telefono = queryFactory.selectFrom(qtelefono)
				.where(qtelefono.persona.id.eq(personaId).and(qtelefono.tipo.id.eq(ContactoEnum.TELEFONO.getValor())))
				.fetchFirst();

		return telefono == null ? "" : telefono.getContacto();

	}

	@Transactional
	public ContactoDto upsert(ContactoDto contacto) throws ServiceException {

		if (contacto == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El contacto a insertar no se encuentra definido."));
		}
		
		return save(contacto);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de contacto no puede ser nulo.");
		}

		contactoRepository.delete(contactoRepository.findOne(id));
	}

	@Transactional
	public void deleteByPersonaId(Integer personaId) throws ServiceException {

		if (personaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		getContactosByPersona(personaId).forEach(contacto -> contactoRepository.delete(contacto.getId()));

	}

	private ContactoDto save(ContactoDto contacto) {

		ContactoEntity contactoEntity = new ContactoEntity();
		if (contacto.getId() != null) {
		
			contactoEntity.setId(contacto.getId());
		}
		contactoEntity.setTipo(sysContactoTipoRepository.findOne(contacto.getTipo()));
		contactoEntity.setDescripcion(contacto.getDescripcion());
		contactoEntity.setContacto(contacto.getContacto());
		contactoEntity.setPersona(personaRepository.findOne(contacto.getPersonaId()));

		ContactoEntity result = contactoRepository.save(contactoEntity);
		
		return (result != null) ? new ContactoDto(result) : null;

	}
}
