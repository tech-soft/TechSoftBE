package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.ProvinciaDto;
import com.guilleavi.techsoftapp.entities.ProvinciaEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.PaisRepository;
import com.guilleavi.techsoftapp.repositories.ProvinciaRepository;

@Service
public class ProvinciaService {

	@Autowired
	private PaisRepository paisRepository = null;
	@Autowired
	private ProvinciaRepository provinciaRepository = null;

	@Transactional(readOnly = true)
	public ProvinciaDto getProvincia(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de provincia no puede ser nulo.");
		}

		ProvinciaEntity provinciaEntity = provinciaRepository.findOne(id);

		return (provinciaEntity != null) ? new ProvinciaDto(provinciaEntity) : null;
	}

	@Transactional(readOnly = true)
	public List<ProvinciaDto> getProvincias() {

		return provinciaRepository.findAll().stream().map(provincia -> new ProvinciaDto(provincia))
				.collect(Collectors.toList());

	}

	@Transactional
	public ProvinciaDto insert(ProvinciaDto provincia) throws ServiceException {

		if (provincia == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La provincia a insertar no se encuentra definido."));
		}

		if (provincia.getId() != null && provinciaRepository.findOne(provincia.getId()) != null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' de la provincia a insertar ya existe en la base de datos."));
		}

		return save(provincia);
	}

	@Transactional
	public ProvinciaDto update(ProvinciaDto provincia) throws ServiceException {

		if (provincia == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La provincia a modificar no se encuentra definido."));
		}

		if (provincia.getId() == null || provinciaRepository.findOne(provincia.getId()) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' de la provincia que desea modificar no existe en la base de datos."));
		}

		return save(provincia);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de provincia no puede ser nulo.");
		}

		if (provinciaRepository.findOne(id) == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "La provincia que se desea eliminar no existe.");
		}

		provinciaRepository.delete(provinciaRepository.findOne(id));
	}

	private ProvinciaDto save(ProvinciaDto provincia) {

		ProvinciaEntity provinciaEntity = new ProvinciaEntity();
		provinciaEntity.setId(provincia.getId());
		provinciaEntity.setNombre(provincia.getNombre());
		provinciaEntity.setPais(paisRepository.findOne(provincia.getPais().getId()));

		ProvinciaEntity result = provinciaRepository.save(provinciaEntity);
		
		return (result != null) ? new ProvinciaDto(result) : null;

	}
}
