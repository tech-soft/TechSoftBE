package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.SucursalDto;
import com.guilleavi.techsoftapp.entities.SucursalEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.PuestoRepository;
import com.guilleavi.techsoftapp.repositories.SucursalRepository;

@Service
public class SucursalService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private PuestoRepository puestoRepository = null;
	@Autowired
	private SucursalRepository sucursalRepository = null;

	@Transactional(readOnly = true)
	public SucursalDto getSucursal(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de sucursal no puede ser nulo.");
		}

		SucursalEntity sucursalEntity = sucursalRepository.findOne(id);

		return (sucursalEntity != null) ? new SucursalDto(sucursalEntity) : null;

	}

	@Transactional(readOnly = true)
	public List<SucursalDto> getSucursales() {

		return sucursalRepository.findAll().stream().map(sucursal -> new SucursalDto(sucursal))
				.collect(Collectors.toList());

	}

	@Transactional
	public SucursalDto insert(SucursalDto sucursal) throws ServiceException {

		if (sucursal == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La sucursal a insertar no se encuentra definida."));
		}

		if (sucursal.getId() != null && sucursalRepository.findOne(sucursal.getId()) != null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' de la sucursal a insertar ya existe en la base de datos."));
		}

		return save(sucursal);
	}

	@Transactional
	public SucursalDto update(SucursalDto sucursal) throws ServiceException {

		if (sucursal == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La sucursal a modificar no se encuentra definida."));
		}

		if (sucursal.getId() == null || sucursalRepository.findOne(sucursal.getId()) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' de la sucursal que desea modificar no existe en la base de datos."));
		}

		return save(sucursal);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de sucursal no puede ser nulo.");
		}

		if (sucursalRepository.findOne(id) == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "La sucursal que se desea eliminar no existe.");
		}

		sucursalRepository.delete(sucursalRepository.findOne(id));
	}

	private SucursalDto save(SucursalDto sucursal) {

		SucursalEntity sucursalEntity = new SucursalEntity();
		sucursalEntity.setId(sucursal.getId());
		sucursalEntity.setDescripcion(sucursal.getDescripcion());
		sucursalEntity.setPuesto(puestoRepository.findOne(sucursal.getPuestoId()));

		SucursalEntity result = sucursalRepository.save(sucursalEntity);
		
		return (result != null) ? new SucursalDto(result) : null;

	}
}
