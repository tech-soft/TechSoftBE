package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.IvaTipoDto;
import com.guilleavi.techsoftapp.repositories.IvaTipoRepository;

@Service
public class IvaTipoService {

	@Autowired
	private IvaTipoRepository ivaTipoRepository = null;

	@Transactional(readOnly = true)
	public List<IvaTipoDto> getIvaTipos() {

		return ivaTipoRepository.findAll().stream().map(ivaTipo -> new IvaTipoDto(ivaTipo)).collect(Collectors.toList());

	}
}