package com.guilleavi.techsoftapp.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.EstadoDto;
import com.guilleavi.techsoftapp.dto.OrdenCompraDto;
import com.guilleavi.techsoftapp.dto.OrdenCompraItemDto;
import com.guilleavi.techsoftapp.dto.OrdenCompraPreviewDto;
import com.guilleavi.techsoftapp.dto.ProductoPreviewDto;
import com.guilleavi.techsoftapp.entities.OrdenCompraEntity;
import com.guilleavi.techsoftapp.entities.OrdenCompraItemEntity;
import com.guilleavi.techsoftapp.entities.QOrdenCompraEntity;
import com.guilleavi.techsoftapp.entities.QOrdenCompraItemEntity;
import com.guilleavi.techsoftapp.enums.EstadoEnum;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.EstadoRepository;
import com.guilleavi.techsoftapp.repositories.OrdenCompraItemRepository;
import com.guilleavi.techsoftapp.repositories.OrdenCompraRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;
import com.guilleavi.techsoftapp.repositories.ProductoRepository;
import com.guilleavi.techsoftapp.repositories.PuestoRepository;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class OrdenCompraService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private ProductoStockService productoStockService = null;

	@Autowired
	private EstadoRepository estadoRepository = null;
	@Autowired
	private OrdenCompraRepository ordenCompraRepository = null;
	@Autowired
	private OrdenCompraItemRepository ordenCompraItemRepository = null;
	@Autowired
	private PersonaRepository personaRepository = null;
	@Autowired
	private ProductoRepository productoRepository = null;
	@Autowired
	private PuestoRepository puestoRepository = null;

	@Transactional(readOnly = true)
	public OrdenCompraDto getOrden(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de orden no puede ser nulo.");
		}

		OrdenCompraEntity ordenEntity = ordenCompraRepository.findOne(id);

		if (ordenEntity != null) {

			OrdenCompraDto ordenDto = new OrdenCompraDto(ordenEntity);

			ordenDto.setItems(getItems(id));

			return ordenDto;
		} else {
			return null;
		}
	}

	@Transactional(readOnly = true)
	public List<OrdenCompraPreviewDto> getOrdenesPreview(Integer proveedorId, LocalDate fechaCreacion, Integer puestoId,
			Long numero, Integer estadoId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraEntity qorden = QOrdenCompraEntity.ordenCompraEntity;
		BooleanBuilder builder = new BooleanBuilder();

		if (proveedorId != null) {
			builder.and(qorden.proveedor.id.eq(proveedorId));
		}

		if (fechaCreacion != null) {
			builder.and(qorden.fechaCreacion.eq(fechaCreacion));
		}

		if (puestoId != null) {
			builder.and(qorden.puesto.id.eq(puestoId));
		}

		if (numero != null) {
			builder.and(qorden.numero.eq(numero));
		}

		if (estadoId != null) {
			builder.and(qorden.estado.id.eq(estadoId));
		}

		if (builder == null || builder.getValue() == null) {
			return null;
		}

		return queryFactory.selectFrom(qorden).where(builder).fetch().stream()
				.map(orden -> new OrdenCompraPreviewDto(orden)).collect(Collectors.toList());

	}

	@Transactional(readOnly = true)
	public List<OrdenCompraPreviewDto> getOrdenesEmitidas(Integer proveedorId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraEntity qorden = QOrdenCompraEntity.ordenCompraEntity;

		return queryFactory.selectFrom(qorden)
				.where(qorden.proveedor.id.eq(proveedorId).and(qorden.estado.id.eq(EstadoEnum.EMITIDA.getValor())))
				.fetch().stream().map(orden -> new OrdenCompraPreviewDto(orden)).collect(Collectors.toList());
	}

	@Transactional
	public OrdenCompraDto insert(OrdenCompraDto orden) throws ServiceException {

		if (orden == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La orden a insertar no se encuentra definida."));
		}

		if (orden.getId() != null && ordenCompraRepository.findOne(orden.getId()) != null) {
			throw new ServiceException(ServiceException.DATA_INTEGRITY_VIOLATION,
					String.format("El 'id' de la orden a insertar ya existe en la base de datos."));
		}

		validarOrden(orden);

		orden = setearDatosAdicionales(orden);
		return save(orden);

	}

	@Transactional
	public OrdenCompraDto update(OrdenCompraDto orden) throws ServiceException {

		if (orden == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La orden a modificar no se encuentra definida."));
		}

		if (orden.getId() == null || ordenCompraRepository.findOne(orden.getId()) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' de la orden que desea modificar no existe en la base de datos."));
		}

		validarOrden(orden);

		limpiarItems(orden.getId());

		return save(orden);
	}

	@Transactional
	public OrdenCompraDto emitirOrdenCompra(OrdenCompraDto ordenCompraDto) {

		if (!ordenCompraDto.getEstado().getId().equals(EstadoEnum.NUEVA.getValor())) {
			// TODO: revisar test porque al comprar valores mayores a 127 con el != fallaba con llamado del cliente
			throw new ServiceException(ServiceException.DATA_INTEGRITY_VIOLATION,
					String.format("Para emitir una orden su estado previo debe ser NUEVA."));
		}

		ordenCompraDto.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.EMITIDA.getValor())));

		ordenCompraRepository.save(toEntity(ordenCompraDto));

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

		List<OrdenCompraItemEntity> items = queryFactory.selectFrom(qitem)
				.where(qitem.orden.id.eq(ordenCompraDto.getId())).fetch();

		items.forEach(item -> {
			item.setEstado(estadoRepository.findOne(EstadoEnum.ITEM_PEDIDO.getValor()));

			ordenCompraItemRepository.save(item);
			productoStockService.editStockEntrante(item.getProducto().getId(), item.getCantidad(), true);
		});

		OrdenCompraDto ordenEmitida = new OrdenCompraDto(ordenCompraRepository.findOne(ordenCompraDto.getId()));
		List<OrdenCompraItemDto> itemsEmitidos = getItems(ordenEmitida.getId());
		ordenEmitida.setItems(itemsEmitidos);

		return ordenEmitida;
	}

	@Transactional
	public OrdenCompraDto cancelarEmisionOrdenCompra(OrdenCompraDto ordenCompraDto) {

		if (!ordenCompraDto.getEstado().getId().equals(EstadoEnum.EMITIDA.getValor())) {
			throw new ServiceException(ServiceException.DATA_INTEGRITY_VIOLATION,
					String.format("Para cancelar la emisión de una orden su estado previo debe ser EMITIDA."));
		}

		if (hayItemsRecibidos(ordenCompraDto.getId())) {
			throw new ServiceException(ServiceException.DATA_INTEGRITY_VIOLATION, String
					.format("No se puede cancelar la emisión de una orden de compras que posee productos RECIBIDOS."));

		}

		ordenCompraDto.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));

		ordenCompraRepository.save(toEntity(ordenCompraDto));

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

		List<OrdenCompraItemEntity> items = queryFactory.selectFrom(qitem)
				.where(qitem.orden.id.eq(ordenCompraDto.getId())).fetch();

		items.forEach(item -> {
			item.setEstado(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor()));

			ordenCompraItemRepository.save(item);
			productoStockService.editStockEntrante(item.getProducto().getId(), item.getCantidad(), false);
		});

		OrdenCompraDto ordenCancelada = new OrdenCompraDto(ordenCompraRepository.findOne(ordenCompraDto.getId()));
		List<OrdenCompraItemDto> itemsCancelados = getItems(ordenCancelada.getId());
		ordenCancelada.setItems(itemsCancelados);

		return ordenCancelada;
	}

	@Transactional
	public void recibirOrdenCompra(Integer ordenId) {
		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

		List<OrdenCompraItemEntity> items = queryFactory.selectFrom(qitem).where(qitem.orden.id.eq(ordenId)).fetch();

		Long cantidadItemsRecibidos = items.stream()
				.filter(item -> item.getEstado().getId().equals(EstadoEnum.ITEM_RECIBIDO.getValor())).count();
		Long cantidadItemsOrden = items.stream().count();

		if (cantidadItemsOrden.equals(cantidadItemsRecibidos)) {
			OrdenCompraEntity ordenRecibida = ordenCompraRepository.findOne(ordenId);
			ordenRecibida.setEstado(estadoRepository.findOne(EstadoEnum.RECIBIDA.getValor()));
			ordenCompraRepository.save(ordenRecibida);
		}
	}

	@Transactional
	public void recibirOrdenCompraItem(Integer ordenId, Integer productoId, Integer cantidad) {
		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

		OrdenCompraItemEntity item = queryFactory.selectFrom(qitem)
				.where(qitem.orden.id.eq(ordenId).and(qitem.producto.id.eq(productoId))).fetchOne();

		productoStockService.editStockEntrante(productoId, cantidad, false);
		productoStockService.editStockDisponible(productoId, cantidad, true);

		if (item.getPendientes().equals(cantidad)) {
			item.setEstado(estadoRepository.findOne(EstadoEnum.ITEM_RECIBIDO.getValor()));
		}
		item.setPendientes(item.getPendientes() - cantidad);

		ordenCompraItemRepository.save(item);

	}

	@Transactional
	public void delete(Integer ordenId) throws ServiceException {

		if (ordenId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					"El 'id' de la orden de compra no puede ser nulo.");
		}

		if (ordenCompraRepository.findOne(ordenId) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					"La orden de compra que se desea eliminar no existe.");
		}

		if (!ordenCompraRepository.findOne(ordenId).getEstado().getId().equals(EstadoEnum.NUEVA.getValor())) {
			throw new ServiceException(ServiceException.DATA_INTEGRITY_VIOLATION,
					"Solo pueden eliminarse órdenes de compra en estado NUEVA.");
		}

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

		List<OrdenCompraItemEntity> items = queryFactory.selectFrom(qitem).where(qitem.orden.id.eq(ordenId)).fetch();

		items.forEach(item -> ordenCompraItemRepository.delete(item.getId()));

		ordenCompraRepository.delete(ordenId);
	}

	private Boolean hayItemsRecibidos(Integer ordenId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

		List<OrdenCompraItemEntity> items = queryFactory.selectFrom(qitem).where(qitem.orden.id.eq(ordenId)).fetch();

		Long itemsRecibidos = items.stream().filter(item -> item.getPendientes() < item.getCantidad()).count();

		return itemsRecibidos > 0;
	}

	private List<OrdenCompraItemDto> getItems(Integer ordenId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

		List<OrdenCompraItemDto> ordenCompraItemDtos = queryFactory.selectFrom(qitem).where(qitem.orden.id.eq(ordenId))
				.fetch().stream().map(item -> new OrdenCompraItemDto(item)).collect(Collectors.toList());

		return ordenCompraItemDtos;
	}

	private void validarOrden(OrdenCompraDto orden) throws ServiceException {

		if (orden.getPuesto() == null || orden.getPuesto().getCodigo() < 1) {
			throw new ServiceException(ServiceException.MISSING_DATA, String.format("El campo puesto es obligatorio."));
		}

		if (orden.getProveedor() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					String.format("El campo proveedor es obligatorio."));
		}

		if (orden.getItems().size() == 0) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					String.format("La orden debe contener al menos un item."));
		}

		validarItems(orden.getItems());
	}

	private void validarItems(List<OrdenCompraItemDto> items) throws ServiceException {

		LinkedList<ProductoPreviewDto> productos = new LinkedList<>();

		items.forEach(item -> {
			if (item.getProducto() == null) {
				throw new ServiceException(ServiceException.MISSING_DATA,
						String.format("El campo producto es obligatorio."));
			}

			if (item.getCantidad() == null || item.getCantidad() < 1) {
				throw new ServiceException(ServiceException.MISSING_DATA,
						String.format("El campo cantidad es obligatorio."));
			}
			productos.add(item.getProducto());
		});
		
		List<Integer> productosIds = new ArrayList<Integer>();
		Set<Integer> ids = new HashSet<>();

		productos.forEach(producto -> {
			productosIds.add(producto.getId());
		});

		Set<Integer> productosRepetidos = productosIds.stream()
		        .filter(id -> !ids.add(id))
		        .collect(Collectors.toSet());

		if (productosRepetidos.size() > 0) {
			throw new ServiceException(ServiceException.DATA_INTEGRITY_VIOLATION,
					String.format("No puede haber productos duplicados."));

		}

	}

	private void limpiarItems(Integer ordenId) {
		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QOrdenCompraItemEntity qitem = QOrdenCompraItemEntity.ordenCompraItemEntity;

		List<OrdenCompraItemEntity> items = queryFactory.selectFrom(qitem).where(qitem.orden.id.eq(ordenId)).fetch();

		items.forEach(item -> {
			ordenCompraItemRepository.delete(item.getId());
		});
	}

	private OrdenCompraDto setearDatosAdicionales(OrdenCompraDto orden) {

		if (orden.getNumero() == null) {
			// TODO: test this
			orden.setNumero(getUltimoNro(orden.getPuesto().getCodigo()));
		}

		if (orden.getFechaCreacion() == null) {
			orden.setFechaCreacion(LocalDate.now());
		}

		if (orden.getEstado() == null) {
			orden.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.NUEVA.getValor())));
		}

		orden.getItems().forEach(item -> {
			setearDatosAdicionales(item);
		});

		return orden;
	}

	private OrdenCompraItemDto setearDatosAdicionales(OrdenCompraItemDto item) {

		if (item.getEstado() == null) {
			item.setEstado(new EstadoDto(estadoRepository.findOne(EstadoEnum.ITEM_NUEVO.getValor())));
		}

		return item;
	}

	private Long getUltimoNro(Integer puestoCodigo) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);

		QOrdenCompraEntity qorden = QOrdenCompraEntity.ordenCompraEntity;
		Long ultimoNumero = queryFactory.select(qorden.numero).from(qorden).where(qorden.puesto.codigo.eq(puestoCodigo))
				.orderBy(qorden.numero.desc()).fetchFirst();

		if (ultimoNumero == null) {
			ultimoNumero = 1L;
		} else {
			ultimoNumero++;
		}

		return ultimoNumero;
	}

	private OrdenCompraDto save(OrdenCompraDto orden) {

		final OrdenCompraEntity ordenSaved = ordenCompraRepository.save(toEntity(orden));

		orden.getItems().forEach(item -> {
			OrdenCompraItemEntity itemEntity = toEntity(item);
			itemEntity.setOrden(ordenSaved);

			ordenCompraItemRepository.save(itemEntity);
		});

		OrdenCompraDto result = new OrdenCompraDto(ordenSaved);
		result.setItems(getItems(result.getId()));

		return result;

	}

	private OrdenCompraEntity toEntity(OrdenCompraDto ordenDto) {

		OrdenCompraEntity ordenEntity = new OrdenCompraEntity();
		if (ordenDto.getId() != null) {
			ordenEntity.setId(ordenDto.getId());
		}
		ordenEntity.setPuesto(puestoRepository.findOne(ordenDto.getPuesto().getId()));
		ordenEntity.setNumero(ordenDto.getNumero());
		ordenEntity.setProveedor(personaRepository.findOne(ordenDto.getProveedor().getId()));
		ordenEntity.setFechaCreacion(ordenDto.getFechaCreacion());
		ordenEntity.setFechaEntrega(ordenDto.getFechaEntrega());
		ordenEntity.setEstado(estadoRepository.findOne(ordenDto.getEstado().getId()));
		ordenEntity.setObservaciones(ordenDto.getObservaciones());

		return ordenEntity;
	}

	private OrdenCompraItemEntity toEntity(OrdenCompraItemDto itemDto) {
		OrdenCompraItemEntity itemEntity = new OrdenCompraItemEntity();
		itemEntity.setProducto(productoRepository.findOne(itemDto.getProducto().getId()));
		itemEntity.setCantidad(itemDto.getCantidad());
		itemEntity.setPrecio(itemDto.getPrecio());
		itemEntity.setEstado(estadoRepository.findOne(itemDto.getEstado().getId()));
		itemEntity.setPendientes(itemDto.getCantidad());

		return itemEntity;
	}

}
