package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.PaisDto;
import com.guilleavi.techsoftapp.entities.PaisEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.PaisRepository;

@Service
public class PaisService {

	@Autowired
	private PaisRepository paisRepository = null;

	@Transactional(readOnly = true)
	public PaisDto getPais(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de pais no puede ser nulo.");
		}

		PaisEntity paisEntity = paisRepository.findOne(id);
		
		return (paisEntity != null) ? new PaisDto(paisEntity) : null;
	}

	@Transactional(readOnly = true)
	public List<PaisDto> getPaises() {

		return paisRepository.findAll().stream().map(pais -> new PaisDto(pais)).collect(Collectors.toList());

	}

	@Transactional
	public PaisDto insert(PaisDto pais) throws ServiceException {

		if (pais == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El pais a insertar no se encuentra definido."));
		}

		if (pais.getId() != null && paisRepository.findOne(pais.getId()) != null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' del pais a insertar ya existe en la base de datos."));
		}

		return save(pais);
	}

	@Transactional
	public PaisDto update(PaisDto pais) throws ServiceException {

		if (pais == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El pais a modificar no se encuentra definido."));
		}

		if (pais.getId() == null || paisRepository.findOne(pais.getId()) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' del pais que desea modificar no existe en la base de datos."));
		}

		return save(pais);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de pais no puede ser nulo.");
		}

		if (paisRepository.findOne(id) == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El pais que se desea eliminar no existe.");
		}

		paisRepository.delete(paisRepository.findOne(id));
	}

	private PaisDto save(PaisDto pais) {

		PaisEntity paisEntity = new PaisEntity();
		paisEntity.setId(pais.getId());
		paisEntity.setNombre(pais.getNombre());

		PaisEntity result = paisRepository.save(paisEntity);
		
		return (result != null) ? new PaisDto(result) : null;

	}
}
