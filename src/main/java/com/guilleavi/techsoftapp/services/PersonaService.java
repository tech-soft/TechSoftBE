package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.CharMatcher;
import com.guilleavi.techsoftapp.dto.PersonaDto;
import com.guilleavi.techsoftapp.dto.PersonaResumenDto;
import com.guilleavi.techsoftapp.entities.PersonaEntity;
import com.guilleavi.techsoftapp.entities.QPersonaEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.DocumentoTipoRepository;
import com.guilleavi.techsoftapp.repositories.EstadoRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class PersonaService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private DocumentoTipoRepository documentoTipoRepository = null;
	@Autowired
	private EstadoRepository estadoRepository = null;
	@Autowired
	private PersonaRepository personaRepository = null;

	@Autowired
	private ContactoService contactoService = null;
	@Autowired
	private DomicilioService domicilioService = null;
	@Autowired
	private PersonaComercialService personaComercialService = null;
	@Autowired
	private PersonaRelacionadaService personaRelacionadaService = null;

	@Transactional(readOnly = true)
	public PersonaDto getPersona(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		PersonaEntity personaEntity = personaRepository.findOne(id);

		return (personaEntity != null) ? new PersonaDto(personaEntity) : null;

	}

	@Transactional(readOnly = true)
	public List<PersonaResumenDto> getPersonasByKeyword(String q) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QPersonaEntity qpersona = QPersonaEntity.personaEntity;

		return queryFactory.selectFrom(qpersona)
				.where(qpersona.nombre.contains(CharMatcher.is('\'').trimFrom(q).toUpperCase())).fetch().stream()
				.map(persona -> setDatosAdicionales(persona)).collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	public List<PersonaResumenDto> getPersonas() {

		return personaRepository.findAll().stream().map(persona -> new PersonaResumenDto(persona)).collect(Collectors.toList());
	}

	@Transactional
	public PersonaDto insert(PersonaDto persona) throws ServiceException {

		if (persona == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La persona a insertar no se encuentra definida."));
		}

		if (persona.getId() != null && personaRepository.findOne(persona.getId()) != null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' de la persona a insertar ya existe en la base de datos."));
		}

		validar(persona);

		return save(persona);
	}

	@Transactional
	public PersonaDto update(PersonaDto persona) throws ServiceException {

		if (persona == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La persona a modificar no se encuentra definida."));
		}

		if (persona.getId() == null || personaRepository.findOne(persona.getId()) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' de la persona que desea modificar no existe en la base de datos."));
		}

		validar(persona);

		return save(persona);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		personaComercialService.deleteByPersonaId(id);
		domicilioService.deleteByPersonaId(id);
		contactoService.deleteByPersonaId(id);
		personaRelacionadaService.deleteByPersonaId(id);

		personaRepository.delete(personaRepository.findOne(id));
	}

	private void validar(PersonaDto persona) {

		if (persona.getNombre() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, String.format("El campo nombre es obligatorio."));
		}

		if ((persona.getDocumentoTipo() != null && persona.getDocumentoNumero() == null)
				|| (persona.getDocumentoTipo() == null && persona.getDocumentoNumero() != null)) {
			throw new ServiceException(ServiceException.MISSING_DATA, String
					.format("Los campos tipo y número de documento deben ser ambos nulos o ambos estar definidos."));
		}
	}

	private PersonaDto save(PersonaDto persona) {

		PersonaEntity personaEntity = new PersonaEntity();
		if (persona.getId() != null) {
			personaEntity.setId(persona.getId());
		}
		personaEntity.setNombre(persona.getNombre());
		personaEntity.setDocumentoTipo((persona.getDocumentoTipo() != null)
				? documentoTipoRepository.findOne(persona.getDocumentoTipo().getId())
				: null);
		personaEntity.setDocumentoNumero(persona.getDocumentoNumero());
		personaEntity.setEstado(estadoRepository.findOne(persona.getEstado().getId()));
		personaEntity.setProveedor(persona.getProveedor());
		personaEntity.setCliente(persona.getCliente());

		PersonaEntity result = personaRepository.save(personaEntity);

		return (result != null) ? new PersonaDto(result) : null;

	}

	private PersonaResumenDto setDatosAdicionales(PersonaEntity persona) {

		PersonaResumenDto personaResumenDto = new PersonaResumenDto(persona);

		personaResumenDto.setDomicilioPrincipal(domicilioService.getDomicilioPrincipal(persona.getId()));
		personaResumenDto.setTelefonoPrincipal(contactoService.getTelefonoPrincipal(persona.getId()));

		return personaResumenDto;
	}

}
