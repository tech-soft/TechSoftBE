package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.PuestoDto;
import com.guilleavi.techsoftapp.entities.PuestoEntity;
import com.guilleavi.techsoftapp.entities.QPuestoEntity;
import com.guilleavi.techsoftapp.entities.QSucursalEntity;
import com.guilleavi.techsoftapp.enums.EstadoEnum;
import com.guilleavi.techsoftapp.repositories.PuestoRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class PuestoService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private PuestoRepository puestoRepository = null;

	@Transactional(readOnly = true)
	public List<PuestoDto> getPuestos() {

		return puestoRepository.findAll().stream().map(puesto -> new PuestoDto(puesto)).collect(Collectors.toList());

	}

	@Transactional(readOnly = true)
	public List<PuestoDto> getPuestosActivos() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QPuestoEntity qpuesto = QPuestoEntity.puestoEntity;

		return queryFactory.selectFrom(qpuesto).where(qpuesto.estado.id.eq(EstadoEnum.ACTIVO.getValor())).fetch()
				.stream().map(puesto -> new PuestoDto(puesto)).collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	public PuestoDto getPuestoBySucursal(Integer sucursalId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);

		QPuestoEntity qpuesto = QPuestoEntity.puestoEntity;
		QSucursalEntity qsucursal = QSucursalEntity.sucursalEntity;

		PuestoEntity puesto = queryFactory
				.select(qpuesto).from(qpuesto, qsucursal).where(qpuesto.id.eq(qsucursal.puesto.id)
						.and(qsucursal.id.eq(sucursalId)).and(qpuesto.estado.id.eq(EstadoEnum.ACTIVO.getValor())))
				.fetchFirst();

		return (puesto != null) ? new PuestoDto(puesto) : null;

	}
}
