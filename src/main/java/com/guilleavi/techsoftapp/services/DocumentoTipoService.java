package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.DocumentoTipoDto;
import com.guilleavi.techsoftapp.repositories.DocumentoTipoRepository;

@Service
public class DocumentoTipoService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private DocumentoTipoRepository documentoTipoRepository = null;

	@Transactional(readOnly = true)
	public List<DocumentoTipoDto> getDocumentoTipos() {

		return documentoTipoRepository.findAll().stream().map(documentoTipo -> new DocumentoTipoDto(documentoTipo))
				.collect(Collectors.toList());

	}
}
