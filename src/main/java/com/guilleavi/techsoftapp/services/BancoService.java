package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.BancoDto;
import com.guilleavi.techsoftapp.entities.BancoEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.BancoRepository;

@Service
public class BancoService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private BancoRepository bancoRepository = null;

	@Transactional(readOnly = true)
	public BancoDto getBanco(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de banco no puede ser nulo.");
		}

		BancoEntity bancoEntity = bancoRepository.findOne(id);

		return (bancoEntity != null) ? new BancoDto(bancoEntity) : null;

	}

	@Transactional(readOnly = true)
	public List<BancoDto> getBancos() {

		return bancoRepository.findAll().stream().map(banco -> new BancoDto(banco)).collect(Collectors.toList());

	}

	@Transactional
	public BancoDto insert(BancoDto banco) throws ServiceException {

		if (banco == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El banco a insertar no se encuentra definido."));
		}

		if (banco.getId() != null && bancoRepository.findOne(banco.getId()) != null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' del banco a insertar ya existe en la base de datos."));
		}

		return save(banco);
	}

	@Transactional
	public BancoDto update(BancoDto banco) throws ServiceException {

		if (banco == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El banco a modificar no se encuentra definido."));
		}

		if (banco.getId() == null || bancoRepository.findOne(banco.getId()) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' del banco que desea modificar no existe en la base de datos."));
		}

		return save(banco);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de banco no puede ser nulo.");
		}

		if (bancoRepository.findOne(id) == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El banco que se desea eliminar no existe.");
		}

		bancoRepository.delete(bancoRepository.findOne(id));
	}

	private BancoDto save(BancoDto banco) {

		BancoEntity bancoEntity = new BancoEntity();
		bancoEntity.setId(banco.getId());
		bancoEntity.setNombre(banco.getNombre());

		BancoEntity result = bancoRepository.save(bancoEntity);

		return (result != null) ? new BancoDto(result) : null;
	}
}
