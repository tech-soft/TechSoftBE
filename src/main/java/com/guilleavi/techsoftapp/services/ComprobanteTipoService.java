package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.ComprobanteTipoDto;
import com.guilleavi.techsoftapp.entities.ComprobanteTipoEntity;
import com.guilleavi.techsoftapp.entities.QComprobanteTipoEntity;
import com.guilleavi.techsoftapp.enums.ComprobanteTipoEnum;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.ComprobanteTipoRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class ComprobanteTipoService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private ComprobanteTipoRepository comprobanteTipoRepository = null;

	@Transactional(readOnly = true)
	public ComprobanteTipoDto getComprobanteTipo(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					"El 'id' del tipo de comprobante no puede ser nulo.");
		}

		ComprobanteTipoEntity comprobanteTipoEntity = comprobanteTipoRepository.findOne(id);

		return (comprobanteTipoEntity != null) ? new ComprobanteTipoDto(comprobanteTipoEntity) : null;
	}

	@Transactional(readOnly = true)
	public List<ComprobanteTipoDto> getComprobanteTipos() {

		return comprobanteTipoRepository.findAll().stream()
				.map(comprobanteTipo -> new ComprobanteTipoDto(comprobanteTipo)).collect(Collectors.toList());

	}

	@Transactional(readOnly = true)
	public List<ComprobanteTipoDto> getRemitoTipos() {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QComprobanteTipoEntity qcomprobanteTipo = QComprobanteTipoEntity.comprobanteTipoEntity;

		return queryFactory.selectFrom(qcomprobanteTipo)
				.where(qcomprobanteTipo.tipo.eq(ComprobanteTipoEnum.REMITO.getValor())).fetch().stream()
				.map(tipo -> new ComprobanteTipoDto(tipo)).collect(Collectors.toList());
	}

	@Transactional
	public ComprobanteTipoDto insert(ComprobanteTipoDto comprobanteTipo) throws ServiceException {

		if (comprobanteTipo == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El tipo de comprobante a insertar no se encuentra definido."));
		}

		if (comprobanteTipo.getId() != null && comprobanteTipoRepository.findOne(comprobanteTipo.getId()) != null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' del tipo de comprobante a insertar ya existe en la base de datos."));
		}

		validarComprobanteTipo(comprobanteTipo);
		
		return save(comprobanteTipo);
	}

	@Transactional
	public ComprobanteTipoDto update(ComprobanteTipoDto comprobanteTipo) throws ServiceException {

		if (comprobanteTipo == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El tipo de comprobante a modificar no se encuentra definido."));
		}

		if (comprobanteTipo.getId() == null || comprobanteTipoRepository.findOne(comprobanteTipo.getId()) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND, String
					.format("El 'id' del tipo de comprobante que desea modificar no existe en la base de datos."));
		}

		validarComprobanteTipo(comprobanteTipo);
		
		return save(comprobanteTipo);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					"El 'id' de tipo de comprobante no puede ser nulo.");
		}

		if (comprobanteTipoRepository.findOne(id) == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					"El tipo de comprobante que se desea eliminar no existe.");
		}

		comprobanteTipoRepository.delete(comprobanteTipoRepository.findOne(id));
	}

	private void validarComprobanteTipo(ComprobanteTipoDto comprobanteTipo) throws ServiceException {

		if (comprobanteTipo.getLetra() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, String.format("El campo letra es obligatorio."));
		}

		if (comprobanteTipo.getTipo() == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, String.format("El campo tipo es obligatorio."));
		}
	}

	private ComprobanteTipoDto save(ComprobanteTipoDto comprobanteTipo) {

		ComprobanteTipoEntity result = comprobanteTipoRepository.save(toEntity(comprobanteTipo));

		return (result != null) ? new ComprobanteTipoDto(result) : null;

	}

	private ComprobanteTipoEntity toEntity(ComprobanteTipoDto comprobanteTipo) {
		ComprobanteTipoEntity entity = new ComprobanteTipoEntity();
		entity.setId(comprobanteTipo.getId());
		entity.setLetra(comprobanteTipo.getLetra());
		entity.setTipo(comprobanteTipo.getTipo());

		return entity;
	}
}
