package com.guilleavi.techsoftapp.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.PersonaComercialDto;
import com.guilleavi.techsoftapp.entities.PersonaComercialEntity;
import com.guilleavi.techsoftapp.entities.QPersonaComercialEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.IvaTipoRepository;
import com.guilleavi.techsoftapp.repositories.PagoFormaRepository;
import com.guilleavi.techsoftapp.repositories.PersonaComercialRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;
import com.guilleavi.techsoftapp.repositories.PrecioListaRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class PersonaComercialService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private IvaTipoRepository ivaTipoRepository = null;
	@Autowired
	private PagoFormaRepository pagoFormaRepository = null;
	@Autowired
	private PersonaRepository personaRepository = null;
	@Autowired
	private PersonaComercialRepository personaComercialRepository = null;
	@Autowired
	private PrecioListaRepository precioListaRepository = null;

	@Transactional(readOnly = true)
	public PersonaComercialDto getDatosComercialesByPersona(Integer personaId) throws ServiceException {

		if (personaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QPersonaComercialEntity qpersonacomercial = QPersonaComercialEntity.personaComercialEntity;

		PersonaComercialEntity personaComercial = queryFactory.selectFrom(qpersonacomercial)
				.where(qpersonacomercial.persona.id.eq(personaId)).fetchOne();

		return (personaComercial == null) ? null : new PersonaComercialDto(personaComercial);

	}

	@Transactional
	public PersonaComercialDto upsert(PersonaComercialDto personaComercial) throws ServiceException {

		if (personaComercial == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("Los datos comerciales a modificar no se encuentra definido."));
		}

		return save(personaComercial);
	}

	@Transactional
	public void deleteByPersonaId(Integer personaId) throws ServiceException {

		if (personaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		PersonaComercialDto personaComercial = getDatosComercialesByPersona(personaId);

		if (personaComercial != null) {
			personaComercialRepository.delete(personaComercial.getId());
		}

	}

	private PersonaComercialDto save(PersonaComercialDto personaComercial) {

		PersonaComercialEntity personaComercialEntity = new PersonaComercialEntity();
		if (personaComercial.getId() != null) {
			personaComercialEntity.setId(personaComercial.getId());
		}
		personaComercialEntity.setPersona(personaRepository.findOne(personaComercial.getPersonaId()));
		personaComercialEntity.setIvaTipo((personaComercial.getIvaTipo() != null)
				? ivaTipoRepository.findOne(personaComercial.getIvaTipo().getId())
				: null);
		personaComercialEntity.setPrecioLista((personaComercial.getPrecioLista() != null)
				? precioListaRepository.findOne(personaComercial.getPrecioLista().getId())
				: null);
		personaComercialEntity.setPagoForma((personaComercial.getPagoForma() != null)
				? pagoFormaRepository.findOne(personaComercial.getPagoForma().getId())
				: null);
		personaComercialEntity.setDescuento((personaComercial.getDescuento() != null) ? personaComercial.getDescuento() : 0);

		PersonaComercialEntity result = personaComercialRepository.save(personaComercialEntity);

		return (result != null) ? new PersonaComercialDto(result) : null;
	}

}
