package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.PrecioListaDto;
import com.guilleavi.techsoftapp.repositories.PrecioListaRepository;

@Service
public class PrecioListaService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private PrecioListaRepository precioListaRepository = null;

	@Transactional(readOnly = true)
	public List<PrecioListaDto> getPrecioListas() {

		return precioListaRepository.findAll().stream().map(precioLista -> new PrecioListaDto(precioLista))
				.collect(Collectors.toList());
	}
}