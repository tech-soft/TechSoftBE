package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.MonedaDto;
import com.guilleavi.techsoftapp.repositories.MonedaRepository;

@Service
public class MonedaService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private MonedaRepository monedaRepository = null;

	@Transactional(readOnly = true)
	public List<MonedaDto> getMonedas() {

		return monedaRepository.findAll().stream().map(moneda -> new MonedaDto(moneda)).collect(Collectors.toList());

	}
}
