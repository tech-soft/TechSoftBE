package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.OrigenDto;
import com.guilleavi.techsoftapp.repositories.OrigenRepository;

@Service
public class OrigenService {

	@Autowired
	private OrigenRepository origenRepository = null;

	@Transactional(readOnly = true)
	public List<OrigenDto> getOrigenes() {

		return origenRepository.findAll().stream().map(origen -> new OrigenDto(origen)).collect(Collectors.toList());

	}
}
