package com.guilleavi.techsoftapp.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.guilleavi.techsoftapp.entities.ProductoStockEntity;
import com.guilleavi.techsoftapp.entities.QProductoStockEntity;
import com.guilleavi.techsoftapp.repositories.ProductoRepository;
import com.guilleavi.techsoftapp.repositories.ProductoStockRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class ProductoStockService {

	@PersistenceContext
	protected EntityManager em = null;
	
	@Autowired
	ProductoRepository productoRepositoty = null;
	@Autowired
	ProductoStockRepository productoStockRepositoty = null;

	public void editStockEntrante(Integer productoId, Integer cantidad, Boolean sumar) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;
		
		ProductoStockEntity stock = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();
				
		if (stock == null) {
			stock = new ProductoStockEntity();
			stock.setProducto(productoRepositoty.findOne(productoId));
			stock.setStockEntrante(0);
			stock.setStockDisponible(0);
			stock.setStockTransito(0);
			stock.setStockReservado(0);
			stock.setStockVendido(0);
		}
		
		Integer stockActual = stock.getStockEntrante() != null ? stock.getStockEntrante() : 0;

		if (sumar) {
			stock.setStockEntrante(stockActual + cantidad);
		} else {
			stock.setStockEntrante(stockActual - cantidad);
		}

		productoStockRepositoty.save(stock);
	}

	public void editStockDisponible(Integer productoId, Integer cantidad, Boolean sumar) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;
		
		ProductoStockEntity stock = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();
		
		if (stock == null) {
			stock = new ProductoStockEntity();
			stock.setProducto(productoRepositoty.findOne(productoId));
			stock.setStockEntrante(0);
			stock.setStockDisponible(0);
			stock.setStockTransito(0);
			stock.setStockReservado(0);
			stock.setStockVendido(0);
		}
		
		Integer stockActual = stock.getStockDisponible() != null ? stock.getStockDisponible() : 0;

		if (sumar) {
			stock.setStockDisponible(stockActual + cantidad);
		} else {
			stock.setStockDisponible(stockActual - cantidad);
		}

		productoStockRepositoty.save(stock);

	}

	public void editStockTransito(Integer productoId, Integer cantidad, Boolean sumar) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;
		
		ProductoStockEntity stock = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();
		
		if (stock == null) {
			stock = new ProductoStockEntity();
			stock.setProducto(productoRepositoty.findOne(productoId));
			stock.setStockEntrante(0);
			stock.setStockDisponible(0);
			stock.setStockTransito(0);
			stock.setStockReservado(0);
			stock.setStockVendido(0);
		}
		
		Integer stockActual = stock.getStockTransito() != null ? stock.getStockTransito() : 0;

		if (sumar) {
			stock.setStockTransito(stockActual + cantidad);
		} else {
			stock.setStockTransito(stockActual - cantidad);
		}

		productoStockRepositoty.save(stock);

	}

	public void editStockReservado(Integer productoId, Integer cantidad, Boolean sumar) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;
		
		ProductoStockEntity stock = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();
		
		if (stock == null) {
			stock = new ProductoStockEntity();
			stock.setProducto(productoRepositoty.findOne(productoId));
			stock.setStockEntrante(0);
			stock.setStockDisponible(0);
			stock.setStockTransito(0);
			stock.setStockReservado(0);
			stock.setStockVendido(0);
		}
		
		Integer stockActual = stock.getStockReservado() != null ? stock.getStockReservado() : 0;

		if (sumar) {
			stock.setStockReservado(stockActual + cantidad);
		} else {
			stock.setStockReservado(stockActual - cantidad);
		}

		productoStockRepositoty.save(stock);

	}

	public void editStockVendido(Integer productoId, Integer cantidad, Boolean sumar) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QProductoStockEntity qstock = QProductoStockEntity.productoStockEntity;
		
		ProductoStockEntity stock = queryFactory.selectFrom(qstock).where(qstock.producto.id.eq(productoId))
				.fetchOne();
		
		if (stock == null) {
			stock = new ProductoStockEntity();
			stock.setProducto(productoRepositoty.findOne(productoId));
			stock.setStockEntrante(0);
			stock.setStockDisponible(0);
			stock.setStockTransito(0);
			stock.setStockReservado(0);
			stock.setStockVendido(0);
		}
		
		Integer stockActual = stock.getStockVendido() != null ? stock.getStockVendido() : 0;

		if (sumar) {
			stock.setStockVendido(stockActual + cantidad);
		} else {
			stock.setStockVendido(stockActual - cantidad);
		}

		productoStockRepositoty.save(stock);

	}
}
