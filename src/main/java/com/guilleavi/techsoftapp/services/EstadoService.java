package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.EstadoDto;
import com.guilleavi.techsoftapp.entities.QEstadoEntity;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class EstadoService {

	@PersistenceContext
	protected EntityManager em = null;

	@Transactional(readOnly = true)
	public List<EstadoDto> getEstados() {

		final int ordenStart = 1;
		final int ordenEnd = 999;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QEstadoEntity qestado = QEstadoEntity.estadoEntity;

		return queryFactory.selectFrom(qestado)
				.where(qestado.id.between(ordenStart, ordenEnd)).fetch().stream()
				.map(estado -> new EstadoDto(estado)).collect(Collectors.toList());

	}

	@Transactional(readOnly = true)
	public List<EstadoDto> getOrdenesEstados() {

		final int ordenStart = 1000;
		final int ordenEnd = 1099;

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QEstadoEntity qestado = QEstadoEntity.estadoEntity;

		return queryFactory.selectFrom(qestado)
				.where(qestado.id.between(ordenStart, ordenEnd)).fetch().stream()
				.map(estado -> new EstadoDto(estado)).collect(Collectors.toList());
	}
}
