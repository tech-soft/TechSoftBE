package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.DomicilioDto;
import com.guilleavi.techsoftapp.entities.DomicilioEntity;
import com.guilleavi.techsoftapp.entities.QDomicilioEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.CiudadRepository;
import com.guilleavi.techsoftapp.repositories.DomicilioRepository;
import com.guilleavi.techsoftapp.repositories.PersonaRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class DomicilioService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private CiudadRepository ciudadRepository = null;
	@Autowired
	private DomicilioRepository domicilioRepository = null;
	@Autowired
	private PersonaRepository personaRepository = null;

	@Transactional(readOnly = true)
	public DomicilioDto getDomicilio(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de domicilio no puede ser nulo.");
		}

		DomicilioEntity domicilioEntity = domicilioRepository.findOne(id);

		return (domicilioEntity != null) ? new DomicilioDto(domicilioEntity) : null;
	}

	@Transactional(readOnly = true)
	public List<DomicilioDto> getDomiciliosByPersona(Integer personaId) throws ServiceException {

		if (personaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QDomicilioEntity qdomicilio = QDomicilioEntity.domicilioEntity;

		return queryFactory.selectFrom(qdomicilio).where(qdomicilio.persona.id.eq(personaId)).fetch().stream()
				.map(domicilio -> new DomicilioDto(domicilio)).collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	public String getDomicilioPrincipal(Integer personaId) {

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QDomicilioEntity qdomicilio = QDomicilioEntity.domicilioEntity;

		DomicilioEntity domicilio = queryFactory.selectFrom(qdomicilio)
				.where(qdomicilio.persona.id.eq(personaId).and(qdomicilio.principal.eq(true))).fetchOne();

		if (domicilio == null) {
			return "";
		}

		StringJoiner joiner = new StringJoiner(" ");
		
		if (domicilio.getCalle() != null && domicilio.getCalle().length() > 0) {
			joiner.add(domicilio.getCalle());
		}
		
		if (domicilio.getNro() != null && domicilio.getNro().length() > 0) {
			joiner.add(domicilio.getNro());
		}
		
		if (domicilio.getPiso() != null && domicilio.getPiso().length() > 0) {
			joiner.add(domicilio.getPiso());
		}
		
		if (domicilio.getDpto() != null && domicilio.getDpto().length() > 0) {
			joiner.add(domicilio.getDpto());
		}
		
		if (domicilio.getCiudad().getNombre() != null && domicilio.getCiudad().getNombre().length() > 0) {
			joiner.add(domicilio.getCiudad().getNombre());
		}
		
		return joiner.toString();

	}

	@Transactional
	public DomicilioDto upsert(DomicilioDto domicilio) throws ServiceException {
// TODO: no permitir dos domicilios principales
		if (domicilio == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El domicilio a insertar no se encuentra definido."));
		}

		return save(domicilio);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de domicilio no puede ser nulo.");
		}

		domicilioRepository.delete(domicilioRepository.findOne(id));
	}

	@Transactional
	public void deleteByPersonaId(Integer personaId) throws ServiceException {

		if (personaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		getDomiciliosByPersona(personaId).forEach(domicilio -> domicilioRepository.delete(domicilio.getId()));

	}

	private DomicilioDto save(DomicilioDto domicilio) {

		DomicilioEntity domicilioEntity = new DomicilioEntity();
		if (domicilio.getId() != null) {
			domicilioEntity.setId(domicilio.getId());
		}
		domicilioEntity.setCalle(domicilio.getCalle());
		domicilioEntity.setNro(domicilio.getNro());
		domicilioEntity.setPiso(domicilio.getPiso());
		domicilioEntity.setDpto(domicilio.getDpto());
		domicilioEntity.setCiudad(ciudadRepository.findOne(domicilio.getCiudad().getId()));
		domicilioEntity.setDescripcion(domicilio.getDescripcion());
		domicilioEntity.setPrincipal(domicilio.getPrincipal());
		domicilioEntity.setPersona(personaRepository.findOne(domicilio.getPersonaId()));

		DomicilioEntity result = domicilioRepository.save(domicilioEntity);
		
		return (result != null) ? new DomicilioDto(result) : null;

	}
}
