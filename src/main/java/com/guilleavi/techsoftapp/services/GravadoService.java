package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.GravadoDto;
import com.guilleavi.techsoftapp.repositories.GravadoRepository;

@Service
public class GravadoService {

	@Autowired
	private GravadoRepository gravadoRepository = null;

	@Transactional(readOnly = true)
	public List<GravadoDto> getGravados() {

		return gravadoRepository.findAll().stream().map(gravado -> new GravadoDto(gravado)).collect(Collectors.toList());

	}
}
