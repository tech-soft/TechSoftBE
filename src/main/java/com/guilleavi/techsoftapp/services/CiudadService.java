package com.guilleavi.techsoftapp.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.techsoftapp.dto.CiudadDto;
import com.guilleavi.techsoftapp.entities.CiudadEntity;
import com.guilleavi.techsoftapp.entities.QCiudadEntity;
import com.guilleavi.techsoftapp.exceptions.ServiceException;
import com.guilleavi.techsoftapp.repositories.CiudadRepository;
import com.guilleavi.techsoftapp.repositories.ProvinciaRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class CiudadService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private CiudadRepository ciudadRepository = null;
	@Autowired
	private ProvinciaRepository provinciaRepository = null;

	@Transactional(readOnly = true)
	public CiudadDto getCiudad(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de ciudad no puede ser nulo.");
		}

		CiudadEntity ciudadEntity = ciudadRepository.findOne(id);

		return (ciudadEntity != null) ? new CiudadDto(ciudadEntity) : null;

	}

	@Transactional(readOnly = true)
	public List<CiudadDto> getCiudadesByProvincia(Integer provinciaId) throws ServiceException {

		if (provinciaId == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de provincia no puede ser nulo.");
		}

		JPAQueryFactory queryFactory = new JPAQueryFactory(em);
		QCiudadEntity qciudad = QCiudadEntity.ciudadEntity;

		return queryFactory.selectFrom(qciudad).where(qciudad.provincia.id.eq(provinciaId)).fetch().stream()
				.map(ciudad -> new CiudadDto(ciudad)).collect(Collectors.toList());

	}

	@Transactional(readOnly = true)
	public List<CiudadDto> getCiudades() {

		return ciudadRepository.findAll().stream().map(ciudad -> new CiudadDto(ciudad)).collect(Collectors.toList());

	}

	@Transactional
	public CiudadDto insert(CiudadDto ciudad) throws ServiceException {

		if (ciudad == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La ciudad a insertar no se encuentra definida."));
		}

		if (ciudad.getId() != null && ciudadRepository.findOne(ciudad.getId()) != null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' de la ciudad a insertar ya existe en la base de datos."));
		}

		return save(ciudad);
	}

	@Transactional
	public CiudadDto update(CiudadDto ciudad) throws ServiceException {

		if (ciudad == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("La ciudad a modificar no se encuentra definida."));
		}

		if (ciudad.getId() == null || ciudadRepository.findOne(ciudad.getId()) == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("El 'id' de la ciudad que desea modificar no existe en la base de datos."));
		}

		return save(ciudad);
	}

	@Transactional
	public void delete(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de ciudad no puede ser nulo.");
		}

		if (ciudadRepository.findOne(id) == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "La ciudad que se desea eliminar no existe.");
		}

		ciudadRepository.delete(ciudadRepository.findOne(id));
	}

	private CiudadDto save(CiudadDto ciudad) {

		CiudadEntity ciudadEntity = new CiudadEntity();
		ciudadEntity.setId(ciudad.getId());
		ciudadEntity.setNombre(ciudad.getNombre());
		ciudadEntity.setCodigoPostal(ciudad.getCodigoPostal());
		ciudadEntity.setProvincia(provinciaRepository.findOne(ciudad.getProvincia().getId()));

		CiudadEntity result = ciudadRepository.save(ciudadEntity);
		
		return (result != null) ? new CiudadDto(result) : null;

	}
}
