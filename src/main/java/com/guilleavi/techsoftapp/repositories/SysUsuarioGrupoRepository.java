package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.SysUsuarioGrupoEntity;

public interface SysUsuarioGrupoRepository
		extends JpaRepository<SysUsuarioGrupoEntity, Integer>, QueryDslPredicateExecutor<SysUsuarioGrupoEntity> {

}
