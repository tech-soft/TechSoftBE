package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.ProductoEntity;

public interface ProductoRepository
		extends JpaRepository<ProductoEntity, Integer>, QueryDslPredicateExecutor<ProductoEntity> {

}
