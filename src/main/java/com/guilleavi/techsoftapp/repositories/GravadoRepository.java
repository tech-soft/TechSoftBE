package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.GravadoEntity;

public interface GravadoRepository
		extends JpaRepository<GravadoEntity, Integer>, QueryDslPredicateExecutor<GravadoEntity> {

}