package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.UsuarioGrupoEntity;

public interface UsuarioGrupoRepository
		extends JpaRepository<UsuarioGrupoEntity, Integer>, QueryDslPredicateExecutor<UsuarioGrupoEntity> {

}
