package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.RemitoProveedorEntity;

public interface RemitoProveedorRepository
		extends JpaRepository<RemitoProveedorEntity, Integer>, QueryDslPredicateExecutor<RemitoProveedorEntity> {
}
