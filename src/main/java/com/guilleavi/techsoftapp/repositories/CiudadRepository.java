package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.CiudadEntity;

public interface CiudadRepository
		extends JpaRepository<CiudadEntity, Integer>, QueryDslPredicateExecutor<CiudadEntity> {

}
