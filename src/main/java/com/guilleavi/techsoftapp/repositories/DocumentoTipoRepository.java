package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.DocumentoTipoEntity;

public interface DocumentoTipoRepository
		extends JpaRepository<DocumentoTipoEntity, Integer>, QueryDslPredicateExecutor<DocumentoTipoEntity> {

}
