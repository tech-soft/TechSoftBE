package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.OrigenEntity;

public interface OrigenRepository
		extends JpaRepository<OrigenEntity, Integer>, QueryDslPredicateExecutor<OrigenEntity> {

}