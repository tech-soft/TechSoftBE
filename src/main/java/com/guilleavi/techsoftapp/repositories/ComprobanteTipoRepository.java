package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.ComprobanteTipoEntity;

public interface ComprobanteTipoRepository
		extends JpaRepository<ComprobanteTipoEntity, Integer>, QueryDslPredicateExecutor<ComprobanteTipoEntity> {
}
