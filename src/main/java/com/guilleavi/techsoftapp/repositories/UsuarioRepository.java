package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.UsuarioEntity;

public interface UsuarioRepository
		extends JpaRepository<UsuarioEntity, Integer>, QueryDslPredicateExecutor<UsuarioEntity> {

}