package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.FacturaProveedorEntity;

public interface FacturaProveedorRepository
		extends JpaRepository<FacturaProveedorEntity, Integer>, QueryDslPredicateExecutor<FacturaProveedorEntity> {
}
