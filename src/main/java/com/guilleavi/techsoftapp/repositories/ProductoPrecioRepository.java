package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.ProductoPrecioEntity;

public interface ProductoPrecioRepository
		extends JpaRepository<ProductoPrecioEntity, Integer>, QueryDslPredicateExecutor<ProductoPrecioEntity> {

}
