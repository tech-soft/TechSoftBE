package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.PersonaComercialEntity;

public interface PersonaComercialRepository
		extends JpaRepository<PersonaComercialEntity, Integer>, QueryDslPredicateExecutor<PersonaComercialEntity> {

}
