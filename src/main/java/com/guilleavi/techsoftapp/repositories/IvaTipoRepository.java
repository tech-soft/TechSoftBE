package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.IvaTipoEntity;

public interface IvaTipoRepository
		extends JpaRepository<IvaTipoEntity, Integer>, QueryDslPredicateExecutor<IvaTipoEntity> {

}
