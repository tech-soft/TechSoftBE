package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.ProvinciaEntity;

public interface ProvinciaRepository
		extends JpaRepository<ProvinciaEntity, Integer>, QueryDslPredicateExecutor<ProvinciaEntity> {

}
