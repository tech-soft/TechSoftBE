package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.PagoChequeEntity;

public interface PagoChequeRepository
		extends JpaRepository<PagoChequeEntity, Integer>, QueryDslPredicateExecutor<PagoChequeEntity> {
}
