package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.DomicilioEntity;

public interface DomicilioRepository
		extends JpaRepository<DomicilioEntity, Integer>, QueryDslPredicateExecutor<DomicilioEntity> {

}
