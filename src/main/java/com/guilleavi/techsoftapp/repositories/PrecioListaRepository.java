package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.PrecioListaEntity;

public interface PrecioListaRepository
		extends JpaRepository<PrecioListaEntity, Integer>, QueryDslPredicateExecutor<PrecioListaEntity> {

}
