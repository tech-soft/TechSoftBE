package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.SucursalEntity;

public interface SucursalRepository
		extends JpaRepository<SucursalEntity, Integer>, QueryDslPredicateExecutor<SucursalEntity> {

}
