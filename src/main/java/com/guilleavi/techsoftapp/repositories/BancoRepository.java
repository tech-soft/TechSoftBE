package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.BancoEntity;

public interface BancoRepository extends JpaRepository<BancoEntity, Integer>, QueryDslPredicateExecutor<BancoEntity> {

}
