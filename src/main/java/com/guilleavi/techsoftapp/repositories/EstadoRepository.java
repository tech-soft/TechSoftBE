package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.EstadoEntity;

public interface EstadoRepository
		extends JpaRepository<EstadoEntity, Integer>, QueryDslPredicateExecutor<EstadoEntity> {

}
