package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.PersonaRelacionTipoEntity;

public interface PersonaRelacionTipoRepository extends JpaRepository<PersonaRelacionTipoEntity, Integer>,
		QueryDslPredicateExecutor<PersonaRelacionTipoEntity> {

}
