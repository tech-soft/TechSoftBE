package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.OrdenCompraEntity;

public interface OrdenCompraRepository
		extends JpaRepository<OrdenCompraEntity, Integer>, QueryDslPredicateExecutor<OrdenCompraEntity> {

}
