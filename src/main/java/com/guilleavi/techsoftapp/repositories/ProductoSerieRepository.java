package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.ProductoSerieEntity;

public interface ProductoSerieRepository
		extends JpaRepository<ProductoSerieEntity, Integer>, QueryDslPredicateExecutor<ProductoSerieEntity> {
}
