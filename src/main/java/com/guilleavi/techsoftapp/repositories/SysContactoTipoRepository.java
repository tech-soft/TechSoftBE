package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.SysContactoTipoEntity;

public interface SysContactoTipoRepository
		extends JpaRepository<SysContactoTipoEntity, Integer>, QueryDslPredicateExecutor<SysContactoTipoEntity> {
}
