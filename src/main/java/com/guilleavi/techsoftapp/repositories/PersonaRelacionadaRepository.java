package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.PersonaRelacionadaEntity;

public interface PersonaRelacionadaRepository
		extends JpaRepository<PersonaRelacionadaEntity, Integer>, QueryDslPredicateExecutor<PersonaRelacionadaEntity> {

}
