package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.FacturaProveedorItemEntity;

public interface FacturaProveedorItemRepository extends JpaRepository<FacturaProveedorItemEntity, Integer>,
		QueryDslPredicateExecutor<FacturaProveedorItemEntity> {

}
