package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.PagoEntity;

public interface PagoRepository extends JpaRepository<PagoEntity, Integer>, QueryDslPredicateExecutor<PagoEntity> {
}
