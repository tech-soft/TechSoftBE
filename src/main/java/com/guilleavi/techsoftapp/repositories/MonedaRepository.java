package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.MonedaEntity;

public interface MonedaRepository
		extends JpaRepository<MonedaEntity, Integer>, QueryDslPredicateExecutor<MonedaEntity> {
}
