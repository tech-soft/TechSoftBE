package com.guilleavi.techsoftapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.techsoftapp.entities.ProductoStockEntity;

public interface ProductoStockRepository
		extends JpaRepository<ProductoStockEntity, Integer>, QueryDslPredicateExecutor<ProductoStockEntity> {
}
