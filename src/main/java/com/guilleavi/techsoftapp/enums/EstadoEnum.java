package com.guilleavi.techsoftapp.enums;

public enum EstadoEnum {
	ACTIVO(1), NUEVA(1000), EMITIDA(1001), RECIBIDA(1002), ITEM_NUEVO(1100), ITEM_PEDIDO(1101), ITEM_RECIBIDO(1102);

	private final Integer valor;

	private EstadoEnum(Integer valor) {
		this.valor = valor;
	}

	public Integer getValor() {
		return valor;
	}

}
