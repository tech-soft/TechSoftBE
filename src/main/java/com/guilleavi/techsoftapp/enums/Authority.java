package com.guilleavi.techsoftapp.enums;

public enum Authority {
	ADMIN, USER
}
