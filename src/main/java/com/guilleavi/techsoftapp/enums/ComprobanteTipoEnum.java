package com.guilleavi.techsoftapp.enums;

public enum ComprobanteTipoEnum {
	REMITO(1), FACTURA(2);

	private final Integer valor;

	private ComprobanteTipoEnum(Integer valor) {
		this.valor = valor;
	}

	public Integer getValor() {
		return valor;
	}

}

