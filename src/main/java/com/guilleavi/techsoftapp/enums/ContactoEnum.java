package com.guilleavi.techsoftapp.enums;

public enum ContactoEnum {
	TELEFONO(1), EMAIL(2);

	private final Integer valor;

	private ContactoEnum(Integer valor) {
		this.valor = valor;
	}

	public Integer getValor() {
		return valor;
	}

}
