package com.guilleavi.techsoftapp.enums;

public enum ListaPrecioEnum {
	LISTA_1(1), LISTA_2(2), LISTA_3(3), LISTA_4(4), LISTA_5(5), LISTA_6(6);

	private final Integer valor;

	private ListaPrecioEnum(Integer valor) {
		this.valor = valor;
	}

	public Integer getValor() {
		return valor;
	}

}
