package com.guilleavi.techsoftapp.converters;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {
	@Override
	public java.sql.Date convertToDatabaseColumn(LocalDate entityValue) {
		if (entityValue != null) {
			return java.sql.Date.valueOf(entityValue);
		} else {
			return null;
		}
	}

	@Override
	public LocalDate convertToEntityAttribute(java.sql.Date databaseValue) {
		if (databaseValue != null) {
			return databaseValue.toLocalDate();
		} else {
			return null;
		}
	}
}
