package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.EstadoEntity;

public class EstadoDto {

	private Integer id = null;

	private String descripcion = null;

	public EstadoDto() {
		super();
	}

	public EstadoDto(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public EstadoDto(EstadoEntity estado) {
		super();
		this.id = estado.getId();
		this.descripcion = estado.getDescripcion();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstadoDto other = (EstadoDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
