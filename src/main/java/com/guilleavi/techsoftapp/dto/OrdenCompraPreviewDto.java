package com.guilleavi.techsoftapp.dto;

import java.time.LocalDate;

import com.guilleavi.techsoftapp.entities.OrdenCompraEntity;

public class OrdenCompraPreviewDto {

	private Integer id = null;

	private Integer puesto = null;

	private Long numero = null;

	private ProveedorDto proveedor = null;

	private LocalDate fechaCreacion = null;

	private EstadoDto estado = null;

	public OrdenCompraPreviewDto() {
		super();
	}

	public OrdenCompraPreviewDto(Integer id, Integer puesto, Long numero, ProveedorDto proveedor,
			LocalDate fechaCreacion, EstadoDto estado) {
		super();
		this.id = id;
		this.puesto = puesto;
		this.numero = numero;
		this.proveedor = proveedor;
		this.fechaCreacion = fechaCreacion;
		this.estado = estado;
	}

	public OrdenCompraPreviewDto(OrdenCompraEntity orden) {
		super();
		this.id = orden.getId();
		this.puesto = orden.getPuesto().getCodigo();
		this.numero = orden.getNumero();
		this.proveedor = (orden != null && orden.getProveedor() != null) ? new ProveedorDto(orden.getProveedor())
				: null;
		this.fechaCreacion = orden.getFechaCreacion();
		this.estado = (orden != null && orden.getEstado() != null) ? new EstadoDto(orden.getEstado()) : null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPuesto() {
		return puesto;
	}

	public void setPuesto(Integer puesto) {
		this.puesto = puesto;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public ProveedorDto getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorDto proveedor) {
		this.proveedor = proveedor;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public EstadoDto getEstado() {
		return estado;
	}

	public void setEstado(EstadoDto estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdenCompraPreviewDto other = (OrdenCompraPreviewDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
