package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.PuestoEntity;

public class PuestoDto {

	private Integer id = null;

	private Integer codigo = null;

	private String codigoDescripcion = null;

	public PuestoDto() {
		super();
	}

	public PuestoDto(Integer id, Integer codigo, String codigoString) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.codigoDescripcion = codigoString;
	}

	public PuestoDto(PuestoEntity puesto) {
		super();
		this.id = puesto.getId();
		this.codigo = puesto.getCodigo();
		this.codigoDescripcion = puesto.getCodigoString();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getCodigoDescripcion() {
		return codigoDescripcion;
	}

	public void setCodigoDescripcion(String codigoString) {
		this.codigoDescripcion = codigoString;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PuestoDto other = (PuestoDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
