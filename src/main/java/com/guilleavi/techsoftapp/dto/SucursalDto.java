package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.SucursalEntity;

public class SucursalDto {

	private Integer id = null;

	private String descripcion = null;

	private Integer puestoId = null;

	public SucursalDto() {
		super();
	}

	public SucursalDto(Integer id, String descripcion, Integer puestoId) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.puestoId = puestoId;
	}

	public SucursalDto(SucursalEntity sucursal) {
		super();
		this.id = sucursal.getId();
		this.descripcion = sucursal.getDescripcion();
		this.puestoId = sucursal.getPuesto().getId();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getPuestoId() {
		return puestoId;
	}

	public void setPuestoId(Integer puestoId) {
		this.puestoId = puestoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SucursalDto other = (SucursalDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
