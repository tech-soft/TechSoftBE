package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.DocumentoTipoEntity;

public class DocumentoTipoDto {

	private Integer id = null;

	private String descripcion = null;

	private String descripcionReducida = null;

	public DocumentoTipoDto() {
		super();
	}

	public DocumentoTipoDto(Integer id, String descripcion, String descripcionReducida) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.descripcionReducida = descripcionReducida;
	}

	public DocumentoTipoDto(DocumentoTipoEntity documentoTipo) {
		super();
		this.id = documentoTipo.getId();
		this.descripcion = documentoTipo.getDescripcion();
		this.descripcionReducida = documentoTipo.getDescripcionReducida();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionReducida() {
		return descripcionReducida;
	}

	public void setDescripcionReducida(String descripcionReducida) {
		this.descripcionReducida = descripcionReducida;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentoTipoDto other = (DocumentoTipoDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
