package com.guilleavi.techsoftapp.dto;

public class AuthenticationTokenDto {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
