package com.guilleavi.techsoftapp.dto;

import java.time.LocalDate;

import com.guilleavi.techsoftapp.entities.RemitoProveedorEntity;

public class RemitoProveedorPreviewDto {

	private Integer id = null;

	private Integer puestoId = null;

	private Long numero = null;

	private ProveedorDto proveedor = null;

	private LocalDate fecha = null;

	public RemitoProveedorPreviewDto(Integer id, Integer puestoId, Long numero, ProveedorDto proveedor,
			LocalDate fecha) {
		super();
		this.id = id;
		this.puestoId = puestoId;
		this.numero = numero;
		this.proveedor = proveedor;
		this.fecha = fecha;
	}

	public RemitoProveedorPreviewDto(RemitoProveedorEntity remitoProveedorEntity) {
		super();
		this.id = remitoProveedorEntity.getId();
		this.puestoId = remitoProveedorEntity.getPuesto();
		this.numero = remitoProveedorEntity.getNumero();
		this.proveedor = (remitoProveedorEntity != null && remitoProveedorEntity.getProveedor() != null)
				? new ProveedorDto(remitoProveedorEntity.getProveedor())
				: null;
		this.fecha = remitoProveedorEntity.getFecha();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPuestoId() {
		return puestoId;
	}

	public void setPuestoId(Integer puestoId) {
		this.puestoId = puestoId;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public ProveedorDto getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorDto proveedor) {
		this.proveedor = proveedor;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemitoProveedorPreviewDto other = (RemitoProveedorPreviewDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
