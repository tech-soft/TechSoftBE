package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.PersonaComercialEntity;

public class PersonaComercialDto {

	private Integer id = null;

	private Integer personaId = null;

	private IvaTipoDto ivaTipo = null;

	private PrecioListaDto precioLista = null;

	private PagoFormaDto pagoForma = null;

	private Double descuento = 0D;

	public PersonaComercialDto() {
		super();
	}

	public PersonaComercialDto(Integer id, Integer personaId, IvaTipoDto ivaTipo, PrecioListaDto precioLista,
			PagoFormaDto pagoForma, Double descuento) {
		super();
		this.id = id;
		this.personaId = personaId;
		this.ivaTipo = ivaTipo;
		this.precioLista = precioLista;
		this.pagoForma = pagoForma;
		this.descuento = descuento;
	}

	public PersonaComercialDto(PersonaComercialEntity personaComercial) {
		super();
		this.id = personaComercial.getId();
		this.personaId = personaComercial.getPersona().getId();
		this.ivaTipo = (personaComercial != null && personaComercial.getIvaTipo() != null)
				? new IvaTipoDto(personaComercial.getIvaTipo())
				: null;
		this.precioLista = (personaComercial != null && personaComercial.getPrecioLista() != null)
				? new PrecioListaDto(personaComercial.getPrecioLista())
				: null;
		this.pagoForma = (personaComercial != null && personaComercial.getPagoForma() != null)
				? new PagoFormaDto(personaComercial.getPagoForma())
				: null;
		this.descuento = personaComercial.getDescuento();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Integer personaId) {
		this.personaId = personaId;
	}

	public IvaTipoDto getIvaTipo() {
		return ivaTipo;
	}

	public void setIvaTipo(IvaTipoDto ivaTipo) {
		this.ivaTipo = ivaTipo;
	}

	public PrecioListaDto getPrecioLista() {
		return precioLista;
	}

	public void setPrecioLista(PrecioListaDto precioLista) {
		this.precioLista = precioLista;
	}

	public PagoFormaDto getPagoForma() {
		return pagoForma;
	}

	public void setPagoForma(PagoFormaDto pagoForma) {
		this.pagoForma = pagoForma;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonaComercialDto other = (PersonaComercialDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
