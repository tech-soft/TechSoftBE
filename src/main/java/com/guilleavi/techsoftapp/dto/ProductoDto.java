package com.guilleavi.techsoftapp.dto;

import java.util.ArrayList;
import java.util.List;

import com.guilleavi.techsoftapp.entities.ProductoEntity;

public class ProductoDto {

	private Integer id = null;

	private String descripcion = null;

	private String descripcionReducida = null;

	private EstadoDto estado = null;

	private String codigoBarras = null;

	private GravadoDto gravado = null;

	private OrigenDto origen = null;

	private List<ProductoPrecioDto> precios = null;

	public ProductoDto() {
		super();
	}

	public ProductoDto(Integer id, String descripcion, String descripcionReducida, EstadoDto estado,
			String codigoBarras, GravadoDto gravado, OrigenDto origen) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.descripcionReducida = descripcionReducida;
		this.estado = estado;
		this.codigoBarras = codigoBarras;
		this.gravado = gravado;
		this.origen = origen;
		this.precios = new ArrayList<ProductoPrecioDto>();
	}

	public ProductoDto(ProductoEntity productoEntity) {
		super();
		this.id = productoEntity.getId();
		this.descripcion = productoEntity.getDescripcion();
		this.descripcionReducida = productoEntity.getDescripcionReducida();
		this.estado = (productoEntity.getEstado() != null) ? new EstadoDto(productoEntity.getEstado()) : null;
		this.codigoBarras = productoEntity.getCodigoBarras();
		this.gravado = (productoEntity.getGravado() != null) ? new GravadoDto(productoEntity.getGravado()) : null;
		this.origen = (productoEntity.getOrigen() != null) ? new OrigenDto(productoEntity.getOrigen()) : null;
		this.precios = new ArrayList<ProductoPrecioDto>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionReducida() {
		return descripcionReducida;
	}

	public void setDescripcionReducida(String descripcionReducida) {
		this.descripcionReducida = descripcionReducida;
	}

	public EstadoDto getEstado() {
		return estado;
	}

	public void setEstado(EstadoDto estado) {
		this.estado = estado;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public GravadoDto getGravado() {
		return gravado;
	}

	public void setGravado(GravadoDto gravado) {
		this.gravado = gravado;
	}

	public OrigenDto getOrigen() {
		return origen;
	}

	public void setOrigen(OrigenDto origen) {
		this.origen = origen;
	}

	public List<ProductoPrecioDto> getPrecios() {
		return precios;
	}

	public void setPrecios(List<ProductoPrecioDto> precios) {
		this.precios = precios;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoDto other = (ProductoDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
