package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.DomicilioEntity;

public class DomicilioDto {

	private Integer id = null;

	private String calle = null;

	private String nro = null;

	private String piso = null;

	private String dpto = null;

	private CiudadDto ciudad = null;

	private String descripcion = null;

	private Boolean principal = null;

	private Integer personaId = null;

	public DomicilioDto() {
		super();
	}

	public DomicilioDto(Integer id, String calle, String nro, String piso, String dpto, CiudadDto ciudad,
			String descripcion, Boolean principal, Integer personaId) {
		super();
		this.id = id;
		this.calle = calle;
		this.nro = nro;
		this.piso = piso;
		this.dpto = dpto;
		this.ciudad = ciudad;
		this.descripcion = descripcion;
		this.principal = principal;
		this.personaId = personaId;
	}

	public DomicilioDto(DomicilioEntity domicilio) {
		super();
		this.id = domicilio.getId();
		this.calle = domicilio.getCalle();
		this.nro = domicilio.getNro();
		this.piso = domicilio.getPiso();
		this.dpto = domicilio.getDpto();
		this.ciudad = (domicilio != null && domicilio.getCiudad() != null) ? new CiudadDto(domicilio.getCiudad())
				: null;
		this.descripcion = domicilio.getDescripcion();
		this.principal = domicilio.getPrincipal();
		this.personaId = domicilio.getPersona().getId();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNro() {
		return nro;
	}

	public void setNro(String nro) {
		this.nro = nro;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public CiudadDto getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadDto ciudad) {
		this.ciudad = ciudad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getPrincipal() {
		return principal;
	}

	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

	public Integer getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Integer personaId) {
		this.personaId = personaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomicilioDto other = (DomicilioDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
