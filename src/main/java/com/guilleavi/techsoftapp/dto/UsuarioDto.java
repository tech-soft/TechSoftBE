package com.guilleavi.techsoftapp.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.guilleavi.techsoftapp.entities.PersonaEntity;
import com.guilleavi.techsoftapp.enums.Authority;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UsuarioDto {

	private Integer id;

	private PersonaEntity persona;

	private String username;

	private List<Authority> grupos;

	private Boolean activo;

	private Byte[] logo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PersonaEntity getPersona() {
		return persona;
	}

	public void setPersona(PersonaEntity persona) {
		this.persona = persona;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Authority> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Authority> grupos) {
		this.grupos = grupos;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Byte[] getLogo() {
		return logo;
	}

	public void setLogo(Byte[] logo) {
		this.logo = logo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioDto other = (UsuarioDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}