package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.MonedaEntity;

public class MonedaDto {

	private Integer id = null;

	private String descripcion = null;

	private String simbolo = null;

	public MonedaDto() {
		super();
	}

	public MonedaDto(Integer id, String descripcion, String simbolo) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.simbolo = simbolo;
	}

	public MonedaDto(MonedaEntity moneda) {
		super();
		this.id = moneda.getId();
		this.descripcion = moneda.getDescripcion();
		this.simbolo = moneda.getSimbolo();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonedaDto other = (MonedaDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
