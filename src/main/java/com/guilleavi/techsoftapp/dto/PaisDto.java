package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.PaisEntity;

public class PaisDto {

	private Integer id = null;

	private String nombre = null;

	public PaisDto() {
		super();
	}

	public PaisDto(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public PaisDto(PaisEntity pais) {
		super();
		this.id = pais.getId();
		this.nombre = pais.getNombre();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaisDto other = (PaisDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
