package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.PersonaEntity;

public class PersonaDto {
	
	private Integer id = null;

	private String nombre = null;

	private DocumentoTipoDto documentoTipo = null;

	private Long documentoNumero = null;

	private EstadoDto estado = null;

	private Boolean cliente = false;

	private Boolean proveedor = false;

	public PersonaDto() {
		super();
	}

	public PersonaDto(Integer id, String nombre, DocumentoTipoDto documentoTipo, Long documentoNumero,
			EstadoDto estado, Boolean cliente, Boolean proveedor) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.documentoTipo = documentoTipo;
		this.documentoNumero = documentoNumero;
		this.estado = estado;
		this.cliente = cliente;
		this.proveedor = proveedor;
	}

	public PersonaDto(PersonaEntity persona) {
		super();
		this.id = persona.getId();
		this.nombre = persona.getNombre();
		this.documentoTipo = (persona != null && persona.getDocumentoTipo() != null) ? new DocumentoTipoDto(persona.getDocumentoTipo()) : null;
		this.documentoNumero = persona.getDocumentoNumero();
		this.estado = (persona != null && persona.getEstado() != null) ? new EstadoDto(persona.getEstado()) : null;
		this.cliente = persona.getCliente();
		this.proveedor = persona.getProveedor();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DocumentoTipoDto getDocumentoTipo() {
		return documentoTipo;
	}

	public void setDocumentoTipo(DocumentoTipoDto documentoTipo) {
		this.documentoTipo = documentoTipo;
	}

	public Long getDocumentoNumero() {
		return documentoNumero;
	}

	public void setDocumentoNumero(Long documentoNumero) {
		this.documentoNumero = documentoNumero;
	}

	public EstadoDto getEstado() {
		return estado;
	}

	public void setEstado(EstadoDto estado) {
		this.estado = estado;
	}

	public Boolean getCliente() {
		return cliente;
	}

	public void setCliente(Boolean cliente) {
		this.cliente = cliente;
	}

	public Boolean getProveedor() {
		return proveedor;
	}

	public void setProveedor(Boolean proveedor) {
		this.proveedor = proveedor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonaDto other = (PersonaDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
