package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.ProductoEntity;

public class ProductoPreciosPreviewDto {

	private Integer id = null;

	private String descripcion = null;

	private Integer stockEntrante = null;

	private Integer stockDisponible = null;

	private Integer stockTransito = null;

	private Double lista1SinIva = null;

	private Double lista1ConIva = null;

	private Double lista2SinIva = null;

	private Double lista2ConIva = null;

	private Double lista3SinIva = null;

	private Double lista3ConIva = null;

	private Double lista4SinIva = null;

	private Double lista4ConIva = null;

	private Double lista5SinIva = null;

	private Double lista5ConIva = null;

	private Double lista6SinIva = null;

	private Double lista6ConIva = null;

	public ProductoPreciosPreviewDto() {
		super();
	}

	public ProductoPreciosPreviewDto(ProductoEntity productoEntity) {
		super();
		this.id = productoEntity.getId();
		this.descripcion = productoEntity.getDescripcion();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getStockEntrante() {
		return stockEntrante;
	}

	public void setStockEntrante(Integer stockEntrante) {
		this.stockEntrante = stockEntrante;
	}

	public Integer getStockDisponible() {
		return stockDisponible;
	}

	public void setStockDisponible(Integer stockDisponible) {
		this.stockDisponible = stockDisponible;
	}

	public Integer getStockTransito() {
		return stockTransito;
	}

	public void setStockTransito(Integer stockTransito) {
		this.stockTransito = stockTransito;
	}

	public Double getLista1SinIva() {
		return lista1SinIva;
	}

	public void setLista1SinIva(Double lista1SinIva) {
		this.lista1SinIva = lista1SinIva;
	}

	public Double getLista1ConIva() {
		return lista1ConIva;
	}

	public void setLista1ConIva(Double lista1ConIva) {
		this.lista1ConIva = lista1ConIva;
	}

	public Double getLista2SinIva() {
		return lista2SinIva;
	}

	public void setLista2SinIva(Double lista2SinIva) {
		this.lista2SinIva = lista2SinIva;
	}

	public Double getLista2ConIva() {
		return lista2ConIva;
	}

	public void setLista2ConIva(Double lista2ConIva) {
		this.lista2ConIva = lista2ConIva;
	}

	public Double getLista3SinIva() {
		return lista3SinIva;
	}

	public void setLista3SinIva(Double lista3SinIva) {
		this.lista3SinIva = lista3SinIva;
	}

	public Double getLista3ConIva() {
		return lista3ConIva;
	}

	public void setLista3ConIva(Double lista3ConIva) {
		this.lista3ConIva = lista3ConIva;
	}

	public Double getLista4SinIva() {
		return lista4SinIva;
	}

	public void setLista4SinIva(Double lista4SinIva) {
		this.lista4SinIva = lista4SinIva;
	}

	public Double getLista4ConIva() {
		return lista4ConIva;
	}

	public void setLista4ConIva(Double lista4ConIva) {
		this.lista4ConIva = lista4ConIva;
	}

	public Double getLista5SinIva() {
		return lista5SinIva;
	}

	public void setLista5SinIva(Double lista5SinIva) {
		this.lista5SinIva = lista5SinIva;
	}

	public Double getLista5ConIva() {
		return lista5ConIva;
	}

	public void setLista5ConIva(Double lista5ConIva) {
		this.lista5ConIva = lista5ConIva;
	}

	public Double getLista6SinIva() {
		return lista6SinIva;
	}

	public void setLista6SinIva(Double lista6SinIva) {
		this.lista6SinIva = lista6SinIva;
	}

	public Double getLista6ConIva() {
		return lista6ConIva;
	}

	public void setLista6ConIva(Double lista6ConIva) {
		this.lista6ConIva = lista6ConIva;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoPreciosPreviewDto other = (ProductoPreciosPreviewDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
