package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.OrigenEntity;

public class OrigenDto {

	private Integer id = null;

	private String descripcion = null;

	public OrigenDto() {
		super();
	}

	public OrigenDto(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public OrigenDto(OrigenEntity origenEntity) {
		super();
		this.id = origenEntity.getId();
		this.descripcion = origenEntity.getDescripcion();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrigenDto other = (OrigenDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
