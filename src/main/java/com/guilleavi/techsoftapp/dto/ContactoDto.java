package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.ContactoEntity;

public class ContactoDto {

	private Integer id = null;

	private Integer tipo = null;

	private String contacto = null;

	private String descripcion = null;

	private Integer personaId = null;

	public ContactoDto() {
		super();
	}

	public ContactoDto(Integer id, Integer tipo, String contacto, String descripcion, Integer personaId) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.contacto = contacto;
		this.descripcion = descripcion;
		this.personaId = personaId;
	}

	public ContactoDto(ContactoEntity contactoEntity) {
		super();
		this.id = contactoEntity.getId();
		this.tipo = (contactoEntity.getTipo() != null) ? contactoEntity.getTipo().getId() : 0;
		this.contacto = contactoEntity.getContacto();
		this.descripcion = contactoEntity.getDescripcion();
		this.personaId = (contactoEntity.getPersona() != null) ? contactoEntity.getPersona().getId() : 0;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Integer personaId) {
		this.personaId = personaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactoDto other = (ContactoDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
