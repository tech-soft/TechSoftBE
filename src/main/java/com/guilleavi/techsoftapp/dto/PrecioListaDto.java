package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.PrecioListaEntity;

public class PrecioListaDto {

	private Integer id = null;

	private String descripcion = null;

	public PrecioListaDto() {
		super();
	}

	public PrecioListaDto(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public PrecioListaDto(PrecioListaEntity precioLista) {
		super();
		this.id = precioLista.getId();
		this.descripcion = precioLista.getDescripcion();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrecioListaDto other = (PrecioListaDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
