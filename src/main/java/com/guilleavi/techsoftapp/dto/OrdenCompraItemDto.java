package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.OrdenCompraItemEntity;

public class OrdenCompraItemDto {

	private Integer id = null;

	private ProductoPreviewDto producto = null;

	private Integer cantidad = null;

	private Double precio = null;

	private EstadoDto estado = null;

	public OrdenCompraItemDto() {
		super();
	}

	public OrdenCompraItemDto(Integer id, ProductoPreviewDto producto, Integer cantidad, Double precio,
			EstadoDto estado) {
		super();
		this.id = id;
		this.producto = producto;
		this.cantidad = cantidad;
		this.precio = precio;
		this.estado = estado;
	}

	public OrdenCompraItemDto(OrdenCompraItemEntity item) {
		super();
		this.id = item.getId();
		this.producto = (item != null && item.getProducto() != null) ? new ProductoPreviewDto(item.getProducto())
				: null;
		this.cantidad = item.getCantidad();
		this.precio = item.getPrecio();
		this.estado = (item != null && item.getEstado() != null) ? new EstadoDto(item.getEstado()) : null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProductoPreviewDto getProducto() {
		return producto;
	}

	public void setProducto(ProductoPreviewDto producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public EstadoDto getEstado() {
		return estado;
	}

	public void setEstado(EstadoDto estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdenCompraItemDto other = (OrdenCompraItemDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
