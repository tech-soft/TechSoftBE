package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.GravadoEntity;

public class GravadoDto {

	private Integer id = null;

	private String descripcion = null;

	private Double porcenaje = null;

	public GravadoDto() {
		super();
	}

	public GravadoDto(Integer id, String descripcion, Double porcenaje) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.porcenaje = porcenaje;
	}

	public GravadoDto(GravadoEntity gravadoEntity) {
		super();
		this.id = gravadoEntity.getId();
		this.descripcion = gravadoEntity.getDescripcion();
		this.porcenaje = gravadoEntity.getPorcentaje();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getPorcenaje() {
		return porcenaje;
	}

	public void setPorcenaje(Double porcenaje) {
		this.porcenaje = porcenaje;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GravadoDto other = (GravadoDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
