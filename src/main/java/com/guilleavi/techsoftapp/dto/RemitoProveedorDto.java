package com.guilleavi.techsoftapp.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.guilleavi.techsoftapp.entities.RemitoProveedorEntity;

public class RemitoProveedorDto {

	private Integer id = null;

	private ComprobanteTipoDto comprobanteTipo = null;

	private Integer puestoId = null;

	private Long numero = null;

	private ProveedorDto proveedor = null;

	private List<RemitoProveedorItemDto> items = null;

	private LocalDate fecha = null;

	private String observaciones = null;

	public RemitoProveedorDto() {
		super();
		items = new ArrayList<RemitoProveedorItemDto>();
	}

	public RemitoProveedorDto(Integer id, ComprobanteTipoDto comprobanteTipo, Integer puestoId, Long numero,
			ProveedorDto proveedor, List<RemitoProveedorItemDto> items, LocalDate fecha, String observaciones) {
		super();
		this.id = id;
		this.comprobanteTipo = comprobanteTipo;
		this.puestoId = puestoId;
		this.numero = numero;
		this.proveedor = proveedor;
		this.items = items;
		this.fecha = fecha;
		this.observaciones = observaciones;
	}

	public RemitoProveedorDto(RemitoProveedorEntity remitoProveedorEntity) {
		super();
		this.id = remitoProveedorEntity.getId();
		this.comprobanteTipo = (remitoProveedorEntity != null && remitoProveedorEntity.getComprobanteTipo() != null)
				? new ComprobanteTipoDto(remitoProveedorEntity.getComprobanteTipo())
				: null;
		this.puestoId = remitoProveedorEntity.getPuesto();
		this.numero = remitoProveedorEntity.getNumero();
		this.proveedor = (remitoProveedorEntity != null && remitoProveedorEntity.getProveedor() != null)
				? new ProveedorDto(remitoProveedorEntity.getProveedor())
				: null;
		this.items = new ArrayList<RemitoProveedorItemDto>();
		this.fecha = remitoProveedorEntity.getFecha();
		this.observaciones = remitoProveedorEntity.getObservaciones();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ComprobanteTipoDto getComprobanteTipo() {
		return comprobanteTipo;
	}

	public void setComprobanteTipo(ComprobanteTipoDto comprobanteTipo) {
		this.comprobanteTipo = comprobanteTipo;
	}

	public Integer getPuestoId() {
		return puestoId;
	}

	public void setPuestoId(Integer puestoId) {
		this.puestoId = puestoId;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public ProveedorDto getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorDto proveedor) {
		this.proveedor = proveedor;
	}

	public List<RemitoProveedorItemDto> getItems() {
		return items;
	}

	public void setItems(List<RemitoProveedorItemDto> items) {
		this.items = items;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemitoProveedorDto other = (RemitoProveedorDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
