package com.guilleavi.techsoftapp.dto;

import java.time.LocalDate;

import com.guilleavi.techsoftapp.entities.ProductoCostoEntity;

public class ProductoCostoDto {

	private Integer id = null;

	private LocalDate fecha = null;

	private Double costo = null;

	public ProductoCostoDto() {
		super();
	}

	public ProductoCostoDto(Integer id, LocalDate fecha, Double costo) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.costo = costo;
	}

	public ProductoCostoDto(ProductoCostoEntity productoCostoEntity) {
		super();
		this.id = productoCostoEntity.getId();
		this.fecha = productoCostoEntity.getFecha();
		this.costo = productoCostoEntity.getCosto();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Double getCosto() {
		return costo;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoCostoDto other = (ProductoCostoDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
