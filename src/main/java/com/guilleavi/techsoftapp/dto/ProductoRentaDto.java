package com.guilleavi.techsoftapp.dto;

import java.time.LocalDate;

import com.guilleavi.techsoftapp.entities.ProductoPrecioEntity;

public class ProductoRentaDto {

	private Integer id = null;

	private Integer listaId = null;

	private LocalDate fecha = null;

	private Double renta = null;

	public ProductoRentaDto() {
		super();
	}

	public ProductoRentaDto(Integer id, Integer listaId, LocalDate fecha, Double renta) {
		super();
		this.id = id;
		this.listaId = listaId;
		this.fecha = fecha;
		this.renta = renta;
	}

	public ProductoRentaDto(ProductoPrecioEntity productoPrecioEntity) {
		super();
		this.id = productoPrecioEntity.getId();
		this.listaId = (productoPrecioEntity.getListaPrecio() != null ) ? productoPrecioEntity.getListaPrecio().getId() : null;
		this.fecha = productoPrecioEntity.getFecha();
		this.renta = productoPrecioEntity.getRenta();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getListaId() {
		return listaId;
	}

	public void setListaId(Integer listaId) {
		this.listaId = listaId;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Double getRenta() {
		return renta;
	}

	public void setRenta(Double renta) {
		this.renta = renta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoRentaDto other = (ProductoRentaDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
