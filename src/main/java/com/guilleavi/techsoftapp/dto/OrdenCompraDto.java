package com.guilleavi.techsoftapp.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.guilleavi.techsoftapp.entities.OrdenCompraEntity;

public class OrdenCompraDto {

	private Integer id = null;

	private PuestoDto puesto = null;

	private Long numero = null;

	private ProveedorDto proveedor = null;

	private LocalDate fechaCreacion = null;

	private LocalDate fechaEntrega = null;

	private EstadoDto estado = null;

	private String observaciones = null;

	private List<OrdenCompraItemDto> items = null;

	public OrdenCompraDto() {
		super();
		items = new ArrayList<OrdenCompraItemDto>();
	}

	public OrdenCompraDto(Integer id, PuestoDto puesto, Long numero, ProveedorDto proveedor, LocalDate fechaCreacion,
			LocalDate fechaEntrega, EstadoDto estado, String observaciones) {
		super();
		this.id = id;
		this.puesto = puesto;
		this.numero = numero;
		this.proveedor = proveedor;
		this.fechaCreacion = fechaCreacion;
		this.fechaEntrega = fechaEntrega;
		this.estado = estado;
		this.observaciones = observaciones;
	}

	public OrdenCompraDto(OrdenCompraEntity ordenEntity) {
		super();
		this.id = ordenEntity.getId();
		this.puesto = (ordenEntity != null && ordenEntity.getPuesto() != null) ? new PuestoDto(ordenEntity.getPuesto()) : null;
		this.numero = ordenEntity.getNumero();
		this.proveedor = (ordenEntity != null && ordenEntity.getProveedor() != null) ? new ProveedorDto(ordenEntity.getProveedor()) : null;
		this.fechaCreacion = ordenEntity.getFechaCreacion();
		this.fechaEntrega = ordenEntity.getFechaEntrega();
		this.estado = (ordenEntity != null && ordenEntity.getEstado() != null) ? new EstadoDto(ordenEntity.getEstado()) : null;
		this.observaciones = ordenEntity.getObservaciones();
		items = new ArrayList<OrdenCompraItemDto>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PuestoDto getPuesto() {
		return puesto;
	}

	public void setPuesto(PuestoDto puesto) {
		this.puesto = puesto;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public ProveedorDto getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorDto proveedor) {
		this.proveedor = proveedor;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public LocalDate getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(LocalDate fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public EstadoDto getEstado() {
		return estado;
	}

	public void setEstado(EstadoDto estado) {
		this.estado = estado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public List<OrdenCompraItemDto> getItems() {
		return items;
	}

	public void setItems(List<OrdenCompraItemDto> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdenCompraDto other = (OrdenCompraDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
