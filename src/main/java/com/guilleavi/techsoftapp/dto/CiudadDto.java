package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.CiudadEntity;

public class CiudadDto {

	private Integer id = null;

	private String nombre = null;

	private String codigoPostal = null;

	private ProvinciaDto provincia = null;

	public CiudadDto() {
		super();
	}

	public CiudadDto(Integer id, String nombre, String codigoPostal, ProvinciaDto provincia) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.codigoPostal = codigoPostal;
		this.provincia = provincia;
	}

	public CiudadDto(CiudadEntity ciudad) {
		super();
		this.id = ciudad.getId();
		this.nombre = ciudad.getNombre();
		this.codigoPostal = ciudad.getCodigoPostal();
		this.provincia = (ciudad != null && ciudad.getProvincia() != null) ? new ProvinciaDto(ciudad.getProvincia())
				: null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public ProvinciaDto getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaDto provincia) {
		this.provincia = provincia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CiudadDto other = (CiudadDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
