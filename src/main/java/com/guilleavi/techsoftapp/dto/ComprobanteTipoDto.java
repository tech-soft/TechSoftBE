package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.ComprobanteTipoEntity;

public class ComprobanteTipoDto {

	private Integer id = null;

	private String letra = null;

	private Integer tipo = null;

	public ComprobanteTipoDto() {
		super();
	}

	public ComprobanteTipoDto(Integer id, String letra, Integer tipo) {
		super();
		this.id = id;
		this.letra = letra;
		this.tipo = tipo;
	}

	public ComprobanteTipoDto(ComprobanteTipoEntity comprobanteTipo) {
		super();
		this.id = comprobanteTipo.getId();
		this.letra = comprobanteTipo.getLetra();
		this.tipo = comprobanteTipo.getTipo();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLetra() {
		return letra;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComprobanteTipoDto other = (ComprobanteTipoDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
