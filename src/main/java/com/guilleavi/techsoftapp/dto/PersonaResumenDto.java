package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.PersonaEntity;

public class PersonaResumenDto {

	private Integer id = null;

	private String nombre = null;

	private String domicilioPrincipal = null;

	private String telefonoPrincipal = null;

	public PersonaResumenDto() {
		super();
	}

	public PersonaResumenDto(Integer id, String nombre, String domicilioPrincipal, String telefonoPrincipal) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.domicilioPrincipal = domicilioPrincipal;
		this.telefonoPrincipal = telefonoPrincipal;
	}

	public PersonaResumenDto(PersonaEntity personaEntity) {
		super();
		this.id = personaEntity.getId();
		this.nombre = personaEntity.getNombre();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilioPrincipal() {
		return domicilioPrincipal;
	}

	public void setDomicilioPrincipal(String domicilioPrincipal) {
		this.domicilioPrincipal = domicilioPrincipal;
	}

	public String getTelefonoPrincipal() {
		return telefonoPrincipal;
	}

	public void setTelefonoPrincipal(String telefonoPrincipal) {
		this.telefonoPrincipal = telefonoPrincipal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonaResumenDto other = (PersonaResumenDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
