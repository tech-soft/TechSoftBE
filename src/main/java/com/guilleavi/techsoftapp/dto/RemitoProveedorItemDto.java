package com.guilleavi.techsoftapp.dto;

import java.util.ArrayList;
import java.util.List;

import com.guilleavi.techsoftapp.entities.RemitoProveedorItemEntity;

public class RemitoProveedorItemDto {

	private Integer id = null;

	private ProductoPreviewDto producto = null;

	private Integer cantidad = null;

	private List<String> numerosSerie = null;

	private OrdenCompraPreviewDto ordenCompra = null;

	private Integer cantidadOrdenCompra = 0;

	public RemitoProveedorItemDto() {
		super();
		this.numerosSerie = new ArrayList<String>();
	}

	public RemitoProveedorItemDto(Integer id, ProductoPreviewDto producto, Integer cantidad, List<String> numerosSerie,
			OrdenCompraPreviewDto ordenCompra, Integer cantidadOrdenCompra) {
		super();
		this.id = id;
		this.producto = producto;
		this.cantidad = cantidad;
		this.numerosSerie = numerosSerie;
		this.ordenCompra = ordenCompra;
		this.cantidadOrdenCompra = cantidadOrdenCompra;
	}

	public RemitoProveedorItemDto(RemitoProveedorItemEntity remitoProveedorItemEntity) {
		super();
		this.id = remitoProveedorItemEntity.getId();
		this.producto = (remitoProveedorItemEntity != null && remitoProveedorItemEntity.getProducto() != null)
				? new ProductoPreviewDto(remitoProveedorItemEntity.getProducto())
				: null;
		this.cantidad = remitoProveedorItemEntity.getCantidad();
		this.numerosSerie = new ArrayList<String>();
		this.ordenCompra = (remitoProveedorItemEntity != null && remitoProveedorItemEntity.getOrden() != null)
				? new OrdenCompraPreviewDto(remitoProveedorItemEntity.getOrden())
				: null;
		this.cantidadOrdenCompra = remitoProveedorItemEntity.getCantidadOrdenCompra();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProductoPreviewDto getProducto() {
		return producto;
	}

	public void setProducto(ProductoPreviewDto producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public List<String> getNumerosSerie() {
		return numerosSerie;
	}

	public void setNumerosSerie(List<String> numerosSerie) {
		this.numerosSerie = numerosSerie;
	}

	public OrdenCompraPreviewDto getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompraPreviewDto ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public Integer getCantidadOrdenCompra() {
		return cantidadOrdenCompra;
	}

	public void setCantidadOrdenCompra(Integer cantidadOrdenCompra) {
		this.cantidadOrdenCompra = cantidadOrdenCompra;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemitoProveedorItemDto other = (RemitoProveedorItemDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
