package com.guilleavi.techsoftapp.dto;

public class ProductoPrecioDto {

	private Integer listaId = null;

	private Double costo = null;

	private Double iva = null;

	private Double renta = null;

	private Double precioFinal = null;

	public ProductoPrecioDto() {
		super();
	}

	public ProductoPrecioDto(Integer listaId, Double costo, Double iva, Double renta, Double precioFinal) {
		super();
		this.listaId = listaId;
		this.costo = costo;
		this.iva = iva;
		this.renta = renta;
		this.precioFinal = precioFinal;
	}

	public Integer getListaId() {
		return listaId;
	}

	public void setListaId(Integer listaId) {
		this.listaId = listaId;
	}

	public Double getCosto() {
		return costo;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}

	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	public Double getRenta() {
		return renta;
	}

	public void setRenta(Double renta) {
		this.renta = renta;
	}

	public Double getPrecioFinal() {
		return precioFinal;
	}

	public void setPrecioFinal(Double precioFinal) {
		this.precioFinal = precioFinal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((listaId == null) ? 0 : listaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoPrecioDto other = (ProductoPrecioDto) obj;
		if (listaId == null) {
			if (other.listaId != null)
				return false;
		} else if (!listaId.equals(other.listaId))
			return false;
		return true;
	}

}
