package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.ProvinciaEntity;

public class ProvinciaDto {

	private Integer id = null;

	private String nombre = null;

	private PaisDto pais = null;

	public ProvinciaDto() {
		super();
	}

	public ProvinciaDto(Integer id, String nombre, PaisDto pais) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.pais = pais;
	}

	public ProvinciaDto(ProvinciaEntity provincia) {
		super();
		this.id = provincia.getId();
		this.nombre = provincia.getNombre();
		this.pais = (provincia != null && provincia.getPais() != null) ? new PaisDto(provincia.getPais()) : null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public PaisDto getPais() {
		return pais;
	}

	public void setPais(PaisDto pais) {
		this.pais = pais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProvinciaDto other = (ProvinciaDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
