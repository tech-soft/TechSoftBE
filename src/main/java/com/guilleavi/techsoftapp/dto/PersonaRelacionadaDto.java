package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.PersonaRelacionadaEntity;

public class PersonaRelacionadaDto {

	private Integer id = null;

	private PersonaDto personaPrincipal = null;

	private PersonaDto personaRelacionada = null;

	private PersonaRelacionTipoDto tipo = null;

	public PersonaRelacionadaDto() {
		super();
	}

	public PersonaRelacionadaDto(Integer id, PersonaDto personaPrincipal, PersonaDto personaRelacionada,
			PersonaRelacionTipoDto tipo) {
		super();
		this.id = id;
		this.personaPrincipal = personaPrincipal;
		this.personaRelacionada = personaRelacionada;
		this.tipo = tipo;
	}

	public PersonaRelacionadaDto(PersonaRelacionadaEntity personaRelacionadaEntity) {
		super();
		this.id = personaRelacionadaEntity.getId();
		this.personaPrincipal = (personaRelacionadaEntity != null
				&& personaRelacionadaEntity.getPersonaPrincipal() != null)
						? new PersonaDto(personaRelacionadaEntity.getPersonaPrincipal())
						: null;
		this.personaRelacionada = (personaRelacionadaEntity != null
				&& personaRelacionadaEntity.getPersonaRelacionada() != null)
						? new PersonaDto(personaRelacionadaEntity.getPersonaRelacionada())
						: null;
		this.tipo = (personaRelacionadaEntity != null && personaRelacionadaEntity.getTipo() != null)
				? new PersonaRelacionTipoDto(personaRelacionadaEntity.getTipo())
				: null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PersonaDto getPersonaPrincipal() {
		return personaPrincipal;
	}

	public void setPersonaPrincipal(PersonaDto personaPrincipal) {
		this.personaPrincipal = personaPrincipal;
	}

	public PersonaDto getPersonaRelacionada() {
		return personaRelacionada;
	}

	public void setPersonaRelacionada(PersonaDto personaRelacionada) {
		this.personaRelacionada = personaRelacionada;
	}

	public PersonaRelacionTipoDto getTipo() {
		return tipo;
	}

	public void setTipo(PersonaRelacionTipoDto tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonaRelacionadaDto other = (PersonaRelacionadaDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
