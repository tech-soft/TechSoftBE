package com.guilleavi.techsoftapp.dto;

import com.guilleavi.techsoftapp.entities.ProductoStockEntity;

public class ProductoStockDto {

	private Integer productoId = null;

	private Integer stockEntrante = null;

	private Integer stockDisponible = null;

	private Integer stockTransito = null;

	private Integer stockReservado = null;

	private Integer stockVendido = null;

	public ProductoStockDto() {
		super();
	}

	public ProductoStockDto(Integer productoId, Integer stockEntrante, Integer stockDisponible,
			Integer stockTransito, Integer stockReservado, Integer stockVendido) {
		super();
		this.productoId = productoId;
		this.stockEntrante = stockEntrante;
		this.stockDisponible = stockDisponible;
		this.stockTransito = stockTransito;
		this.stockReservado = stockReservado;
		this.stockVendido = stockVendido;
	}

	public ProductoStockDto(ProductoStockEntity productoStockEntity) {
		super();
		this.productoId = (productoStockEntity.getProducto() != null) ? productoStockEntity.getProducto().getId() : null;
		this.stockEntrante = productoStockEntity.getStockEntrante();
		this.stockDisponible = productoStockEntity.getStockDisponible();
		this.stockTransito = productoStockEntity.getStockTransito();
		this.stockReservado = productoStockEntity.getStockReservado();
		this.stockVendido = productoStockEntity.getStockVendido();
	}

	public Integer getProductoId() {
		return productoId;
	}

	public void setProductoId(Integer productoId) {
		this.productoId = productoId;
	}

	public Integer getStockEntrante() {
		return stockEntrante;
	}

	public void setStockEntrante(Integer stockEntrante) {
		this.stockEntrante = stockEntrante;
	}

	public Integer getStockDisponible() {
		return stockDisponible;
	}

	public void setStockDisponible(Integer stockDisponible) {
		this.stockDisponible = stockDisponible;
	}

	public Integer getStockTransito() {
		return stockTransito;
	}

	public void setStockTransito(Integer stockTransito) {
		this.stockTransito = stockTransito;
	}

	public Integer getStockReservado() {
		return stockReservado;
	}

	public void setStockReservado(Integer stockReservado) {
		this.stockReservado = stockReservado;
	}

	public Integer getStockVendido() {
		return stockVendido;
	}

	public void setStockVendido(Integer stockVendido) {
		this.stockVendido = stockVendido;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productoId == null) ? 0 : productoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoStockDto other = (ProductoStockDto) obj;
		if (productoId == null) {
			if (other.productoId != null)
				return false;
		} else if (!productoId.equals(other.productoId))
			return false;
		return true;
	}
	
}
