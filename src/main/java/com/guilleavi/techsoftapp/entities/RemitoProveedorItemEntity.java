package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "remito_proveedor_item")
public class RemitoProveedorItemEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "remito_id", referencedColumnName = "id", nullable = false)
	private RemitoProveedorEntity remito = null;

	@OneToOne
	@JoinColumn(name = "producto_id", referencedColumnName = "id", nullable = false)
	private ProductoEntity producto = null;

	@Column(name = "cantidad", nullable = false)
	private Integer cantidad = null;

	@OneToOne
	@JoinColumn(name = "orden_id", referencedColumnName = "id", nullable = true)
	private OrdenCompraEntity orden = null;

	@Column(name = "cantidad_orden_compra", nullable = false)
	private Integer cantidadOrdenCompra = null;

	public RemitoProveedorItemEntity() {
		super();
	}

	public RemitoProveedorItemEntity(Integer id, RemitoProveedorEntity remito, ProductoEntity producto,
			Integer cantidad, OrdenCompraEntity orden, Integer cantidadOrdenCompra) {
		super();
		this.id = id;
		this.remito = remito;
		this.producto = producto;
		this.cantidad = cantidad;
		this.orden = orden;
		this.cantidadOrdenCompra = cantidadOrdenCompra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RemitoProveedorEntity getRemito() {
		return remito;
	}

	public void setRemito(RemitoProveedorEntity remito) {
		this.remito = remito;
	}

	public ProductoEntity getProducto() {
		return producto;
	}

	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public OrdenCompraEntity getOrden() {
		return orden;
	}

	public void setOrden(OrdenCompraEntity orden) {
		this.orden = orden;
	}

	public Integer getCantidadOrdenCompra() {
		return cantidadOrdenCompra;
	}

	public void setCantidadOrdenCompra(Integer cantidadOrdenCompra) {
		this.cantidadOrdenCompra = cantidadOrdenCompra;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemitoProveedorItemEntity other = (RemitoProveedorItemEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
