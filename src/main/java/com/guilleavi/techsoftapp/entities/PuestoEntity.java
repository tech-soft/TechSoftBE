package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "puesto")
public class PuestoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@Column(name = "codigo", nullable = false)
	private Integer codigo = null;

	@Column(name = "codigo_string", nullable = true, length = 100)
	private String codigoString = null;

	@OneToOne
	@JoinColumn(name = "estado_id", referencedColumnName = "id", nullable = false)
	private EstadoEntity estado = null;

	public PuestoEntity() {
		super();
	}

	public PuestoEntity(Integer id, Integer codigo, String codigoString, EstadoEntity estado) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.codigoString = codigoString;
		this.estado = estado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getCodigoString() {
		return codigoString;
	}

	public void setCodigoString(String codigoString) {
		this.codigoString = codigoString;
	}

	public EstadoEntity getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntity estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PuestoEntity other = (PuestoEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
