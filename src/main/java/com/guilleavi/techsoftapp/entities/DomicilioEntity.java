package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "domicilio")
public class DomicilioEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "persona_id", referencedColumnName = "id", nullable = false)
	private PersonaEntity persona = null;

	@Column(name = "calle", nullable = false, length = 100)
	private String calle = null;

	@Column(name = "nro", nullable = false, length = 10)
	private String nro = null;

	@Column(name = "piso", nullable = true, length = 5)
	private String piso = null;

	@Column(name = "dpto", nullable = true, length = 5)
	private String dpto = null;

	@OneToOne
	@JoinColumn(name = "ciudad_id", referencedColumnName = "id", nullable = false)
	private CiudadEntity ciudad = null;

	@Column(name = "descripcion", nullable = true, length = 45)
	private String descripcion = null;

	@Column(name = "principal", nullable = false)
	private Boolean principal = null;

	public DomicilioEntity() {
		super();
	}

	public DomicilioEntity(Integer id, PersonaEntity persona, String calle, String nro, String piso, String dpto,
			CiudadEntity ciudad, String descripcion, Boolean principal) {
		super();
		this.id = id;
		this.persona = persona;
		this.calle = calle;
		this.nro = nro;
		this.piso = piso;
		this.dpto = dpto;
		this.ciudad = ciudad;
		this.descripcion = descripcion;
		this.principal = principal;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PersonaEntity getPersona() {
		return persona;
	}

	public void setPersona(PersonaEntity persona) {
		this.persona = persona;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNro() {
		return nro;
	}

	public void setNro(String nro) {
		this.nro = nro;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public CiudadEntity getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadEntity ciudad) {
		this.ciudad = ciudad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getPrincipal() {
		return principal;
	}

	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DomicilioEntity other = (DomicilioEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "DomicilioEntity [id=" + id + ", calle=" + calle + ", nro=" + nro + "]";
	}
}
