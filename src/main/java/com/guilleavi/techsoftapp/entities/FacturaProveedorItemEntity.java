package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "factura_proveedor_item")
public class FacturaProveedorItemEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "factura_id", referencedColumnName = "id", nullable = false)
	private FacturaProveedorEntity factura = null;

	@OneToOne
	@JoinColumn(name = "producto_id", referencedColumnName = "id", nullable = false)
	private ProductoEntity producto = null;

	@Column(name = "cantidad", nullable = false)
	private Integer cantidad = null;

	@OneToOne
	@JoinColumn(name = "iva_tipo_id", referencedColumnName = "id", nullable = false)
	private IvaTipoEntity ivaTipo = null;

	@Column(name = "precio_unitario_sin_iva", nullable = false)
	private Double precioUnitarioSinIva = null;

	@Column(name = "valor_unitario_iva", nullable = false)
	private Double valorUnitarioIva = null;

	public FacturaProveedorItemEntity() {
		super();
	}

	public FacturaProveedorItemEntity(Integer id, FacturaProveedorEntity factura, ProductoEntity producto,
			Integer cantidad, IvaTipoEntity ivaTipo, Double precioUnitarioSinIva, Double valorUnitarioIva) {
		super();
		this.id = id;
		this.factura = factura;
		this.producto = producto;
		this.cantidad = cantidad;
		this.ivaTipo = ivaTipo;
		this.precioUnitarioSinIva = precioUnitarioSinIva;
		this.valorUnitarioIva = valorUnitarioIva;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public FacturaProveedorEntity getFactura() {
		return factura;
	}

	public void setFactura(FacturaProveedorEntity factura) {
		this.factura = factura;
	}

	public ProductoEntity getProducto() {
		return producto;
	}

	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public IvaTipoEntity getIvaTipo() {
		return ivaTipo;
	}

	public void setIvaTipo(IvaTipoEntity ivaTipo) {
		this.ivaTipo = ivaTipo;
	}

	public Double getPrecioUnitarioSinIva() {
		return precioUnitarioSinIva;
	}

	public void setPrecioUnitarioSinIva(Double precioUnitarioSinIva) {
		this.precioUnitarioSinIva = precioUnitarioSinIva;
	}

	public Double getValorUnitarioIva() {
		return valorUnitarioIva;
	}

	public void setValorUnitarioIva(Double valorUnitarioIva) {
		this.valorUnitarioIva = valorUnitarioIva;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FacturaProveedorItemEntity other = (FacturaProveedorItemEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
