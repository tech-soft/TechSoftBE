package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pago")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.INTEGER, name = "id", columnDefinition = "INT(1)")
public class PagoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "proveedor_id", referencedColumnName = "id", nullable = true)
	private PersonaEntity proveedor = null;

	@OneToOne
	@JoinColumn(name = "pago_forma_id", referencedColumnName = "id", nullable = false)
	private PagoFormaEntity pagoForma = null;

	@OneToOne
	@JoinColumn(name = "moneda_id", referencedColumnName = "id", nullable = false)
	private MonedaEntity moneda = null;

	@Column(name = "fecha_emision", nullable = false)
	private LocalDate fechaEmision = null;

	@Column(name = "importe", nullable = false)
	private Double importe = null;

	@Column(name = "descripcion", nullable = true, length = 100)
	private String descripcion = null;

	public PagoEntity() {
		super();
	}

	public PagoEntity(Integer id, PersonaEntity proveedor, PagoFormaEntity pagoForma, MonedaEntity moneda,
			LocalDate fechaEmision, Double importe, String descripcion) {
		super();
		this.id = id;
		this.proveedor = proveedor;
		this.pagoForma = pagoForma;
		this.moneda = moneda;
		this.fechaEmision = fechaEmision;
		this.importe = importe;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PersonaEntity getProveedor() {
		return proveedor;
	}

	public void setProveedor(PersonaEntity proveedor) {
		this.proveedor = proveedor;
	}

	public PagoFormaEntity getPagoForma() {
		return pagoForma;
	}

	public void setPagoForma(PagoFormaEntity pagoForma) {
		this.pagoForma = pagoForma;
	}

	public MonedaEntity getMoneda() {
		return moneda;
	}

	public void setMoneda(MonedaEntity moneda) {
		this.moneda = moneda;
	}

	public LocalDate getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(LocalDate fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PagoEntity other = (PagoEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
