package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "factura_proveedor")
public class FacturaProveedorEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "comprobante_tipo_id", referencedColumnName = "id", nullable = false)
	private ComprobanteTipoEntity comprobanteTipo = null;

	@OneToOne
	@JoinColumn(name = "puesto_id", referencedColumnName = "id", nullable = false)
	private PuestoEntity puesto = null;

	@Column(name = "numero", nullable = false)
	private Integer numero = null;

	@OneToOne
	@JoinColumn(name = "proveedor_id", referencedColumnName = "id", nullable = false)
	private PersonaEntity proveedor = null;

	@Column(name = "fecha", nullable = false)
	private LocalDate fecha = null;

	@OneToOne
	@JoinColumn(name = "remito_id", referencedColumnName = "id", nullable = false)
	private RemitoProveedorEntity remito = null;

	public FacturaProveedorEntity() {
		super();
	}

	public FacturaProveedorEntity(Integer id, ComprobanteTipoEntity comprobanteTipo, PuestoEntity puesto,
			Integer numero, PersonaEntity proveedor, LocalDate fecha, RemitoProveedorEntity remito) {
		super();
		this.id = id;
		this.comprobanteTipo = comprobanteTipo;
		this.puesto = puesto;
		this.numero = numero;
		this.proveedor = proveedor;
		this.fecha = fecha;
		this.remito = remito;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ComprobanteTipoEntity getComprobanteTipo() {
		return comprobanteTipo;
	}

	public void setComprobanteTipo(ComprobanteTipoEntity comprobanteTipo) {
		this.comprobanteTipo = comprobanteTipo;
	}

	public PuestoEntity getPuesto() {
		return puesto;
	}

	public void setPuesto(PuestoEntity puesto) {
		this.puesto = puesto;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public PersonaEntity getProveedor() {
		return proveedor;
	}

	public void setProveedor(PersonaEntity proveedor) {
		this.proveedor = proveedor;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public RemitoProveedorEntity getRemito() {
		return remito;
	}

	public void setRemito(RemitoProveedorEntity remito) {
		this.remito = remito;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FacturaProveedorEntity other = (FacturaProveedorEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
