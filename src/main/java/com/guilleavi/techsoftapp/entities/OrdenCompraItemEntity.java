package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orden_compra_item")
public class OrdenCompraItemEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "orden_id", referencedColumnName = "id", nullable = false)
	private OrdenCompraEntity orden = null;

	@OneToOne
	@JoinColumn(name = "producto_id", referencedColumnName = "id", nullable = false)
	private ProductoEntity producto = null;

	@Column(name = "cantidad", nullable = false)
	private Integer cantidad = null;

	@Column(name = "precio", nullable = true)
	private Double precio = null;

	@OneToOne
	@JoinColumn(name = "estado_id", referencedColumnName = "id", nullable = false)
	private EstadoEntity estado = null;

	@Column(name = "pendientes", nullable = true)
	private Integer pendientes = null;

	public OrdenCompraItemEntity() {
		super();
	}

	public OrdenCompraItemEntity(Integer id, OrdenCompraEntity orden, ProductoEntity producto, Integer cantidad,
			Double precio, EstadoEntity estado, Integer pendientes) {
		super();
		this.id = id;
		this.orden = orden;
		this.producto = producto;
		this.cantidad = cantidad;
		this.precio = precio;
		this.estado = estado;
		this.pendientes = pendientes;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public OrdenCompraEntity getOrden() {
		return orden;
	}

	public void setOrden(OrdenCompraEntity orden) {
		this.orden = orden;
	}

	public ProductoEntity getProducto() {
		return producto;
	}

	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public EstadoEntity getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntity estado) {
		this.estado = estado;
	}

	public Integer getPendientes() {
		return pendientes;
	}

	public void setPendientes(Integer pendientes) {
		this.pendientes = pendientes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdenCompraItemEntity other = (OrdenCompraItemEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
