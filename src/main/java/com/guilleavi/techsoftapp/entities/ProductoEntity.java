package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class ProductoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@Column(name = "descripcion", nullable = false, length = 200)
	private String descripcion = null;

	@Column(name = "descripcion_reducida", nullable = true, length = 10)
	private String descripcionReducida = null;

	@OneToOne
	@JoinColumn(name = "estado_id", referencedColumnName = "id", nullable = false)
	private EstadoEntity estado = null;

	@Column(name = "codigo_barras", nullable = true, length = 100)
	private String codigoBarras = null;

	@OneToOne
	@JoinColumn(name = "gravado_id", referencedColumnName = "id", nullable = false)
	private GravadoEntity gravado = null;

	@OneToOne
	@JoinColumn(name = "origen_id", referencedColumnName = "id", nullable = true)
	private OrigenEntity origen = null;

	public ProductoEntity() {
		super();
	}

	public ProductoEntity(Integer id, String descripcion, String descripcionReducida, EstadoEntity estado,
			String codigoBarras, GravadoEntity gravado, OrigenEntity origen) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.descripcionReducida = descripcionReducida;
		this.estado = estado;
		this.codigoBarras = codigoBarras;
		this.gravado = gravado;
		this.origen = origen;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionReducida() {
		return descripcionReducida;
	}

	public void setDescripcionReducida(String descripcionReducida) {
		this.descripcionReducida = descripcionReducida;
	}

	public EstadoEntity getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntity estado) {
		this.estado = estado;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public GravadoEntity getGravado() {
		return gravado;
	}

	public void setGravado(GravadoEntity gravado) {
		this.gravado = gravado;
	}

	public OrigenEntity getOrigen() {
		return origen;
	}

	public void setOrigen(OrigenEntity origen) {
		this.origen = origen;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoEntity other = (ProductoEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
