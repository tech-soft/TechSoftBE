package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "persona_comercial")
public class PersonaComercialEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "persona_id", referencedColumnName = "id", nullable = false)
	private PersonaEntity persona = null;

	@OneToOne
	@JoinColumn(name = "iva_tipo_id", referencedColumnName = "id", nullable = true)
	private IvaTipoEntity ivaTipo = null;

	@OneToOne
	@JoinColumn(name = "precio_lista_id", referencedColumnName = "id", nullable = true)
	private PrecioListaEntity precioLista = null;

	@OneToOne
	@JoinColumn(name = "pago_forma_id", referencedColumnName = "id", nullable = true)
	private PagoFormaEntity pagoForma = null;

	@Column(name = "descuento", nullable = false)
	private Double descuento = 0D;

	public PersonaComercialEntity() {
		super();
	}

	public PersonaComercialEntity(Integer id, PersonaEntity persona, IvaTipoEntity ivaTipo,
			PrecioListaEntity precioLista, PagoFormaEntity pagoForma, Double descuento) {
		super();
		this.id = id;
		this.persona = persona;
		this.ivaTipo = ivaTipo;
		this.precioLista = precioLista;
		this.pagoForma = pagoForma;
		this.descuento = descuento;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PersonaEntity getPersona() {
		return persona;
	}

	public void setPersona(PersonaEntity persona) {
		this.persona = persona;
	}

	public IvaTipoEntity getIvaTipo() {
		return ivaTipo;
	}

	public void setIvaTipo(IvaTipoEntity ivaTipo) {
		this.ivaTipo = ivaTipo;
	}

	public PrecioListaEntity getPrecioLista() {
		return precioLista;
	}

	public void setPrecioLista(PrecioListaEntity precioLista) {
		this.precioLista = precioLista;
	}

	public PagoFormaEntity getPagoForma() {
		return pagoForma;
	}

	public void setPagoForma(PagoFormaEntity pagoForma) {
		this.pagoForma = pagoForma;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PersonaComercialEntity other = (PersonaComercialEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PersonaComercialEntity [id=" + id + "]";
	}

}
