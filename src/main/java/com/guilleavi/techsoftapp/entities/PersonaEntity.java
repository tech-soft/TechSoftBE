package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "persona")
public class PersonaEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@Column(name = "nombre", nullable = false, length = 200)
	private String nombre = null;

	@OneToOne
	@JoinColumn(name = "documento_tipo_id", referencedColumnName = "id", nullable = true)
	private DocumentoTipoEntity documentoTipo = null;

	@Column(name = "documento_numero", nullable = true, scale = 18)
	private Long documentoNumero = null;

	@OneToOne
	@JoinColumn(name = "estado_id", referencedColumnName = "id", nullable = false)
	private EstadoEntity estado = null;

	@Column(name = "cliente", nullable = false)
	private Boolean cliente = false;

	@Column(name = "proveedor", nullable = false)
	private Boolean proveedor = false;

	public PersonaEntity() {
		super();
	}

	public PersonaEntity(Integer id, String nombre, DocumentoTipoEntity documentoTipo, Long documentoNumero,
			EstadoEntity estado, Boolean cliente, Boolean proveedor) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.documentoTipo = documentoTipo;
		this.documentoNumero = documentoNumero;
		this.estado = estado;
		this.cliente = cliente;
		this.proveedor = proveedor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DocumentoTipoEntity getDocumentoTipo() {
		return documentoTipo;
	}

	public void setDocumentoTipo(DocumentoTipoEntity documentoTipo) {
		this.documentoTipo = documentoTipo;
	}

	public Long getDocumentoNumero() {
		return documentoNumero;
	}

	public void setDocumentoNumero(Long documentoNumero) {
		this.documentoNumero = documentoNumero;
	}

	public EstadoEntity getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntity estado) {
		this.estado = estado;
	}

	public Boolean getCliente() {
		return cliente;
	}

	public void setCliente(Boolean cliente) {
		this.cliente = cliente;
	}

	public Boolean getProveedor() {
		return proveedor;
	}

	public void setProveedor(Boolean proveedor) {
		this.proveedor = proveedor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PersonaEntity other = (PersonaEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PersonaEntity [id=" + id + ", nombre=" + nombre + "]";
	}

}
