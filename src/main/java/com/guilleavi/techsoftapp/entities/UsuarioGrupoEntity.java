package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario_grupos")
public class UsuarioGrupoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "usuario_id", referencedColumnName = "id", nullable = false)
	private UsuarioEntity usuario = null;

	@OneToOne
	@JoinColumn(name = "grupo_id", referencedColumnName = "id", nullable = false)
	private SysUsuarioGrupoEntity grupo = null;

	public UsuarioGrupoEntity() {
		super();
	}

	public UsuarioGrupoEntity(Integer id, UsuarioEntity usuario, SysUsuarioGrupoEntity grupo) {
		super();
		this.id = id;
		this.usuario = usuario;
		this.grupo = grupo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UsuarioEntity getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioEntity usuario) {
		this.usuario = usuario;
	}

	public SysUsuarioGrupoEntity getGrupo() {
		return grupo;
	}

	public void setGrupo(SysUsuarioGrupoEntity grupo) {
		this.grupo = grupo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioGrupoEntity other = (UsuarioGrupoEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
