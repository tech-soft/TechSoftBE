package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "producto_serie")
public class ProductoSerieEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "remito_id", referencedColumnName = "id", nullable = false)
	private RemitoProveedorItemEntity remitoProveedorItem = null;

	@Column(name = "numero_serie", nullable = false, length = 100)
	private String numeroSerie = null;

	public ProductoSerieEntity() {
		super();
	}

	public ProductoSerieEntity(Integer id, RemitoProveedorItemEntity remitoProveedorItem, String numeroSerie) {
		super();
		this.id = id;
		this.remitoProveedorItem = remitoProveedorItem;
		this.numeroSerie = numeroSerie;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RemitoProveedorItemEntity getRemitoProveedorItem() {
		return remitoProveedorItem;
	}

	public void setRemitoProveedorItem(RemitoProveedorItemEntity remitoProveedorItem) {
		this.remitoProveedorItem = remitoProveedorItem;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoSerieEntity other = (ProductoSerieEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
