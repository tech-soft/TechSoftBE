package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "pago_transferencia")
@PrimaryKeyJoinColumn(name = "id")
public class PagoTransferenciaEntity extends PagoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "cuenta_bancaria", nullable = false, length = 100)
	private String cuentaBancaria = null;

	@OneToOne
	@JoinColumn(name = "banco_id", referencedColumnName = "id", nullable = false)
	private BancoEntity banco = null;

	public PagoTransferenciaEntity() {
		super();
	}

	public PagoTransferenciaEntity(Integer id, PersonaEntity proveedor, PagoFormaEntity pagoForma, MonedaEntity moneda,
			LocalDate fechaEmision, Double importe, String descripcion, String cuentaBancaria, BancoEntity banco) {
		super(id, proveedor, pagoForma, moneda, fechaEmision, importe, descripcion);
		this.cuentaBancaria = cuentaBancaria;
		this.banco = banco;
	}

	public String getCuentaBancaria() {
		return cuentaBancaria;
	}

	public void setCuentaBancaria(String cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

	public BancoEntity getBanco() {
		return banco;
	}

	public void setBanco(BancoEntity banco) {
		this.banco = banco;
	}

}
