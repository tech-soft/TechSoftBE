package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "remito_proveedor")
public class RemitoProveedorEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "comprobante_tipo_id", referencedColumnName = "id", nullable = false)
	private ComprobanteTipoEntity comprobanteTipo = null;

	@Column(name = "puesto", nullable = false)
	private Integer puesto = null;

	@Column(name = "numero", nullable = false)
	private Long numero = null;

	@OneToOne
	@JoinColumn(name = "proveedor_id", referencedColumnName = "id", nullable = false)
	private PersonaEntity proveedor = null;

	@Column(name = "fecha", nullable = false)
	private LocalDate fecha = null;

	@Column(name = "observaciones", nullable = true, length = 255)
	private String observaciones = null;

	public RemitoProveedorEntity() {
		super();
	}

	public RemitoProveedorEntity(Integer id, ComprobanteTipoEntity comprobanteTipo, Integer puesto, Long numero,
			PersonaEntity proveedor, LocalDate fecha, String observaciones) {
		super();
		this.id = id;
		this.comprobanteTipo = comprobanteTipo;
		this.puesto = puesto;
		this.numero = numero;
		this.proveedor = proveedor;
		this.fecha = fecha;
		this.observaciones = observaciones;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ComprobanteTipoEntity getComprobanteTipo() {
		return comprobanteTipo;
	}

	public void setComprobanteTipo(ComprobanteTipoEntity comprobanteTipo) {
		this.comprobanteTipo = comprobanteTipo;
	}

	public Integer getPuesto() {
		return puesto;
	}

	public void setPuesto(Integer puesto) {
		this.puesto = puesto;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public PersonaEntity getProveedor() {
		return proveedor;
	}

	public void setProveedor(PersonaEntity proveedor) {
		this.proveedor = proveedor;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemitoProveedorEntity other = (RemitoProveedorEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
