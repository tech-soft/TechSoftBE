package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "producto_stock")
public class ProductoStockEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "producto_id", referencedColumnName = "id", nullable = false)
	private ProductoEntity producto = null;

	@Column(name = "stock_entrante", nullable = true)
	private Integer stockEntrante = null;

	@Column(name = "stock_disponible", nullable = true)
	private Integer stockDisponible = null;

	@Column(name = "stock_transito", nullable = true)
	private Integer stockTransito = null;

	@Column(name = "stock_reservado", nullable = true)
	private Integer stockReservado = null;

	@Column(name = "stock_vendido", nullable = true)
	private Integer stockVendido = null;

	public ProductoStockEntity() {
		super();
	}

	public ProductoStockEntity(Integer id, ProductoEntity producto, Integer stockEntrante, Integer stockDisponible,
			Integer stockTransito, Integer stockReservado, Integer stockVendido) {
		super();
		this.id = id;
		this.producto = producto;
		this.stockEntrante = stockEntrante;
		this.stockDisponible = stockDisponible;
		this.stockTransito = stockTransito;
		this.stockReservado = stockReservado;
		this.stockVendido = stockVendido;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProductoEntity getProducto() {
		return producto;
	}

	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}

	public Integer getStockEntrante() {
		return stockEntrante;
	}

	public void setStockEntrante(Integer stockEntrante) {
		this.stockEntrante = stockEntrante;
	}

	public Integer getStockDisponible() {
		return stockDisponible;
	}

	public void setStockDisponible(Integer stockDisponible) {
		this.stockDisponible = stockDisponible;
	}

	public Integer getStockTransito() {
		return stockTransito;
	}

	public void setStockTransito(Integer stockTransito) {
		this.stockTransito = stockTransito;
	}

	public Integer getStockReservado() {
		return stockReservado;
	}

	public void setStockReservado(Integer stockReservado) {
		this.stockReservado = stockReservado;
	}

	public Integer getStockVendido() {
		return stockVendido;
	}

	public void setStockVendido(Integer stockVendido) {
		this.stockVendido = stockVendido;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoStockEntity other = (ProductoStockEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
