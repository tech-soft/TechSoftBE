package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "pago_cheque")
@PrimaryKeyJoinColumn(name = "id")
public class PagoChequeEntity extends PagoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@OneToOne
	@JoinColumn(name = "emisor_id", referencedColumnName = "id", nullable = false)
	private PersonaEntity emisor = null;

	@Column(name = "numero", nullable = false, length = 100)
	private String numero = null;

	@OneToOne
	@JoinColumn(name = "banco_id", referencedColumnName = "id", nullable = false)
	private BancoEntity banco = null;

	@Column(name = "fecha_pago", nullable = true)
	private LocalDate fechaPago = null;

	public PagoChequeEntity() {
		super();
	}

	public PagoChequeEntity(Integer id, PersonaEntity proveedor, PagoFormaEntity pagoForma, MonedaEntity moneda,
			LocalDate fechaEmision, Double importe, String descripcion, PersonaEntity emisor, String numero,
			BancoEntity banco, LocalDate fechaPago) {
		super(id, proveedor, pagoForma, moneda, fechaEmision, importe, descripcion);
		this.emisor = emisor;
		this.numero = numero;
		this.banco = banco;
		this.fechaPago = fechaPago;
	}

	public PersonaEntity getEmisor() {
		return emisor;
	}

	public void setEmisor(PersonaEntity emisor) {
		this.emisor = emisor;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public BancoEntity getBanco() {
		return banco;
	}

	public void setBanco(BancoEntity banco) {
		this.banco = banco;
	}

	public LocalDate getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(LocalDate fechaPago) {
		this.fechaPago = fechaPago;
	}

}
