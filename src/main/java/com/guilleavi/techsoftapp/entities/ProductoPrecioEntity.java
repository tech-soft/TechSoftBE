package com.guilleavi.techsoftapp.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "producto_precio")
public class ProductoPrecioEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "producto_id", referencedColumnName = "id", nullable = false)
	private ProductoEntity producto = null;

	@Column(name = "fecha", nullable = false)
	private LocalDate fecha = null;

	@OneToOne
	@JoinColumn(name = "lista_id", referencedColumnName = "id", nullable = false)
	private PrecioListaEntity listaPrecio = null;
	
	// TODO: null = false
	@Column(name = "renta", precision = 9, scale = 2, nullable = true)
	private Double renta = null;

	public ProductoPrecioEntity() {
		super();
	}

	public ProductoPrecioEntity(Integer id, ProductoEntity producto, LocalDate fechaCreacion,
			PrecioListaEntity listaPrecio, Double renta) {
		super();
		this.id = id;
		this.producto = producto;
		this.fecha = fechaCreacion;
		this.listaPrecio = listaPrecio;
		this.renta = renta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProductoEntity getProducto() {
		return producto;
	}

	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}
	
	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fechaCreacion) {
		this.fecha = fechaCreacion;
	}

	public PrecioListaEntity getListaPrecio() {
		return listaPrecio;
	}

	public void setListaPrecio(PrecioListaEntity listaPrecio) {
		this.listaPrecio = listaPrecio;
	}

	public Double getRenta() {
		return renta;
	}

	public void setRenta(Double renta) {
		this.renta = renta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoPrecioEntity other = (ProductoPrecioEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
