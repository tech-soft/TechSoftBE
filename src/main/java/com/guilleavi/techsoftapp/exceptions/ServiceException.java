package com.guilleavi.techsoftapp.exceptions;

public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public static final int UNKNOWN = 5500;
	public static final int ENTITY_NOT_FOUND = 5501;
	public static final int DATA_INTEGRITY_VIOLATION = 5502;
	public static final int MISSING_DATA = 5503;
	public static final int TRANSACION_API_EXCEPTION = 5504;

	private int code;

	public ServiceException(int code, String message) {
		super(message);
		this.code = code;
	}

	public ServiceException(String message) {
		this(UNKNOWN, message);
	}

	public int getCode() {
		return code;
	}
}
